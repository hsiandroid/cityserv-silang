package ph.cityserv.oneserve.silang.android.dialog;

import android.Manifest;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.CitizenReportItem;
import ph.cityserv.oneserve.silang.data.model.server.JobItem;
import ph.cityserv.oneserve.silang.data.model.server.NewsItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.server.request.Article;
import ph.cityserv.oneserve.silang.server.request.CitizenReport;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseDialog;
import ph.cityserv.oneserve.silang.vendor.android.java.ToastMessage;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;


public class ApplyJobDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = ApplyJobDialog.class.getName().toString();

	public static ApplyJobDialog newInstance(Callback callback, int id) {
		ApplyJobDialog dialog = new ApplyJobDialog();
		dialog.callback = callback;
		dialog.jobID = id;
		return dialog;
	}
	private Callback callback;


	@BindView(R.id.emailET) 				EditText emailET;
	@BindView(R.id.nameET) 					EditText nameET;
	@BindView(R.id.contactET) 				EditText contactET;
	@BindView(R.id.applyBTN)				View applyBTN;
	@BindView(R.id.exitBTN)					View exitBTN;

	@State int jobID;
	@State String name;
	@State String email;
	@State String contact;

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_apply_job;
	}

	@Override
	public void onViewReady() {
		emailET.setText(UserData.getUserItem().email);
		nameET.setText(UserData.getUserItem().info.data.name);
		contactET.setText(UserData.getUserItem().info.data.contactNumber);
		exitBTN.setOnClickListener(this);
		applyBTN.setOnClickListener(this);

	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
		setDialogMatchParent();
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}


	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.exitBTN:
				dismiss();
				break;
			case R.id.applyBTN:
				Article.getDefault(getContext()).apply(emailET.getText().toString(), nameET.getText().toString(), contactET.getText().toString(), jobID);
				break;
		}
	}


	@Subscribe
	public void onResponse(Article.ApplyResponse responseData){
		SingleTransformer<JobItem> singleTransformer = responseData.getData(SingleTransformer.class);
		if(singleTransformer.status){
			ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
			if(callback != null){
				callback.onSuccess();
				dismiss();
			}else{
				ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
			}
		}
	}

	public interface Callback {
		void onSuccess();
	}
}
