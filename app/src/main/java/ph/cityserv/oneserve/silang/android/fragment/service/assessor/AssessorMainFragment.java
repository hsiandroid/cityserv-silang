package ph.cityserv.oneserve.silang.android.fragment.service.assessor;

import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.AssessorActivity;
import ph.cityserv.oneserve.silang.android.adapter.AssessorListViewAdapter;
import ph.cityserv.oneserve.silang.android.adapter.NotificationListViewAdapter;
import ph.cityserv.oneserve.silang.android.adapter.RequestTypeListViewAdapter;
import ph.cityserv.oneserve.silang.data.model.server.NotificationItem;
import ph.cityserv.oneserve.silang.data.model.server.RequestItem;
import ph.cityserv.oneserve.silang.data.model.server.ServiceItem;
import ph.cityserv.oneserve.silang.data.model.server.SubServiceItem;
import ph.cityserv.oneserve.silang.server.request.CitizenRequest;
import ph.cityserv.oneserve.silang.server.request.Notification;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.Keyboard;
import ph.cityserv.oneserve.silang.vendor.android.widget.ExpandableHeightListView;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;

public class AssessorMainFragment extends BaseFragment implements
        View.OnClickListener,
        AssessorListViewAdapter.ClickListener,
        RequestTypeListViewAdapter.ClickListener,
        SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = AssessorMainFragment.class.getName().toString();

    private AssessorActivity assessorActivity;

    private ServiceItem serviceItem;

    private AssessorListViewAdapter         assessorListViewAdapter;
    private NotificationListViewAdapter     notificationListViewAdapter;
    private RequestTypeListViewAdapter      requestTypeListViewAdapter;

    @BindView(R.id.assessorSRL)             SwipeRefreshLayout assessorSRL;
    @BindView(R.id.assessorSV)              ScrollView assessorSV;
    @BindView(R.id.imageIV)                 ImageView imageIV;
    @BindView(R.id.headerTXT)               TextView headerTXT;
    @BindView(R.id.tracking1ET)             EditText tracking1ET;
    @BindView(R.id.tracking2ET)             EditText tracking2ET;
    @BindView(R.id.tracking3ET)             EditText tracking3ET;
    @BindView(R.id.clearBTN)                View clearBTN;
    @BindView(R.id.headerTrackTXT)          TextView headerTrackTXT;
    @BindView(R.id.requestTabTXT)           TextView requestTabTXT;
    @BindView(R.id.notificationTabTXT)      TextView notificationTabTXT;
    @BindView(R.id.requestCON)              View requestCON;
    @BindView(R.id.requestEHLV)             ExpandableHeightListView requestEHLV;
    @BindView(R.id.requestMoreTXT)          TextView requestMoreTXT;
    @BindView(R.id.notificationCON)         View notificationCON;
    @BindView(R.id.requestPlaceholderCON)   View requestPlaceholderCON;
    @BindView(R.id.notificationEHLV)        ExpandableHeightListView notificationEHLV;
    @BindView(R.id.notificationMoreTXT)     TextView notificationMoreTXT;
    @BindView(R.id.requestTypeEHLV)         ExpandableHeightListView requestTypeEHLV;
    @BindView(R.id.requestTypeMoreTXT)      TextView requestTypeMoreTXT;
    @BindView(R.id.requestTabPB)            ProgressBar requestTabPB;
    @BindView(R.id.notificationTabPB)       ProgressBar notificationTabPB;

    public static AssessorMainFragment newInstance(ServiceItem serviceItem) {
        AssessorMainFragment fragment = new AssessorMainFragment();
        fragment.serviceItem = serviceItem;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_assessor_main;
    }

    @Override
    public void onViewReady() {
        assessorActivity = (AssessorActivity) getActivity();
        assessorActivity.setTitle(getString(R.string.title_assessor));

        headerTXT.setOnClickListener(this);
        headerTrackTXT.setOnClickListener(this);
        clearBTN.setOnClickListener(this);
        requestTabTXT.setOnClickListener(this);
        notificationTabTXT.setOnClickListener(this);

        assessorSRL.setOnRefreshListener(this);

        setUpAssessorListView();
        setUpNotificationListView();
        setUpRequestTypeListView();
        setUpTracking();
        setRequestTabSelected();
    }

    private void setUpAssessorListView(){
        assessorListViewAdapter = new AssessorListViewAdapter(getContext());
        assessorListViewAdapter.setClickListener(this);
        requestEHLV.setAdapter(assessorListViewAdapter);
    }

    private void setUpNotificationListView(){
        notificationListViewAdapter = new NotificationListViewAdapter(getContext());
        notificationEHLV.setAdapter(notificationListViewAdapter);
    }

    private void setUpRequestTypeListView(){
        requestTypeListViewAdapter = new RequestTypeListViewAdapter(getContext());
        requestTypeListViewAdapter.setClickListener(this);
        requestTypeEHLV.setAdapter(requestTypeListViewAdapter);
        if(serviceItem != null && serviceItem.subservices != null){
            requestTypeListViewAdapter.setNewData(serviceItem.subservices.data);
        }
    }

    private void setUpTracking(){
        tracking1ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after){

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
                if(tracking1ET.getText().length() == 2){
                    tracking1ET.clearFocus();
                    tracking2ET.requestFocus();
                    tracking2ET.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                clearTrackingButton();
            }
        });

        tracking2ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
                if(tracking2ET.getText().length() == 5){
                    tracking2ET.clearFocus();
                    tracking3ET.requestFocus();
                    tracking3ET.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(tracking2ET.getText().length() == 0){
                    tracking2ET.clearFocus();
                    tracking1ET.requestFocus();
                    tracking1ET.setSelection(tracking1ET.length());
                }

                clearTrackingButton();
            }
        });

        tracking3ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
                if(tracking3ET.getText().length() == 2){
                    attemptTrackRequest();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(tracking3ET.getText().length() == 0){
                    tracking3ET.clearFocus();
                    tracking2ET.requestFocus();
                    tracking2ET.setSelection(tracking2ET.length());
                }

                clearTrackingButton();
            }
        });
    }

    public void attemptTrackRequest(){
        Keyboard.hideKeyboard(assessorActivity);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(tracking1ET.getText().toString());
        stringBuilder.append("-");
        stringBuilder.append(tracking2ET.getText().toString());
        stringBuilder.append("-");
        stringBuilder.append(tracking3ET.getText().toString());

        CitizenRequest.getDefault(getContext()).create(stringBuilder.toString());
    }

    public void clearTrackingButton(){
        String id1 = tracking1ET.getText().toString();
        String id2 = tracking2ET.getText().toString();
        String id3 = tracking3ET.getText().toString();

        if(TextUtils.isEmpty(id1) && TextUtils.isEmpty(id2) && TextUtils.isEmpty(id3)){
            clearBTN.setVisibility(View.GONE);
        }else{
            clearBTN.setVisibility(View.VISIBLE);
        }
    }

    private void setRequestTabSelected(){
        requestTabTXT.setSelected(true);
        notificationTabTXT.setSelected(false);
        notificationCON.setVisibility(View.GONE);
        requestCON.setVisibility(View.VISIBLE);
        assessorSV.smoothScrollTo(assessorSV.getScrollX(), assessorSV.getScrollY());
    }

    private void setNotificationTabSelected(){
        requestTabTXT.setSelected(false);
        notificationTabTXT.setSelected(true);
        notificationCON.setVisibility(View.VISIBLE);
        requestPlaceholderCON.setVisibility(View.GONE);
        requestCON.setVisibility(View.GONE);
        assessorSV.smoothScrollTo(assessorSV.getScrollX(), assessorSV.getScrollY());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.headerTXT:
                assessorActivity.openAssessorFormFragment();
                break;
            case R.id.headerTrackTXT:
                attemptTrackRequest();
                break;
            case R.id.requestTabTXT:
                setRequestTabSelected();
                break;
            case R.id.notificationTabTXT:
                setNotificationTabSelected();
                break;
            case R.id.clearBTN:
                clearTrackingData();
                break;
        }
    }

    private void clearTrackingData(){
        tracking1ET.setText("");
        tracking2ET.setText("");
        tracking3ET.setText("");

        tracking1ET.clearFocus();
        tracking3ET.clearFocus();
        tracking2ET.requestFocus();
        tracking2ET.setSelection(tracking2ET.length());
    }

    @Override
    public void onItemClick(RequestItem requestItem) {
        assessorActivity.openRequirementStatusFragment(requestItem);

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onRefresh() {
        assessorSRL.setRefreshing(false);
        refreshList();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Subscribe
    public void onResponse(CitizenRequest.TrackRequestResponse responseData){
        SingleTransformer<RequestItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            assessorActivity.startAssessorActivity(singleTransformer.data, "status");
        }
    }

    @Subscribe
    public void onResponse(CitizenRequest.MyRequestResponse responseData){
        CollectionTransformer<RequestItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            requestTabPB.setVisibility(View.GONE);
            if(responseData.isNext()){
                assessorListViewAdapter.addNewData(collectionTransformer.data);
            }else{
                assessorListViewAdapter.setNewData(collectionTransformer.data);
            }
        }

        if (assessorListViewAdapter.getCount() == 0){
            requestPlaceholderCON.setVisibility(View.VISIBLE);
            requestEHLV.setVisibility(View.GONE);
        }else {
            requestPlaceholderCON.setVisibility(View.GONE);
            requestEHLV.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onResponse(Notification.NotificationResponse responseData){
        CollectionTransformer<NotificationItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            notificationTabPB.setVisibility(View.GONE);
            if(responseData.isNext()){
                notificationListViewAdapter.addNewData(collectionTransformer.data);
            }else{
                notificationListViewAdapter.setNewData(collectionTransformer.data);
            }
        } if (notificationListViewAdapter.getCount() == 0){
            notificationEHLV.setVisibility(View.GONE);
        }else {
            notificationEHLV.setVisibility(View.VISIBLE);
        }

    }

    public void refreshList(){
        requestTabPB.setVisibility(View.VISIBLE);
        notificationTabPB.setVisibility(View.VISIBLE);
        CitizenRequest.getDefault(getContext()).myRequest();
        Notification.getDefault(getContext()).all();
    }

    @Override
    public void onItemClick(SubServiceItem subServiceItem) {
        assessorActivity.startAssessorActivity(subServiceItem, "requirements-subservice");
    }
}
