package ph.cityserv.oneserve.silang.android.dialog;

import android.Manifest;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.JobItem;
import ph.cityserv.oneserve.silang.server.request.Article;
import ph.cityserv.oneserve.silang.server.request.PESOModel;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseDialog;
import ph.cityserv.oneserve.silang.vendor.android.java.ToastMessage;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;


public class PostJobDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = PostJobDialog.class.getName().toString();

	public static PostJobDialog newInstance() {
		PostJobDialog dialog = new PostJobDialog();
		return dialog;
	}

	@BindView(R.id.emailTXT) 				TextView emailTXT;
	@BindView(R.id.celTXT) 					TextView celTXT;
	@BindView(R.id.telTXT) 					TextView telTXT;
	@BindView(R.id.addressTXT) 			    TextView addressTXT;
	@BindView(R.id.exitBTN)					TextView exitBTN;

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_post_job;
	}

	@Override
	public void onViewReady() {
		Article.getDefault(getContext()).peso();
		emailTXT.setPaintFlags(emailTXT.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
		celTXT.setPaintFlags(celTXT.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
		telTXT.setPaintFlags(celTXT.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
		emailTXT.setOnClickListener(this);
		celTXT.setOnClickListener(this);
		telTXT.setOnClickListener(this);
		emailTXT.setEnabled(false);
		celTXT.setEnabled(false);
		telTXT.setEnabled(false);

		exitBTN.setOnClickListener(this);

		Dexter.withActivity(getActivity())
				.withPermission(Manifest.permission.CALL_PHONE)
				.withListener(new PermissionListener() {
					@Override public void onPermissionGranted(PermissionGrantedResponse response) {
						emailTXT.setEnabled(true);
						celTXT.setEnabled(true);
						telTXT.setEnabled(true);
					}
					@Override public void onPermissionDenied(PermissionDeniedResponse response) {
					}
					@Override
					public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {

					}
				}).check();

	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
		setDialogMatchParent();
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	@Subscribe
	public void onResponse(Article.PesoResponse responseData){
		SingleTransformer<PESOModel> singleTransformer = responseData.getData(SingleTransformer.class);
		if(singleTransformer.status){
			emailTXT.setText(singleTransformer.data.email);
			Log.d(TAG, "onResponse: " + new Gson().toJson(singleTransformer.data));
			telTXT.setText(singleTransformer.data.phoneNumber);
			Log.d(TAG, "onResponse: " + singleTransformer.data.phoneNumber);
			celTXT.setText(singleTransformer.data.contactNumber);
			Log.d(TAG, "onResponse: " + singleTransformer.data.contactNumber);
			addressTXT.setText(singleTransformer.data.address);
			Log.d(TAG, "onResponse: " + singleTransformer.data.address);
			}else{
				ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
			}
		}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.exitBTN:
				dismiss();
				break;
			case R.id.emailTXT:
				Intent emailIntent = new Intent(Intent.ACTION_VIEW);
				Uri data = Uri.parse("mailto:?subject=" + "" + "&body=" + "" + "&to=" + emailTXT.getText().toString());
				emailIntent.setData(data);
				startActivity(emailIntent);
				break;
			case R.id.celTXT:
				Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + celTXT.getText().toString()));
				startActivity(intent);
				break;
			case R.id.telTXT:
				Intent intent1 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + telTXT.getText().toString()));
				startActivity(intent1);
				break;
		}
	}
}
