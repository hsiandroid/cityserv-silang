package ph.cityserv.oneserve.silang.vendor.server.transformer;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 8/3/2017.
 */

public class SingleTransformer<T> extends BaseTransformer{

    @SerializedName("data")
    public T data;
}
