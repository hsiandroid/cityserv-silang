package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.WidgetItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseRecylerViewAdapter;

public class WidgetRecyclerViewAdapter extends BaseRecylerViewAdapter<WidgetRecyclerViewAdapter.ViewHolder, WidgetItem> {

    private ClickListener clickListener;

    public WidgetRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        switch (getItem(position).display_type){
            case "text_and_image":
                return new ViewHolder(getDefaultView(parent, R.layout.adapter_list_text_image));
            case "image_only":
                return new ViewHolder(getDefaultView(parent, R.layout.adapter_list_image_only));
            default:
                return new ViewHolder(getDefaultView(parent, R.layout.adapter_list_text_only));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setOnClickListener(this);
        holder.getBaseView().setTag(holder.getItem());

        if(holder.widgetIMG != null){
            Picasso.with(getContext())
                    .load(holder.getItem().image.full_path)
                    .placeholder(R.drawable.placeholder_logo)
                    .error(R.drawable.placeholder_logo)
                    .into(holder.widgetIMG);
        }

        if(holder.widgetTXT != null){
            holder.widgetTXT.setText(holder.getItem().title);
        }

        if(holder.contentTXT != null){
            if(holder.getItem().short_description != null){
                holder.contentTXT.setText(holder.getItem().short_description);
                holder.contentTXT.setVisibility(View.VISIBLE);
            }else{
                holder.contentTXT.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((WidgetItem) v.getTag());
        }
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @Nullable@BindView(R.id.widgetIMG)       ImageView widgetIMG;
        @Nullable@BindView(R.id.widgetTXT)       TextView widgetTXT;
        @Nullable@BindView(R.id.contentTXT)      TextView contentTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public WidgetItem getItem() {
            return (WidgetItem) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends Listener{
        void onItemClick(WidgetItem widgetItem);
    }
} 
