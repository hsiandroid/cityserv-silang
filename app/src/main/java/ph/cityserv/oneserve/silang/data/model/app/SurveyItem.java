package ph.cityserv.oneserve.silang.data.model.app;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 6/25/2016.
 */
public class SurveyItem extends AndroidModel {
    public int number;
    public String question;
    public boolean isEditText;
    public String answer;

    public boolean hasNote;
    public String note1;
    public String note2;

    public boolean isRadio;
    public boolean strongly_agree;
    public boolean somewhat_agree;
    public boolean strongly_disagree;
    public boolean somewhat_disagree;
}
