package ph.cityserv.oneserve.silang.vendor.android.java;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.text.SpannedString;

/**
 * Created by Jomar Olaybal on 8/3/2017.
 */

public class StringFormatter {

    public static boolean isEmpty(String string) {
        if(string == null) return true;
        if(string.length() == 0) return true;
        return string.equals("null");
    }

    public static String first(String string) {
        if(string.length()>0){
            return string.substring(0,1).toUpperCase() + string.substring(1).toLowerCase();
        }
        return string;
    }

    public static String firstWord(String string) {
        if(string == null) return string;

        if(string.length()<0) return string;

        String [] words = string.split(" ");

        if(words.length < 0 ) return string;

        return words[0];
    }

    public static String titleCase(String string) {
        if(string == null) return string;

        if(string.length()<0) return string;

        StringBuilder sb = new StringBuilder(string.length());
        for(String word : string.split(" ")){
            sb.append(first(word));
            sb.append(" ");
        }
        return sb.toString().trim();
    }

    public static Spanned fromHtml(String string) {
        if(string == null) return new SpannedString(string);

        if(string.length()<0) return new SpannedString(string);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(string,Html.FROM_HTML_MODE_LEGACY);
        }else{
            return Html.fromHtml(string);
        }
    }


}
