package ph.cityserv.oneserve.silang.android.fragment.main;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.JobActivity;
import ph.cityserv.oneserve.silang.android.adapter.YouthAffairsRecyclerViewAdapter;
import ph.cityserv.oneserve.silang.android.dialog.NewsDialog;
import ph.cityserv.oneserve.silang.android.dialog.YouthAffairsDialog;
import ph.cityserv.oneserve.silang.data.model.server.NewsItem;
import ph.cityserv.oneserve.silang.data.model.server.YouthAffairsItem;
import ph.cityserv.oneserve.silang.server.request.Article;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.DividerItemDecoration;
import ph.cityserv.oneserve.silang.vendor.android.java.EndlessRecyclerViewScrollListener;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;

public class YouthAffairsFragment extends BaseFragment implements
        YouthAffairsRecyclerViewAdapter.ClickListener,
        SwipeRefreshLayout.OnRefreshListener, EndlessRecyclerViewScrollListener.Callback{

    public static final String TAG = YouthAffairsFragment.class.getName().toString();

    private JobActivity jobActivity;

    private YouthAffairsRecyclerViewAdapter newsRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private APIRequest apiRequest;

    @BindView(R.id.newsSRL)                 SwipeRefreshLayout newsSRL;
    @BindView(R.id.newsRV)                  RecyclerView newsRV;
    @BindView(R.id.newsPlaceholderCON)      View newsPlaceholderCON;

    public static YouthAffairsFragment newInstance() {
        YouthAffairsFragment fragment = new YouthAffairsFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_news;
    }

    @Override
    public void onViewReady() {
        jobActivity = (JobActivity) getActivity();
        jobActivity.setTitle("YOUTH AFFAIRS");

        setUpNewsListView();
    }

    private void setUpNewsListView(){
        newsSRL.setOnRefreshListener(this);
        linearLayoutManager = new LinearLayoutManager(getContext());

        newsRecyclerViewAdapter = new YouthAffairsRecyclerViewAdapter(getContext());
        newsRecyclerViewAdapter.setClickListener(this);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        newsRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        newsRV.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(getResources().getDimensionPixelSize(R.dimen.default_dimen)));
        newsRV.setLayoutManager(linearLayoutManager);
        newsRV.setAdapter(newsRecyclerViewAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Subscribe
    public void onResponse(Article.AllYouthAffairsResponse responseData){
        CollectionTransformer<YouthAffairsItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            if(responseData.isNext()){
                newsRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                newsRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }
        }
        if (newsRecyclerViewAdapter.getItemCount() == 0){
            newsRV.setVisibility(View.GONE);
            newsPlaceholderCON.setVisibility(View.VISIBLE);
        }else {
            newsRV.setVisibility(View.VISIBLE);
            newsPlaceholderCON.setVisibility(View.GONE);
        }
    }

    public void refreshList(){
        apiRequest = Article.getDefault(getContext()).allYouthAffairs(newsSRL);
        apiRequest.first();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    @Override
    public void onReadMoreClick(YouthAffairsItem youthAffairsItem) {
        YouthAffairsDialog.newInstance(youthAffairsItem).show(getChildFragmentManager(),NewsDialog.TAG);

    }
}
