package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.RequestItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseListViewAdapter;

public class AssessorListViewAdapter extends BaseListViewAdapter<AssessorListViewAdapter.ViewHolder, RequestItem> {

    private ClickListener clickListener;

    public AssessorListViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_assessor));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setOnClickListener(this);

        holder.titleTXT.setText(holder.getItem().title);
        holder.statusTXT.setText(holder.getItem().status);
        holder.codeTXT.setText(holder.getItem().tracking_number);
        holder.contentTXT.setText(holder.getItem().remarks);

        holder.indicatorView.setBackgroundColor(ActivityCompat.getColor(getContext(), ((position % 2) == 0) ? R.color.colorPrimary : R.color.colorPrimaryLight));

    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick(((ViewHolder) v.getTag()).getItem());
        }
    }

    public class ViewHolder extends BaseListViewAdapter.ViewHolder{

        @BindView(R.id.indicatorView)       View indicatorView;
        @BindView(R.id.titleTXT)            TextView titleTXT;
        @BindView(R.id.statusTXT)           TextView statusTXT;
        @BindView(R.id.codeTXT)             TextView codeTXT;
        @BindView(R.id.contentTXT)          TextView contentTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public RequestItem getItem() {
            return (RequestItem) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends Listener{
        void onItemClick(RequestItem requestItem);
    }
} 
