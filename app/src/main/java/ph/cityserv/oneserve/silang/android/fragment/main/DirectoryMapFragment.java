package ph.cityserv.oneserve.silang.android.fragment.main;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.MainActivity;
import ph.cityserv.oneserve.silang.android.adapter.DirectoryMapRecyclerViewAdapter;
import ph.cityserv.oneserve.silang.data.model.server.DirectoryItem;
import ph.cityserv.oneserve.silang.data.model.server.EstablishmentItem;
import ph.cityserv.oneserve.silang.server.request.Directory;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;

public class DirectoryMapFragment extends BaseFragment implements
        View.OnClickListener,
        DirectoryMapRecyclerViewAdapter.ClickListener,
        GoogleMap.OnMarkerClickListener,
        OnMapReadyCallback,
        GoogleMap.OnMapClickListener {

    public static final String TAG = DirectoryMapFragment.class.getName().toString();

    private MainActivity mainActivity;

    private GoogleMap googleMap;
    private SupportMapFragment customMapFragment;
    private LatLngBounds.Builder builder = new LatLngBounds.Builder();
    private DirectoryItem selectedDirectoryItem;

    private DirectoryMapRecyclerViewAdapter directoryMapRecyclerViewAdapter;
    private LinearLayoutManager directoryLinearLayoutManager;

    @BindView(R.id.directoryRV)         RecyclerView directoryRV;
    @BindView(R.id.directoryPB)         ProgressBar directoryPB;
    @BindView(R.id.listBTN)             View listBTN;
    @BindView(R.id.mapBTN)              View mapBTN;
    @BindView(R.id.infoCON)             View inforCON;
    @BindView(R.id.cityTXT)             TextView cityTXT;
    @BindView(R.id.addressTXT)          TextView addressTXT;
    @BindView(R.id.contactTXT)          TextView contactTXT;

    public static DirectoryMapFragment newInstance() {
        DirectoryMapFragment fragment = new DirectoryMapFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_directory_map;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_directory));
        listBTN.setOnClickListener(this);
        setUpDirectoryListView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (customMapFragment == null) {
            customMapFragment = new SupportMapFragment();
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.add(R.id.directory_map_fl, customMapFragment).commit();
            customMapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getChildFragmentManager().beginTransaction()
                .remove(customMapFragment)
                .commit();
        customMapFragment = null;
    }

    private void setUpDirectoryListView(){
        directoryMapRecyclerViewAdapter = new DirectoryMapRecyclerViewAdapter(getContext());
        directoryMapRecyclerViewAdapter.setonClickListener(this);

        directoryLinearLayoutManager = new LinearLayoutManager(getContext());
        directoryLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        directoryRV.setLayoutManager(directoryLinearLayoutManager);
        directoryRV.setAdapter(directoryMapRecyclerViewAdapter);
    }

    private void displayMarkers(final DirectoryItem directoryItem){
        this.googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                selectedDirectoryItem = directoryItem;
                googleMap.clear();
                builder = new LatLngBounds.Builder();
                for(EstablishmentItem establishmentItem : directoryItem.establishments.data){
                    googleMap.addMarker(addMarker(establishmentItem, directoryItem.establishments.data.indexOf(establishmentItem)));
                    builder.include(establishmentItem.geolocation.getLatLong());
                }

                DirectoryMapFragment.this.googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), getActivity().getResources().getDimensionPixelSize(R.dimen.dimen_40)));
            }
        });
    }

    private MarkerOptions addMarker(EstablishmentItem establishmentItem, int position) {
        MarkerOptions marker = new MarkerOptions();
        marker.position(establishmentItem.geolocation.getLatLong());
        marker.title(String.valueOf(position));
        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.default_marker));
        return marker;
    }

    private void displayMarkerInfo(int position){
        EstablishmentItem establishmentItem = selectedDirectoryItem.establishments.data.get(position);
        cityTXT.setText(establishmentItem.name);
        addressTXT.setText(establishmentItem.address);
        contactTXT.setText(establishmentItem.contact);
        inforCON.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.listBTN:
                mainActivity.openDirectoryListFragment();
                break;
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        inforCON.setVisibility(View.VISIBLE);
        displayMarkerInfo(Integer.parseInt(marker.getTitle()));
        return false;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        inforCON.setVisibility(View.GONE);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setOnMarkerClickListener(this);
        this.googleMap.setOnMapClickListener(this);
        this.googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return new View(getContext());
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });
        refreshList();
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Directory.DirectoryResponse responseData){
        CollectionTransformer<DirectoryItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            if(responseData.isNext()){
                directoryMapRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else{
                directoryMapRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }

            if(directoryMapRecyclerViewAdapter.getItemCount() > 0){
                displayMarkers(directoryMapRecyclerViewAdapter.getItem(0));
            }else{
                googleMap.clear();
            }
        }
    }

    public void refreshList(){
        Directory.getDefault(getContext()).all(null);
    }

    @Override
    public void onItemClick(DirectoryItem directoryItem) {
        displayMarkers(directoryItem);
    }
}
