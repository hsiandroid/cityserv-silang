package ph.cityserv.oneserve.silang.vendor.server.request;

import android.content.Context;

/**
 * Created by BCTI 3 on 8/16/2017.
 */

public class BaseRequest {
    private Context context;

    public BaseRequest(Context context){
        this.context = context;
    }

    public Context getContext() {
        return context;
    }
}
