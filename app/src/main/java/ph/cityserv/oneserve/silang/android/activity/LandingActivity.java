package ph.cityserv.oneserve.silang.android.activity;

import android.view.View;

import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.fragment.landing.LoginFragment;
import ph.cityserv.oneserve.silang.android.fragment.landing.SplashFragment;
import ph.cityserv.oneserve.silang.android.route.RouteActivity;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class LandingActivity extends RouteActivity {
    public static final String TAG = LandingActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_landing;
    }

    @Override
    public void onViewReady() {
        getWindow().getDecorView().setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                            View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        openSplashFragment();
    }

    public void openSplashFragment(){
        switchFragment(SplashFragment.newInstance());
    }

    public void openLoginFragment(){
        switchFragment(LoginFragment.newInstance());
    }

}
