package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 6/25/2016.
 */
public class DirectoryItem extends AndroidModel {

    @SerializedName("title")
    public String title;

    public boolean isSelected = false;

    @SerializedName("date")
    public Date date;

    @SerializedName("icon")
    public Icon icon;

    @SerializedName("establishments")
    public Establishments establishments;

    public class Date {

        @SerializedName("data")
        public DateItem data;
    }

    public class Icon {
        @SerializedName("data")
        public Data data;

        public class Data {
            @SerializedName("path")
            public String path;

            @SerializedName("directory")
            public String directory;

            @SerializedName("full_path")
            public String full_path;

            @SerializedName("thumb_path")
            public String thumb_path;
        }
    }

    public class Establishments {
        @SerializedName("data")
        public List<EstablishmentItem> data;
    }
}
