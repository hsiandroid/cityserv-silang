package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class AppSettingsItem extends AndroidModel{

    @SerializedName("version_name")
    public String version_name;

    @SerializedName("major_version")
    public int major_version;

    @SerializedName("minor_version")
    public int minor_version;

    @SerializedName("changelogs")
    public String changelogs;
}
