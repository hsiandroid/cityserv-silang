package ph.cityserv.oneserve.silang.android.activity;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.fragment.landing.RegisterFragment;
import ph.cityserv.oneserve.silang.android.fragment.registration.ForgotPasswordFragment;
import ph.cityserv.oneserve.silang.android.fragment.registration.ResetPasswordFragment;
import ph.cityserv.oneserve.silang.android.fragment.registration.SignUpFragment;
import ph.cityserv.oneserve.silang.android.route.RouteActivity;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class RegistrationActivity extends RouteActivity implements View.OnClickListener {
    public static final String TAG = RegistrationActivity.class.getName().toString();

    @BindView(R.id.mainIconIV)      View mainIconIV;
    @BindView(R.id.mainTitleTXT)    TextView mainTitleTXT;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_registration;
    }

    @Override
    public void onViewReady() {
        mainIconIV.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "signup":
                openSignUpFragment();
                break;
            case "forgot":
                openForgotPasswordFragment();
                break;
            case "reset":
                openResetPasswordFragment(getFragmentBundle().getString("email"));
                break;
        }
    }

    public void openSignUpFragment(){
        switchFragment(RegisterFragment.newInstance());
    }

    public void openForgotPasswordFragment(){
        switchFragment(ForgotPasswordFragment.newInstance());
    }

    public void openResetPasswordFragment(String email){
        switchFragment(ResetPasswordFragment.newInstance(email));
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainIconIV:
                onBackPressed();
        }
    }

}
