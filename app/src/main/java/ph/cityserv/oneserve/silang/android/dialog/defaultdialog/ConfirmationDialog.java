package ph.cityserv.oneserve.silang.android.dialog.defaultdialog;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import icepick.State;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseDialog;

public class ConfirmationDialog extends BaseDialog {
	public static final String TAG = ConfirmationDialog.class.getName().toString();

    @State int icon;
    @State String title;
    @State String description;
    @State String positiveButton;
    @State String negativeButton;

    @BindView(R.id.iconIV)                  ImageView iconIV;
    @BindView(R.id.descriptionTXT)          TextView descriptionTXT;
    @BindView(R.id.positiveBTN)             TextView positiveBTN;
    @BindView(R.id.negativeBTN)             TextView negativeBTN;

    private View.OnClickListener positiveClickListener;
    private View.OnClickListener negativeClickListener;

	public static ConfirmationDialog Builder() {
		ConfirmationDialog fragment = new ConfirmationDialog();
		return fragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_confirmation;
	}

	@Override
	public void onViewReady() {
        if(icon != 0){
            iconIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), icon));
        }

        if(description != null){
            descriptionTXT.setText(description);
        }

        if(positiveBTN != null){
            positiveBTN.setText(positiveButton);
        }
        if(negativeBTN != null){
            negativeBTN.setText(negativeButton);
        }

        if(positiveClickListener != null){
            positiveBTN.setOnClickListener(positiveClickListener);
        }
        if(negativeClickListener != null){
            negativeBTN.setOnClickListener(negativeClickListener);
        }
	}

	@Override
	public void onStart() {
		super.onStart();
        setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	public ConfirmationDialog setIcon(int icon){
		this.icon = icon;
		return this;
	}

	public ConfirmationDialog setTitle(String title){
        this.title = title;
		return this;
	}

	public ConfirmationDialog setDescription(String description){
        this.description = description;
		return this;
	}

	public ConfirmationDialog setPositiveButtonText(String positiveButton){
        this.positiveButton = positiveButton;
		return this;
	}

	public ConfirmationDialog setNegativeButtonText(String negativeButton){
        this.negativeButton = negativeButton;
		return this;
	}

	public ConfirmationDialog setPositiveButtonClickListener(View.OnClickListener positiveClickListener){
        this.positiveClickListener = positiveClickListener;
		return this;
	}

	public ConfirmationDialog setNegativeButtonClickListener(View.OnClickListener negativeClickListener){
        this.negativeClickListener = negativeClickListener;
		return this;
	}

	public void build(FragmentManager manager){
        show(manager, TAG);
	}
}


