package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 8/12/2017.
 */

public class ReportTypeModel extends AndroidModel {


    @SerializedName("title")
    public String title;

    @SerializedName("date")
    public Date date;

    public static class Date {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("date_db")
            public String dateDb;

            @SerializedName("month_year")
            public String monthYear;

            @SerializedName("time_passed")
            public String timePassed;

            @SerializedName("timestamp")
            public Timestamp timestamp;

            public static class Timestamp {
                @SerializedName("date")
                public String date;

                @SerializedName("timezone_type")
                public int timezoneType;

                @SerializedName("timezone")
                public String timezone;
            }
        }
    }
}
