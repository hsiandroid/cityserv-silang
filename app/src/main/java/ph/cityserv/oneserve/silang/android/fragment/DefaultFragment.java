package ph.cityserv.oneserve.silang.android.fragment;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;
import io.realm.Realm;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.db.SampleRealModel;
import ph.cityserv.oneserve.silang.server.request.Auth;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.server.transformer.BaseTransformer;

public class DefaultFragment extends BaseFragment {
    public static final String TAG = DefaultFragment.class.getName().toString();

    @BindView(R.id.sampleTXT) TextView sampleTXT;
    @BindView(R.id.sampleIMG)
    ImageView sampleIMG;

    @State String path = "https://avisassets.abgemea.com/.imaging/featureImageLarge/dms/DMS/local/ZA/fleet/fleet-page/luxury-cars-feature.jpg";

    public static DefaultFragment newInstance() {
        DefaultFragment fragment = new DefaultFragment();
        return fragment;
    }

    @Override
    public void onViewReady() {
        sampleTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Auth.getDefault(getContext()).login("jomar.olaybal@gmail.com", "asdfghjkl");
            }
        });


        Picasso.with(getContext())
                .load(path)
                .fit()
                .centerCrop()
                .into(sampleIMG);

//        UserModel userModel = UserModel.getModel();
//        userModel.name = "Jilmer";
//        userModel.save();
//
//        sampleTXT.setText(UserModel.getModel().findOrNew(2).name);

        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                SampleRealModel sampleRealModel = realm.createObject(SampleRealModel.class);
                sampleRealModel.setName("Marivic");
                sampleRealModel.setAge(2);
            }
        });
        SampleRealModel sampleRealModel = realm.where(SampleRealModel.class).equalTo("age", 2).findFirst();
        sampleTXT.setText(sampleRealModel.getName());
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_default;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
}
