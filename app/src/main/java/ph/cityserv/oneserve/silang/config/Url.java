package ph.cityserv.oneserve.silang.config;

import ph.cityserv.oneserve.silang.vendor.android.java.SecurityLayer;

/**
 * Created by BCTI 3 on 8/3/2017.
 */

public class Url extends SecurityLayer {

    public static String api(){
        return decrypt("aHR0cDovL3NpbGFuZy5jaXR5c2Vydi5vbmVzZXJ2LnBo");
    }

    public static String token(){
        return decrypt("YmFzZTY0OkxocDFCQjNtczdXVmdYQktiT2tTbURRYUlZbENRdS9zWGZWMVRwMndvUjA9");
    }

    public static String getLogin(){
        return decrypt("L2FwaS9hdXRoL2xvZ2luLmpzb24=");
    }

    public static String getOfficials(){
        return decrypt("L2FwaS9jaXR5L29mZmljaWFscy5qc29u");
    }

    public static String getPhoto(){ return decrypt("L2FwaS9jaXR5L2dhbGxlcnkuanNvbg=="); }

    public static String getSignUp(){ return decrypt("L2FwaS9hdXRoL3JlZ2lzdGVyLmpzb24="); }

    public static String getForgotPassword(){ return decrypt("L2FwaS9hdXRoL2ZvcmdvdC1wYXNzd29yZC5qc29u");
    }

    public static String getResetPassword(){
        return decrypt("L2FwaS9hdXRoL3Jlc2V0LXBhc3N3b3JkLmpzb24=");
    }

    public static String getLogout(){
        return decrypt("L2FwaS9hdXRoL2xvZ291dC5qc29u");
    }

    public static String getAllWidget(){
        return decrypt("L2FwaS93aWRnZXQvYWxsLmpzb24=");
    }

    public static String getAllShow(){
        return decrypt("L2FwaS93aWRnZXQvc2hvdy5qc29u");
    }

    public static String getIDStatus(){ return decrypt("L2FwaS9jYXJkL3N0YXR1cy5qc29u"); }

    public static String getRequestID(){ return decrypt("L2FwaS9jYXJkL3JlcXVlc3QuanNvbg=="); }

    public static String getAllArticle(){
        return decrypt("L2FwaS9hcnRpY2xlL2FsbC5qc29u");
    }

    public static String getRecentArticle(){
        return decrypt("L2FwaS9hcnRpY2xlL3JlY2VudC5qc29u");
    }

    public static String getAllService(){
        return decrypt("L2FwaS9zZXJ2aWNlL2FsbC5qc29u");
    }

    public static String getAllDirectory(){
        return decrypt("L2FwaS9kaXJlY3RvcnkvYWxsLmpzb24=");
    }

    public static String getMyMoment(){
        return decrypt(" L2FwaS9tb21lbnQvbXktbW9tZW50cy5qc29u");
    }

    public static String getCreateMoment(){
        return decrypt("L2FwaS9tb21lbnQvY3JlYXRlLmpzb24=");
    }

    public static String getDeleteMoment(){
        return decrypt("L2FwaS9tb21lbnQvZGVsZXRlLmpzb24=");
    }

    public static String getCreateCitizenRequest(){ return decrypt("L2FwaS9jaXRpemVuLXJlcXVlc3QvY3JlYXRlLmpzb24="); }

    public static String getShowCitizenRequest(){ return decrypt("L2FwaS9jaXRpemVuLXJlcXVlc3Qvc2hvdy5qc29u"); }

    public static String getMyRequestCitizenRequest(){ return decrypt("L2FwaS9jaXRpemVuLXJlcXVlc3QvbXktcmVxdWVzdHMuanNvbg=="); }

    public static String getAllNotification(){ return decrypt("L2FwaS9ub3RpZmljYXRpb24vYWxsLmpzb24="); }

    public static String getFieldUser(){
        return decrypt("L2FwaS91c2VyL3VwZGF0ZS9maWVsZC5qc29u");
    }

    public static String getPasswordUser(){ return decrypt("L2FwaS91c2VyL3VwZGF0ZS9wYXNzd29yZC5qc29u"); }

    public static String getCreateReport(){ return decrypt("L2FwaS9jaXRpemVuLXJlcG9ydC9jcmVhdGUuanNvbg=="); }

    public static String getMyReport(){ return decrypt("L2FwaS9jaXRpemVuLXJlcG9ydC9teS1yZXBvcnRzLmpzb24="); }

    public static String getShowReport(){ return decrypt("L2FwaS9jaXRpemVuLXJlcG9ydC9zaG93Lmpzb24="); }

    public static String getReportType(){ return decrypt("L2FwaS9jaXRpemVuLXJlcG9ydC90eXBlLmpzb24="); }

    public static String getJobList(){ return decrypt("L2FwaS9qb2ItaGlyaW5nL2FsbC5qc29u"); }

    public static String getRefreshToken(){ return decrypt("L2FwaS9hdXRoL3JlZnJlc2gtdG9rZW4uanNvbg=="); }

    public static String getApplyJob(){ return decrypt("L2FwaS9qb2ItaGlyaW5nL2FwcGx5Lmpzb24="); }

    public static String getPeso(){ return decrypt("L2FwaS9hcHAtc2V0dGluZy9wZXNvLmpzb24="); }

    public static String getAllYouth(){ return decrypt("L2FwaS95b3V0aC1hZmZhaXIvYWxsLmpzb24="); }

    public static String getAllLeipo(){ return decrypt("L2FwaS9kb2luZy1idXNpbmVzcy9hbGwuanNvbg=="); }

    public static String getAllTourism(){ return decrypt("L2FwaS90b3VyaXNtL2FsbC5qc29u"); }

    public static String getServiceDetail(){ return decrypt("L2FwaS9zZXJ2aWNlL3Nob3cuanNvbg=="); }

}
