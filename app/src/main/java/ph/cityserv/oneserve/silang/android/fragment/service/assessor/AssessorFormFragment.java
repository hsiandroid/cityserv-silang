package ph.cityserv.oneserve.silang.android.fragment.service.assessor;

import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;

public class AssessorFormFragment extends BaseFragment {

    public static final String TAG = AssessorFormFragment.class.getName().toString();

    public static AssessorFormFragment newInstance() {
        AssessorFormFragment fragment = new AssessorFormFragment();
        return fragment;
    }

    @BindView(R.id.trackingNumberET)            EditText trackingNumberET;
    @BindView(R.id.typeSPR)                     Spinner typeSPR;
    @BindView(R.id.nameET)                      EditText nameET;
    @BindView(R.id.addressET)                   EditText addressET;
    @BindView(R.id.telNoET)                     EditText telNoET;
    @BindView(R.id.cellphoneET)                 EditText cellphoneET;
    @BindView(R.id.ownerNameET)                 EditText ownerNameET;
    @BindView(R.id.propertyLocationET)          EditText propertyLocationET;
    @BindView(R.id.pinET)                       EditText pinET;
    @BindView(R.id.street_addressET)            EditText street_addressET;
    @BindView(R.id.barangayET)                  EditText barangayET;
    @BindView(R.id.zoneSPR)                     Spinner zoneSPR;
    @BindView(R.id.submitBTN)                   TextView submitBTN;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_assessor_form;
    }

    @Override
    public void onViewReady() {


    }
}
