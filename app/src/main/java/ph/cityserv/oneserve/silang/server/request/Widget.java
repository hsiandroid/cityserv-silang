package ph.cityserv.oneserve.silang.server.request;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import ph.cityserv.oneserve.silang.config.Keys;
import ph.cityserv.oneserve.silang.config.Url;
import ph.cityserv.oneserve.silang.data.model.server.WidgetItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.request.APIResponse;
import ph.cityserv.oneserve.silang.vendor.server.request.BaseRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by BCTI 3 on 8/16/2017.
 */

public class Widget extends BaseRequest {
    public Widget(Context context) {
        super(context);
    }

    private String include = "date,children";

    public static Widget getDefault(Context context) {
        Widget fragment = new Widget(context);
        return fragment;
    }

    public void all(SwipeRefreshLayout swipeRefreshLayout, String display) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<WidgetItem>>(getContext()) {
            @Override
            public Call<CollectionTransformer<WidgetItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionAPI(Url.getAllWidget(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new WidgetAllResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.DISPLAY_SCREEN, display)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .execute();
    }

    public void show(SwipeRefreshLayout swipeRefreshLayout, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<WidgetItem>>(getContext()) {
            @Override
            public Call<SingleTransformer<WidgetItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleAPI(Url.getAllShow(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new WidgetShowResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.WIDGET_ID, id)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .execute();
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<WidgetItem>> requestCollectionAPI(@Path("p") String p, @Header("Authorization") String authorization,  @Part List<MultipartBody.Part> parts);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<WidgetItem>> requestSingleAPI(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);
    }

    public class WidgetAllResponse extends APIResponse<CollectionTransformer<WidgetItem>> {
        public WidgetAllResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class WidgetShowResponse extends APIResponse<SingleTransformer<WidgetItem>> {
        public WidgetShowResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
