package ph.cityserv.oneserve.silang.android.fragment.main;

import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.AssessorActivity;
import ph.cityserv.oneserve.silang.data.model.server.RequestItem;
import ph.cityserv.oneserve.silang.data.model.server.ServiceItem;
import ph.cityserv.oneserve.silang.data.model.server.SubServiceItem;
import ph.cityserv.oneserve.silang.server.request.CitizenRequest;
import ph.cityserv.oneserve.silang.server.request.Service;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.ToastMessage;
import ph.cityserv.oneserve.silang.vendor.android.java.WrapContentWebView;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;

public class CitizenCardFragment extends BaseFragment {

    public static final String TAG = CitizenCardFragment.class.getName().toString();

    private AssessorActivity assessorActivity;

    private ServiceItem serviceItem;

    @BindView(R.id.requirementTXT)          TextView requirementTXT;
    @BindView(R.id.requirementWV)           WrapContentWebView requirementWV;


    public static CitizenCardFragment newInstance() {
        CitizenCardFragment fragment = new CitizenCardFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_requirement;
    }

    @Override
    public void onViewReady() {
        assessorActivity = (AssessorActivity) getActivity();
        Service.getDefault(getContext()).detail(7);
    }

    @Subscribe
    public void onResponse(Service.DetailResponse responseData){
        SingleTransformer<ServiceItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            serviceItem = singleTransformer.data;
            assessorActivity.setTitle(serviceItem.title);
            requirementTXT.setText(serviceItem.title);
            requirementWV.loadDataWithBaseURL(null, serviceItem.info.data.content, "text/html", "utf-8", null);
        }else{
            ToastMessage.show(getActivity(), "Service unavailable.", ToastMessage.Status.FAILED);
            assessorActivity.finish();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();

    }
}
