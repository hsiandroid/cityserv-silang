package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 6/25/2016.
 */
public class RequestItem extends AndroidModel {

    @SerializedName("user_id")
    public int user_id;

    @SerializedName("code")
    public String code;

    @SerializedName("tracking_number")
    public String tracking_number;

    @SerializedName("title")
    public String title;

    @SerializedName("name")
    public String name;

    @SerializedName("email")
    public String email;

    @SerializedName("phone")
    public String phone;

    @SerializedName("mobile")
    public String mobile;

    @SerializedName("target_table")
    public String target_table;

    @SerializedName("type")
    public String type;

    @SerializedName("sub_type")
    public String sub_type;

    @SerializedName("remarks")
    public String remarks;

    @SerializedName("status")
    public String status;

    @SerializedName("date")
    public Date date;

    @SerializedName("info")
    public Info info;

    @SerializedName("survey")
    public Survey survey;

    @SerializedName("duration")
    public String duration;

    public class Date {

        @SerializedName("data")
        public DateItem data;
    }

    public class Survey {

        @SerializedName("data")
        public Data data;

        public class Data {

            @SerializedName("survey_exist")
            public boolean survey_exist;
        }
    }

    public class Info {

        @SerializedName("data")
        public Data data;

        public class Data {

            @SerializedName("more")
            public More more;

            public class More {

                @SerializedName("data")
                public List<RequestInfoItem> data;
            }
        }
    }
}
