package ph.cityserv.oneserve.silang.vendor.android.java;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import ph.cityserv.oneserve.silang.R;


/**
 * Created by BCTI 3 on 4/20/2017.
 */

public class PasswordEditTextManager {

    public static void addShowPassword(final Context context, final EditText editText, final ImageView imageView) {
        editText.setSelected(false);
        format(context, editText, imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setSelected(!editText.isSelected());
                format(context, editText, imageView);
            }
        });
    }

    private static void format(Context context, EditText editText, ImageView imageView) {
        int selection = editText.getSelectionStart();
        if (editText.isSelected()) {
            editText.setTransformationMethod(null);
            imageView.setImageDrawable(ActivityCompat.getDrawable(context, R.drawable.icon_show_password));
        } else {
            editText.setTransformationMethod(new AsteriskPasswordTransformationMethod());
            imageView.setImageDrawable(ActivityCompat.getDrawable(context, R.drawable.icon_hide_password));
        }

        editText.setSelection(selection);
    }

    public static void addShowPassword(final Context context, final EditText editText, final CheckBox checkBox) {
        editText.setSelected(false);
        format(context, editText, checkBox);
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setSelected(!editText.isSelected());
                format(context, editText, checkBox);
            }
        });
    }

    private static void format(Context context, EditText editText, CheckBox checkBox) {
        int selection = editText.getSelectionStart();
        if (editText.isSelected()) {
            editText.setTransformationMethod(null);
            checkBox.setChecked(true);
        } else {
            editText.setTransformationMethod(new AsteriskPasswordTransformationMethod());
            checkBox.setChecked(false);
        }

        editText.setSelection(selection);
    }

    public static void usernameFormat(final EditText editText){
        editText.addTextChangedListener(new TextWatcher() {

            public void beforeTextChanged(CharSequence s,int start,int count, int after){

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void afterTextChanged(Editable s){
                String result = s.toString().replaceAll(" ", "_").toLowerCase();
                if (!s.toString().equals(result)) {
                    editText.setText(result);
                    editText.setSelection(result.length());
                }
            }
        });
    }
}
