package ph.cityserv.oneserve.silang.vendor.server.transformer;

import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.data.model.server.ErrorItem;

/**
 * Created by BCTI 3 on 8/3/2017.
 */

public class BaseTransformer {

    @SerializedName("msg")
    public String msg = "Internal Server Error";

    @SerializedName("status")
    public Boolean status = false;

    @SerializedName("status_code")
    public String status_code = "RETROFIT_FAILED";

    @SerializedName("token")
    public String token = "";

    @SerializedName("new_token")
    public String new_token = "";

    @SerializedName("has_morepage")
    public Boolean has_morepage = false;

    @SerializedName("first_login")
    public Boolean first_login = false;

    @SerializedName("errors")
    public ErrorItem errors;

    public boolean hasRequirements(){
        return errors!= null;
    }

//    public boolean hasRequirements(){
//        return false;
//    }
}
