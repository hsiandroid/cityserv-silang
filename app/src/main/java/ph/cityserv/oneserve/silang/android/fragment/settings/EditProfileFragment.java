package ph.cityserv.oneserve.silang.android.fragment.settings;

import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import butterknife.BindView;
import icepick.State;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.SettingsActivity;
import ph.cityserv.oneserve.silang.android.dialog.ImagePickerDialog;
import ph.cityserv.oneserve.silang.data.model.server.UserItem;
import ph.cityserv.oneserve.silang.data.preference.CountryData;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;

public class EditProfileFragment extends BaseFragment implements View.OnClickListener,
//        CountryDialog.CountryPickerListener,
        ImagePickerDialog.ImageCallback{

    public static final String TAG = EditProfileFragment.class.getName().toString();

    private SettingsActivity settingsActivity;

    @BindView(R.id.nameET)              EditText nameET;
    @BindView(R.id.street_addressET)    EditText street_addressET;
    @BindView(R.id.cityET)              EditText cityET;
    @BindView(R.id.stateET)             EditText stateET;
    @BindView(R.id.countryTXT)          TextView countryTXT;
    @BindView(R.id.usernameET)          EditText usernameET;
    @BindView(R.id.emailET)             EditText emailET;
    @BindView(R.id.contactET)           EditText contactET;
    @BindView(R.id.passwordET)          EditText passwordET;
    @BindView(R.id.showPasswordBTN)     ImageView showPasswordBTN;
    @BindView(R.id.maleRBTN)            RadioButton maleRBTN;
    @BindView(R.id.femaleRBTN)          RadioButton femaleRBTN;
    @BindView(R.id.saveBTN)             View saveBTN;
    @BindView(R.id.avatarCIV)           ImageView avatarCIV;
    @BindView(R.id.changeAvatarTXT)     View changeAvatarTXT;
    @BindView(R.id.scrollView)          ScrollView scrollView;

    @State  File file;
    private CountryData.Country countryData;

    public EditProfileFragment() {
    }

    public static EditProfileFragment newInstance() {
        EditProfileFragment fragment = new EditProfileFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_edit_profile;
    }

    @Override
    public void onViewReady() {
       settingsActivity = (SettingsActivity) getActivity();
        displayData(UserData.getUserItem());
        settingsActivity.setTitle("Edit Profile");
        saveBTN.setOnClickListener(this);
        changeAvatarTXT.setOnClickListener(this);
        avatarCIV.setOnClickListener(this);
        countryTXT.setOnClickListener(this);

        setupCountryTextView();

//        PasswordEditTextManager.addShowPassword(getContext(), passwordET, showPasswordBTN);

    }

    private void setupCountryTextView(){

        countryTXT.setInputType(InputType.TYPE_NULL);
        countryTXT.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    showCountryPicker();

            }
        });
    }

    private void showCountryPicker(){
//        CountryDialog.newInstance(this).show(getChildFragmentManager(), CountryDialog.TAG);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.saveBTN:
                attemptSignUp();
                break;
            case R.id.changeAvatarTXT:
            case R.id.avatarCIV:
                ImagePickerDialog.newInstance("Change Avatar", true, this).show(getChildFragmentManager(), ImagePickerDialog.TAG);
                break;
            case R.id.countryTXT:
                countryTXT.setError(null);
                showCountryPicker();
                break;
        }
    }

    private void displayData(UserItem userItem){
        nameET.setText(userItem.info.data.name);
        emailET.setText(userItem.email);
        usernameET.setText(userItem.username);
        if(userItem.info.data.gender == null){
            userItem.info.data.gender = "male";
        }
        maleRBTN.setChecked(userItem.info.data.gender.equals("male"));
        femaleRBTN.setChecked(userItem.info.data.gender.equals("female"));

        if(userItem.info != null){
            contactET.setText(userItem.info.data.contactNumber);
//            street_addressET.setText(userItem.info.data.street_address);
//            cityET.setText(userItem.info.data.city);
//            stateET.setText(userItem.info.data.state);
        }
//        countryData = CountryData.getCountryDataByCode1(userItem.info.data.country);
//        countryTXT.setText(countryData.country);


        Picasso.with(getContext())
                .load(userItem.avatar.data.fullPath)
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .into(avatarCIV);
    }

    private void attemptSignUp(){
//        ProfileEditRequest profileEditRequest = new ProfileEditRequest(getContext());
//
//        if(file != null){
//            profileEditRequest.addPart(Variable.server.key.FILE, file);
//        }
//
//        profileEditRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Updating Profile...", false, false))
//                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addRequestBodyParameters(Variable.server.key.INCLUDE, "info,social,statistics")
//                .addRequestBodyParameters(Variable.server.key.NAME, nameET.getText().toString())
//                .addRequestBodyParameters(Variable.server.key.CONTACT_NUMBER, contactET.getText().toString())
//                .addRequestBodyParameters(Variable.server.key.USERNAME, usernameET.getText().toString())
//                .addRequestBodyParameters(Variable.server.key.EMAIL, emailET.getText().toString())
//                .addRequestBodyParameters(Variable.server.key.GENDER, femaleRBTN.isChecked() ? "female" :  "male")
//                .addRequestBodyParameters(Variable.server.key.STREET_ADDRESS, street_addressET.getText().toString())
//                .addRequestBodyParameters(Variable.server.key.CITY, cityET.getText().toString())
//                .addRequestBodyParameters(Variable.server.key.STATE, stateET.getText().toString())
////                .addRequestBodyParameters(Variable.server.key.COUNTRY, countryData.code1)
//                .addRequestBodyParameters(Variable.server.key.PASSWORD, passwordET.getText().toString())
//                .execute();
    }


    @Override
    public void result(File file) {
        this.file = file;
        Picasso.with(getContext())
                .load(file)
                .into(avatarCIV);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

//    @Subscribe
//    public void onResponse(ProfileEditRequest.ServerResponse responseData) {
//        UserSingleTransformer userSingleTransformer = responseData.getData(UserSingleTransformer.class);
//        if(userSingleTransformer.status){
//            passwordET.setText("");
//            displayData(userSingleTransformer.data);
//            UserData.insert(userSingleTransformer.data);
//            scrollView.smoothScrollTo(0, 0);
//            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.SUCCESS);
//        }else{
//            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.FAILED);
//            if(userSingleTransformer.hasRequirements()){
//                ErrorResponseManger.first(nameET, userSingleTransformer.requires.name);
//                ErrorResponseManger.first(street_addressET, userSingleTransformer.requires.street_address);
//                ErrorResponseManger.first(cityET, userSingleTransformer.requires.city);
//                ErrorResponseManger.first(stateET, userSingleTransformer.requires.state);
//                ErrorResponseManger.first(countryTXT, userSingleTransformer.requires.country);
//                ErrorResponseManger.first(emailET, userSingleTransformer.requires.email);
//                ErrorResponseManger.first(passwordET, userSingleTransformer.requires.password);
//            }
//        }
//    }

//    @Override
//    public void onSelectCountry(CountryData.Country country) {
//        countryData = country;
//        countryTXT.setText(country.country);
//    }
}
