package ph.cityserv.oneserve.silang.android.fragment.main;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;
import io.realm.Realm;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.MainActivity;
import ph.cityserv.oneserve.silang.android.dialog.CardDetailDialog;
import ph.cityserv.oneserve.silang.android.dialog.WebViewDialog;
import ph.cityserv.oneserve.silang.data.model.db.SampleRealModel;
import ph.cityserv.oneserve.silang.data.model.server.RequestIDItem;
import ph.cityserv.oneserve.silang.server.request.Auth;
import ph.cityserv.oneserve.silang.server.request.LGU;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.ToastMessage;
import ph.cityserv.oneserve.silang.vendor.server.transformer.BaseTransformer;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;

public class MayorsActionFragment extends BaseFragment implements View.OnClickListener{
    public static final String TAG = MayorsActionFragment.class.getName().toString();

    @BindView(R.id.requestInfoBTN)              LinearLayout requestInfoBTN;
    @BindView(R.id.createRequestBTN)            LinearLayout createRequestBTN;
    @BindView(R.id.appointmentBTN)              LinearLayout appointmentBTN;
    @BindView(R.id.newsEventBTN)                LinearLayout newsEventBTN;
    @BindView(R.id.applyForIdBTN)               LinearLayout applyForIdBTN;

    private MainActivity mainActivity;

    public static MayorsActionFragment newInstance() {
        MayorsActionFragment fragment = new MayorsActionFragment();
        return fragment;
    }

    @Override
    public void onViewReady() {
       mainActivity = (MainActivity) getContext();
       mainActivity.setTitle(getString(R.string.nav_action));
       newsEventBTN.setOnClickListener(this);
       applyForIdBTN.setOnClickListener(this);
       requestInfoBTN.setOnClickListener(this);
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_mayors_action;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(LGU.IDResponse responseData) {
        SingleTransformer<RequestIDItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            CardDetailDialog.newInstance(singleTransformer.data).show(getFragmentManager(), TAG);
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
            mainActivity.openCitizenIDFragment();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.newsEventBTN:
                mainActivity.openNewsFragment();
                break;
            case R.id.applyForIdBTN:
                LGU.getDefault(getContext()).validateID();
                break;
            case R.id.requestInfoBTN:
                WebViewDialog.newInstance("https://docs.google.com/document/d/1Egux6BGFArIyFcCplroa0v-0tmu6reIKddkuK94p3-M/edit").show(getChildFragmentManager(), WebViewDialog.TAG);
                break;
        }
    }
}
