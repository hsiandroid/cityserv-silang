package ph.cityserv.oneserve.silang.data.model.app;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class MenuItem extends AndroidModel {
    public String name;
    public String description;
    public int icon;
    public int background;
    public String code;
}
