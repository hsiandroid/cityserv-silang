package ph.cityserv.oneserve.silang.android.dialog;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.PopupMenu;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.MomentItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseDialog;
import ph.cityserv.oneserve.silang.vendor.android.java.ImageManager;

public class MomentDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = MomentDialog.class.getName().toString();

	private MomentItem momentItem;
	private File imageFile;

	@BindView(R.id.titleBarTXT)					TextView titleBarTXT;
	@BindView(R.id.dateTXT)						TextView dateTXT;
	@BindView(R.id.contentTXT)					TextView contentTXT;
	@BindView(R.id.shareView)					View shareView;
	@BindView(R.id.imageRIV)					ImageView imageRIV;
	@BindView(R.id.backBTN)						ImageView backBTN;
	@BindView(R.id.titleBarOptionBTN)			ImageView titleBarOptionBTN;

	public static MomentDialog newInstance(MomentItem momentItem) {
		MomentDialog fragment = new MomentDialog();
		fragment.momentItem = momentItem;
		return fragment;
	}
	
	@Override
	public int onLayoutSet() {
		return R.layout.dialog_moment;
	}

	@Override
	public void onViewReady() {
		titleBarTXT.setText("MOMENT DETAILS");

		backBTN.setOnClickListener(this);
		shareView.setOnClickListener(this);
		titleBarOptionBTN.setOnClickListener(this);

		momentData(momentItem);
	}

	private void momentData(MomentItem momentItem){
		Picasso.with(getContext())
				.load(momentItem.info.data.full_path)
				.placeholder(R.drawable.placeholder_logo)
				.error(R.drawable.placeholder_logo)
				.into(imageRIV);

		contentTXT.setText(Html.fromHtml(momentItem.info.data.content));
		dateTXT.setText(momentItem.date.data.time_passed);
	}

	public void shareImage(String image){
		if(imageFile == null){
			final ProgressDialog progressDialog = ProgressDialog.show(getContext(), "", "Preparing to share.", true, false);
			Picasso.with(getContext())
					.load(image)
					.into(new Target() {
						@Override
						public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
							ImageManager.getFileFromBitmap(getContext(),bitmap, "temp", new ImageManager.Callback() {
								@Override
								public void success(File file) {
									if(file != null){
										imageFile = file;
										progressDialog.cancel();
										shareIntent(file);
									}
								}
							});
						}
						@Override
						public void onBitmapFailed(Drawable errorDrawable) {
							progressDialog.cancel();
						}

						@Override
						public void onPrepareLoad(Drawable placeHolderDrawable) {
							progressDialog.cancel();
						}
					});
		}else{
			shareIntent(imageFile);
		}
	}

	private void shareIntent(File file){
		Uri imageUri = Uri.fromFile(file);
		Intent sharingIntent = new Intent(Intent.ACTION_SEND);
		sharingIntent.setType("image/*");
		sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "My IloIlo Moment");
		sharingIntent.putExtra(Intent.EXTRA_TEXT, "My IloIlo Moment");
		sharingIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
		startActivity(Intent.createChooser(sharingIntent, "Where would you like to share this photo?"));
	}

	private void showOtherOption(View v){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			PopupMenu popup = new PopupMenu(getContext(), v);
			popup.getMenuInflater().inflate(R.menu.pop_menu_moment, popup.getMenu());

			popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem menuItem) {
					int id = menuItem.getItemId();
					if(id == R.id.message_menu_remove){

					}else if(id == R.id.message_menu_share){
						shareImage(momentItem.info.data.full_path);
					}
					return true;
				}
			});
			popup.show();
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){

			case R.id.shareView:
				shareImage(momentItem.info.data.full_path);
				break;
			case R.id.backBTN:
				dismiss();
				break;
			case R.id.titleBarOptionBTN:
				showOtherOption(v);
				break;
		}
	}
}
