package ph.cityserv.oneserve.silang.android.dialog;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.RequestIDItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseDialog;

public class CardDetailDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = CardDetailDialog.class.getName().toString();

	@BindView(R.id.lguIDTXT) 				TextView lguIDTXT;
	@BindView(R.id.nameTXT) 				TextView nameTXT;
	@BindView(R.id.birtDateTXT) 			TextView birthDateTXT;
	@BindView(R.id.nationalityTXT) 			TextView nationalityTXT;
	@BindView(R.id.occupationTXT) 			TextView occupationTXT;
	@BindView(R.id.addressTXT) 				TextView addressTXT;
	@BindView(R.id.emailTXT) 				TextView emailTXT;
	@BindView(R.id.contactNumberTXT) 		TextView contactNumberTXT;
	@BindView(R.id.cardDetailIV) 			ImageView cardDetailIV;
	@BindView(R.id.exitBTN) 				ImageView exitBTN;

	private  RequestIDItem requestIDItem;

	public static CardDetailDialog newInstance(RequestIDItem requestIDItem) {
		CardDetailDialog dialog = new CardDetailDialog();
		dialog.requestIDItem = requestIDItem;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_card_detail;
	}

	@Override
	public void onViewReady() {
		exitBTN.setOnClickListener(this);
		nameTXT.setText(requestIDItem.displayName);
		birthDateTXT.setText(requestIDItem.birthdate);
		nationalityTXT.setText(requestIDItem.nationality);
		occupationTXT.setText(requestIDItem.occupation);
		addressTXT.setText(requestIDItem.brgy);
		emailTXT.setText(requestIDItem.contactEmail);
		contactNumberTXT.setText(requestIDItem.contactNumber);

		Picasso.with(getContext()).load("http://silang.cityserv.oneserv.ph/uploads/file/20180814/resized/cmvt6rd6yd081420181227.jpg").fit().into(cardDetailIV);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View view) {
		switch(view.getId()){
			case R.id.exitBTN:
				dismiss();
				break;
		}
	}
}
