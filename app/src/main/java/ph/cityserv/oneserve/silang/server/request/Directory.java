package ph.cityserv.oneserve.silang.server.request;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import ph.cityserv.oneserve.silang.config.Keys;
import ph.cityserv.oneserve.silang.config.Url;
import ph.cityserv.oneserve.silang.data.model.server.DirectoryItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.request.APIResponse;
import ph.cityserv.oneserve.silang.vendor.server.request.BaseRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by BCTI 3 on 8/16/2017.
 */

public class Directory extends BaseRequest {
    public Directory(Context context) {
        super(context);
    }

    private String include = "date,icon,establishments";

    public static Directory getDefault(Context context) {
        Directory fragment = new Directory(context);
        return fragment;
    }

    public void all(SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<DirectoryItem>>(getContext()) {
            @Override
            public Call<CollectionTransformer<DirectoryItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestAPI(Url.getAllDirectory(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new DirectoryResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .execute();
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<DirectoryItem>> requestAPI(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);
    }

    public class DirectoryResponse extends APIResponse<CollectionTransformer<DirectoryItem>> {
        public DirectoryResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
