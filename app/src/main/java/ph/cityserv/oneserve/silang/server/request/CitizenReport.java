package ph.cityserv.oneserve.silang.server.request;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import ph.cityserv.oneserve.silang.config.Keys;
import ph.cityserv.oneserve.silang.config.Url;
import ph.cityserv.oneserve.silang.data.model.server.CitizenReportItem;
import ph.cityserv.oneserve.silang.data.model.server.ReportTypeModel;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.request.APIResponse;
import ph.cityserv.oneserve.silang.vendor.server.request.BaseRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by BCTI 3 on 8/16/2017.
 */

public class CitizenReport extends BaseRequest {

    public CitizenReport(Context context) {
        super(context);
    }

    private String include = "info,date,user";

    public static CitizenReport getDefault(Context context) {
        CitizenReport fragment = new CitizenReport(context);
        return fragment;
    }

    public APIRequest myReport(SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<CitizenReportItem>>(getContext()) {
            @Override
            public Call<CollectionTransformer<CitizenReportItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionAPI(Url.getMyReport(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new MyReportResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .setPerPage(10)
                .showSwipeRefreshLayout(true);
        return apiRequest;
    }

    public void delete(int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<CitizenReportItem>>(getContext()) {
            @Override
            public Call<SingleTransformer<CitizenReportItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleAPI(Url.getDeleteMoment(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new DeleteReportResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.REPORT_ID, id)
                .showDefaultProgressDialog("Deleting...")
                .execute();
    }

    public void create(File file, String content, String type) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<CitizenReportItem>>(getContext()) {
            @Override
            public Call<SingleTransformer<CitizenReportItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleAPI(Url.getCreateReport(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new CreateReportResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.CONTENT, content)
                .addParameter(Keys.TYPE, type)
                .addParameter(Keys.FILE, file)
                .showDefaultProgressDialog("Uploading...")
                .execute();

    }

    public void Show(int ID) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<CitizenReportItem>>(getContext()) {
            @Override
            public Call<SingleTransformer<CitizenReportItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleAPI(Url.getShowReport(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ShowReportResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addParameter(Keys.REPORT_ID, ID)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Loading...")
                .execute();
    }

    public void type() {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<ReportTypeModel>>(getContext()) {
            @Override
            public Call<CollectionTransformer<ReportTypeModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestTypeAPI(Url.getReportType(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new TypeResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.INCLUDE, "info")
                .execute();
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<CitizenReportItem>> requestCollectionAPI(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<ReportTypeModel>> requestTypeAPI(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);


        @Multipart
        @POST("{p}")
        Call<SingleTransformer<CitizenReportItem>> requestSingleAPI(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);
    }

    public class MyReportResponse extends APIResponse<CollectionTransformer<CitizenReportItem>> {
        public MyReportResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class TypeResponse extends APIResponse<CollectionTransformer<ReportTypeModel>> {
        public TypeResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class DeleteReportResponse extends APIResponse<SingleTransformer<CitizenReportItem>> {
        public DeleteReportResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class CreateReportResponse extends APIResponse<SingleTransformer<CitizenReportItem>> {
        public CreateReportResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class ShowReportResponse extends APIResponse<SingleTransformer<CitizenReportItem>> {
        public ShowReportResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
