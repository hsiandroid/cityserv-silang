package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.RequestInfoItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseListViewAdapter;

public class RequestInfoListViewAdapter extends BaseListViewAdapter<RequestInfoListViewAdapter.ViewHolder, RequestInfoItem> {

    private ClickListener clickListener;

    public RequestInfoListViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_request_status));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.titleTXT.setText(holder.getItem().title);
        holder.durationTXT.setText(holder.getItem().duration);
        holder.viewTXT.setTag(holder.getItem());
        holder.viewTXT.setOnClickListener(this);

        holder.viewTXT.setVisibility(View.GONE);
        switch (holder.getItem().status.toLowerCase()){
            case "done":
                holder.imageIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_status_completed));
                holder.statusCON.setVisibility(View.VISIBLE);
                break;
            case "pending":
                holder.imageIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_status_pending));
                holder.statusCON.setVisibility(View.GONE);
                break;
            case "for_revalidation":
                holder.imageIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_status_revalidate));
                holder.statusCON.setVisibility(View.GONE);
                break;
            case "rejected":
                holder.imageIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_status_rejected));
                holder.imageIV.setVisibility(View.GONE);
                holder.viewTXT.setVisibility(View.VISIBLE);
                //ADD "View Remarks?" clickable here @jomar
                break;
            default :

                holder.statusCON.setVisibility(View.GONE);
                holder.viewTXT.setVisibility(View.GONE);
        }
    }

    public class ViewHolder extends BaseListViewAdapter.ViewHolder{

        @BindView(R.id.titleTXT)         TextView titleTXT;
        @BindView(R.id.statusCON)        View statusCON;
        @BindView(R.id.imageIV)          ImageView imageIV;
        @BindView(R.id.viewTXT)          TextView viewTXT;
        @BindView(R.id.durationTXT)      TextView durationTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public RequestInfoItem getItem() {
            return (RequestInfoItem) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick(((ViewHolder) v.getTag()).getItem());
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends Listener{
        void onItemClick(RequestInfoItem requestInfoItem);
    }
} 
