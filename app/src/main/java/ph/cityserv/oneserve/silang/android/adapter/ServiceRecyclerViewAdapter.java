package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.ServiceItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseRecylerViewAdapter;

public class ServiceRecyclerViewAdapter extends BaseRecylerViewAdapter<ServiceRecyclerViewAdapter.ViewHolder, ServiceItem> {

    private ClickListener clickListener;

    public ServiceRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_service));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setOnClickListener(this);
        holder.getBaseView().setTag(holder.getItem());
        holder.serviceTXT.setText(holder.getItem().title);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.serviceTXT)      TextView serviceTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public ServiceItem getItem() {
            return (ServiceItem) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((ServiceItem) v.getTag());
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends Listener{
        void onItemClick(ServiceItem serviceItem);
    }
} 
