
package ph.cityserv.oneserve.silang.android.fragment.main;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.MainActivity;
import ph.cityserv.oneserve.silang.android.adapter.ServiceRecyclerViewAdapter;
import ph.cityserv.oneserve.silang.data.model.server.ServiceItem;
import ph.cityserv.oneserve.silang.server.request.Service;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;

public class ServicesFragment extends BaseFragment implements
        ServiceRecyclerViewAdapter.ClickListener,
        SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = ServicesFragment.class.getName().toString();

    private MainActivity mainActivity;

    private ServiceRecyclerViewAdapter serviceRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.serviceSRL)              SwipeRefreshLayout serviceSRL;
    @BindView(R.id.serviceRV)               RecyclerView serviceRV;
    @BindView(R.id.servicePlaceholderCON)   View servicePlaceholderCON;

    public static ServicesFragment newInstance() {
        ServicesFragment fragment = new ServicesFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_service;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_services));

        setUpListView();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void setUpListView(){
        linearLayoutManager = new LinearLayoutManager(getContext());
        serviceRecyclerViewAdapter = new ServiceRecyclerViewAdapter(getContext());
        serviceRecyclerViewAdapter.setClickListener(this);

        serviceRV.setLayoutManager(linearLayoutManager);
        serviceRV.setAdapter(serviceRecyclerViewAdapter);
    }

    @Override
    public void onItemClick(ServiceItem serviceItem) {
        switch (serviceItem.code.toLowerCase()){
            case "assessor":
                mainActivity.startAssessorActivity(serviceItem,"assessor");
                break;
            case "public-employment-service-office":
                mainActivity.startJobActivty("job");
                break;
            case "youth-affairs-office":
                mainActivity.startJobActivty("youth");
                break;
            default:
                mainActivity.startAssessorActivity(serviceItem,"requirements");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Subscribe
    public void onResponse(Service.ServiceResponse responseData){
        CollectionTransformer<ServiceItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            if(responseData.isNext()){
                serviceRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else{
                serviceRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }
        }
        if (serviceRecyclerViewAdapter.getItemCount() == 0){
            serviceRV.setVisibility(View.GONE);
            servicePlaceholderCON.setVisibility(View.VISIBLE);
        }else {
            serviceRV.setVisibility(View.VISIBLE);
            servicePlaceholderCON.setVisibility(View.GONE);
        }
    }

    public void refreshList(){
        Service.getDefault(getContext()).all(serviceSRL);
    }
}
