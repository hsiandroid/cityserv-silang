package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 6/25/2016.
 */
public class NotificationItem extends AndroidModel {

    @SerializedName("user_id")
    public String user_id;

    @SerializedName("msg")
    public String msg;

    @SerializedName("type")
    public String type;

    @SerializedName("reference_id")
    public String reference_id;

    @SerializedName("title")
    public String title;

    @SerializedName("is_read")
    public String is_read;

    @SerializedName("image")
    public String image;

    @SerializedName("date")
    public Date date;

    public class Date{

        @SerializedName("data")
        public DateItem data;
    }
}
