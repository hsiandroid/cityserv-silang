package ph.cityserv.oneserve.silang.vendor.android.java;

/**
 * Created by BCTI 3 on 11/25/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.text.Layout;
import android.text.Spannable;
import android.text.method.MovementMethod;
import android.text.style.URLSpan;
import android.view.MotionEvent;
import android.widget.TextView;

import ph.cityserv.oneserve.silang.android.dialog.WebViewDialog;

public class LinkMovementMethod extends android.text.method.LinkMovementMethod {

    // The context we pass to the method
    private static Context movementContext;
    private static FragmentManager fragmentManager;
    // A new LinkMovementMethod
    private static LinkMovementMethod linkMovementMethod  = new LinkMovementMethod();

    public static MovementMethod getInstance(Context c, FragmentManager f){
        // Set the context
        movementContext = c;
        fragmentManager = f;
        // Return this movement method
        return linkMovementMethod;
    }

    public boolean onTouchEvent(TextView widget, Spannable buffer, MotionEvent event){
        // Get the event action
        int action = event.getAction();

        // If action has finished
        if(action == MotionEvent.ACTION_UP) {
            // Locate the area that was pressed
            int x = (int) event.getX();
            int y = (int) event.getY();
            x -= widget.getTotalPaddingLeft();
            y -= widget.getTotalPaddingTop();
            x += widget.getScrollX();
            y += widget.getScrollY();

            // Locate the URL text
            Layout layout = widget.getLayout();
            int line = layout.getLineForVertical(y);
            int off = layout.getOffsetForHorizontal(line, x);

            // Find the URL that was pressed
            URLSpan[] link = buffer.getSpans(off, off, URLSpan.class);
            // If we've found a URL
            if (link.length != 0) {
                // Find the URL
                String url = link[0].getURL();
                // If it's a valid URL
//                if (url.contains("https") | url.contains("tel") | url.contains("mailto") | url.contains("http") | url.contains("https") | url.contains("www")){
//                    // Open it in an instance of InlineBrowser
//                    movementContext.startActivity(new Intent(movementContext, MinimalBrowser.class).putExtra("url", url));
//                }

                Intent intent;
                if(url.startsWith("tel:")){
                    intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                    movementContext.startActivity(intent);
                }else if(url.startsWith("mailto:")){
                    intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(url));
                    movementContext.startActivity(intent);
                }else{
                    WebViewDialog.newInstance(url).show(fragmentManager, WebViewDialog.TAG);
                }
                // If we're here, something's wrong
                return true;
            }
        }
        return super.onTouchEvent(widget, buffer, event);
    }
}