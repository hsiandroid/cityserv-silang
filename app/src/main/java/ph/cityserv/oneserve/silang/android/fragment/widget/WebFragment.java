package ph.cityserv.oneserve.silang.android.fragment.widget;

import android.content.Intent;
import android.net.Uri;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.WidgetActivity;
import ph.cityserv.oneserve.silang.android.dialog.WebViewDialog;
import ph.cityserv.oneserve.silang.data.model.server.WidgetItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;

public class WebFragment extends BaseFragment {

    public static final String TAG = WebFragment.class.getName().toString();

    private WidgetActivity widgetActivity;
    private WidgetItem widgetItem;

    @BindView(R.id.widgetWV)    WebView widgetWV;

    public static WebFragment newInstance(WidgetItem widgetItem) {
        WebFragment fragment = new WebFragment();
        fragment.widgetItem = widgetItem;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_widget_web;
    }

    @Override
    public void onViewReady() {
        widgetActivity = (WidgetActivity) getActivity();
        widgetActivity.setTitle(widgetItem.title);
        init();
    }

    private void init(){
        widgetWV.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Intent intent;
                if(url.startsWith("tel:")){
                    intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                    startActivity(intent);
                }else if(url.startsWith("mailto:")){
                    intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(url));
                    startActivity(intent);
                }else{
                    WebViewDialog.newInstance(url).show(getChildFragmentManager(), WebViewDialog.TAG);
                }
                return true;
            }
        });
        widgetWV.loadDataWithBaseURL(null, widgetItem.content, "text/html", "utf-8", null);
        WebSettings webSettings = widgetWV.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }
}
