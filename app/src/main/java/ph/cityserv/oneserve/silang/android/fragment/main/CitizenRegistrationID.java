package ph.cityserv.oneserve.silang.android.fragment.main;

import android.app.DatePickerDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import icepick.State;
import io.realm.Realm;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.MainActivity;
import ph.cityserv.oneserve.silang.android.dialog.BarangayDialog;
import ph.cityserv.oneserve.silang.data.model.db.SampleRealModel;
import ph.cityserv.oneserve.silang.data.model.server.RequestIDItem;
import ph.cityserv.oneserve.silang.server.request.Auth;
import ph.cityserv.oneserve.silang.server.request.LGU;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.ToastMessage;
import ph.cityserv.oneserve.silang.vendor.server.transformer.BaseTransformer;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;

public class CitizenRegistrationID extends BaseFragment implements View.OnClickListener, BarangayDialog.Callback{
    public static final String TAG = CitizenRegistrationID.class.getName().toString();

    @BindView(R.id.lastNameET)                   EditText lastNameET;
    @BindView(R.id.firstNameET)                  EditText firstNameET;
    @BindView(R.id.middleNameET)                 EditText middleNameET;
    @BindView(R.id.streetET)                     EditText streetET;
    @BindView(R.id.nationalityET)                EditText nationalityET;
    @BindView(R.id.occupationET)                 EditText occupationET;
    @BindView(R.id.heightET)                     EditText heightET;
    @BindView(R.id.weightET)                     EditText weightET;
    @BindView(R.id.emailET)                      EditText emailET;
    @BindView(R.id.birthDateBTN)                 TextView birthDateBTN;
    @BindView(R.id.barangayBTN)                  TextView barangayBTN;
    @BindView(R.id.continueBTN)                  TextView continueBTN;
    @BindView(R.id.barangayDropDownBTN)          ImageView barangayDropDownBTN;
    @BindView(R.id.contactNumberET)              EditText contactNumberET;
    @BindView(R.id.maleRBTN)                     RadioButton maleRBTN;
    @BindView(R.id.femaleRBTN)                   RadioButton femaleRBTN;

    @State String gender;

    private int mYear, mMonth, mDay;

    private MainActivity mainActivity;

    public static CitizenRegistrationID newInstance() {
        CitizenRegistrationID fragment = new CitizenRegistrationID();
        return fragment;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getContext();
        mainActivity.setTitle("Apply for Citizen Card");
        birthDateBTN.setOnClickListener(this);
        barangayBTN.setOnClickListener(this);
        barangayDropDownBTN.setOnClickListener(this);
        continueBTN.setOnClickListener(this);
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_city_id_registration;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(LGU.IDResponse responseData) {
        SingleTransformer<RequestIDItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            mainActivity.openActionCentertFragment();
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onClick(View v) {
            switch (v.getId()){
                case R.id.birthDateBTN:
                    startDate();
                    break;
                case R.id.barangayBTN:
                case R.id.barangayDropDownBTN:
                    BarangayDialog.newInstance(this).show(getFragmentManager(), TAG);
                    break;
                case R.id.continueBTN:
                    if (maleRBTN.isSelected()){
                        gender = "male";
                    }else{
                        gender = "female";
                    }
                    LGU.getDefault(getContext()).requestID(firstNameET.getText().toString(),
                            lastNameET.getText().toString(),
                            middleNameET.getText().toString(),
                            gender, nationalityET.getText().toString(),
                            streetET.getText().toString(), barangayBTN.getText().toString(), occupationET.getText().toString(),
                            weightET.getText().toString(), heightET.getText().toString(), birthDateBTN.getText().toString(),
                            contactNumberET.getText().toString(), emailET.getText().toString() );
                    break;
            }
    }

    private void startDate(){
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String month = String.format("%02d",(monthOfYear + 1));
                        String day = String.format("%02d",dayOfMonth);

                        birthDateBTN.setText(month+ "/" + day+ "/" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    public void onSuccess(String categoryName, int categoryID) {
        barangayBTN.setText(categoryName);
    }
}
