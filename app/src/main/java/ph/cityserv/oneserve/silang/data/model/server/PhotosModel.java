package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 8/12/2017.
 */

public class PhotosModel extends AndroidModel {


    @SerializedName("caption")
    public String caption;

    @SerializedName("path")
    public String path;

    @SerializedName("directory")
    public String directory;

    @SerializedName("full_path")
    public String fullPath;

    @SerializedName("thumb_path")
    public String thumbPath;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public PhotosModel convertFromJson(String json) {
        return convertFromJson(json, PhotosModel.class);
    }
}
