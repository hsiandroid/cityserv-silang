package ph.cityserv.oneserve.silang.android.fragment.main;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;
import io.realm.Realm;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.MainActivity;
import ph.cityserv.oneserve.silang.android.adapter.LGUPagerAdapter;
import ph.cityserv.oneserve.silang.data.model.db.SampleRealModel;
import ph.cityserv.oneserve.silang.server.request.Auth;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.server.transformer.BaseTransformer;

public class SilangLguFragment extends BaseFragment {
    public static final String TAG = SilangLguFragment.class.getName().toString();

    private MainActivity mainActivity;
    private LGUPagerAdapter messagesPagerAdapter;


    public static SilangLguFragment newInstance() {
        SilangLguFragment fragment = new SilangLguFragment();
        return fragment;
    }

    @BindView(R.id.silangLguTL)           TabLayout landingTL;
    @BindView(R.id.silangLguVP)           ViewPager landingVP;

    @Override
    public void onViewReady() {
      mainActivity = (MainActivity) getContext();
      mainActivity.setTitle(getString(R.string.nav_lgu));
        messagesPagerAdapter = new LGUPagerAdapter(getChildFragmentManager(), mainActivity);
        landingVP.setAdapter(messagesPagerAdapter);
        landingTL.setupWithViewPager(landingVP);
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_silang_lgu;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
}
