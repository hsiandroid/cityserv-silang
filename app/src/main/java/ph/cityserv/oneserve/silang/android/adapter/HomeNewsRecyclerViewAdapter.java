package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.NewsItem;
import ph.cityserv.oneserve.silang.data.model.server.NewsItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseRecylerViewAdapter;

public class HomeNewsRecyclerViewAdapter extends BaseRecylerViewAdapter<HomeNewsRecyclerViewAdapter.ViewHolder, NewsItem> {

    private ClickListener clickListener;

    public HomeNewsRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_home_news));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());

        holder.titleTXT.setText(Html.fromHtml(holder.getItem().title));
        holder.contentTXT.setText(Html.fromHtml(holder.getItem().excerpt));
        holder.dateTXT.setText(Html.fromHtml(holder.getItem().date.data.date_db));

        Picasso.with(getContext()).load(holder.getItem().info.data.full_path)
                .placeholder(R.drawable.placeholder_silang)
                .centerCrop()
                .fit()
                .into(holder.imageIV);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.titleTXT)     TextView titleTXT;
        @BindView(R.id.contentTXT)   TextView contentTXT;
        @BindView(R.id.dateTXT)      TextView dateTXT;
        @BindView(R.id.imageIV)      ImageView imageIV;
        @BindView(R.id.adapterCON)   View adapterCON;

        public ViewHolder(View view) {
            super(view);
            adapterCON.setOnClickListener(HomeNewsRecyclerViewAdapter.this);
        }

        public NewsItem getItem() {
            return (NewsItem) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onItemClick((NewsItem) v.getTag());
                }
                break;
        }
    }

    public interface ClickListener extends Listener{
        void onItemClick(NewsItem newsItem);
    }
} 
