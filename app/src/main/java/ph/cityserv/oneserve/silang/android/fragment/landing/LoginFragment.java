package ph.cityserv.oneserve.silang.android.fragment.landing;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.LandingActivity;
import ph.cityserv.oneserve.silang.data.model.server.UserItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.server.request.Auth;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.ToastMessage;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;

public class LoginFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = LoginFragment.class.getName().toString();
    public LandingActivity landingActivity;

    @BindView(R.id.emailACTV)           EditText emailACTV;
    @BindView(R.id.passwordET)          EditText passwordET;
    @BindView(R.id.forgotPasswordTV)    TextView forgotPasswordTV;
    @BindView(R.id.signUpBTN)           TextView signUpBTN;
    @BindView(R.id.loginTV)             TextView loginTV;

    public static LoginFragment newInstance(){
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_login;
    }

    @Override
    public void onViewReady() {
        landingActivity = (LandingActivity) getContext();
        forgotPasswordTV.setOnClickListener(this);
        signUpBTN.setOnClickListener(this);
        loginTV.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.forgotPasswordTV:
                landingActivity.startRegistrationActivity("", "forgot");
                break;
            case R.id.signUpBTN:
                landingActivity.startRegistrationActivity("", "signup");
                break;
            case R.id.loginTV:
                attemptLogin();
                break;
        }
    }

    private void attemptLogin(){
        Auth.getDefault(getContext()).login(emailACTV.getText().toString(), passwordET.getText().toString());
}

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }
    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            UserData.insert(singleTransformer.data);
            UserData.insert(UserData.AUTHORIZATION, singleTransformer.token);
//            UserData.insert(UserData.FIRST_LOGIN, singleTransformer.first_login);
            landingActivity.startMainActivity("main");
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }
}
