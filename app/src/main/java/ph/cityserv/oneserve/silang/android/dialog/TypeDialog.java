package ph.cityserv.oneserve.silang.android.dialog;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;



import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.adapter.TypeRecyclerViewAdapter;
import ph.cityserv.oneserve.silang.data.model.server.ReportTypeModel;
import ph.cityserv.oneserve.silang.server.request.CitizenReport;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseDialog;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class TypeDialog extends BaseDialog implements View.OnClickListener, TypeRecyclerViewAdapter.ClickListener {
	public static final String TAG = TypeDialog.class.getName().toString();

	private int category_id;

	public static TypeDialog newInstance(Callback callback) {
		TypeDialog mentorshipSuccessDialog = new TypeDialog();
		mentorshipSuccessDialog.callback = callback;
		return mentorshipSuccessDialog;
	}

	private TypeRecyclerViewAdapter typeRecylerviewAdater;
	private LinearLayoutManager menteesLLM;
	private Callback callback;

	@BindView(R.id.exitBTN)                 View exitBTN;
	@BindView(R.id.categoryRV) 				RecyclerView categoryRV;
	@BindView(R.id.progressBAR) 			ProgressBar progressBar;


	@Override
	public int onLayoutSet() {return R.layout.dialog_report_type;}

	@Override
	public void onViewReady() {
        exitBTN.setOnClickListener(this);
        setupRV();
        CitizenReport.getDefault(getContext()).type();
	}

	private void setupRV(){
		menteesLLM = new LinearLayoutManager(getContext());
		typeRecylerviewAdater = new TypeRecyclerViewAdapter(getContext());
		categoryRV.setLayoutManager(menteesLLM);
		categoryRV.setAdapter(typeRecylerviewAdater);
		typeRecylerviewAdater.setClickListener(this);
		progressBar.setVisibility(View.VISIBLE);
	}

	@Subscribe
	public void onResponse(CitizenReport.TypeResponse responseData) {
		CollectionTransformer<ReportTypeModel> collectionTransformer = responseData.getData(CollectionTransformer.class);
		if(collectionTransformer.status){
			if (responseData.isNext()){
				typeRecylerviewAdater.addNewData(collectionTransformer.data);
				progressBar.setVisibility(View.GONE);

			}else {
				typeRecylerviewAdater.setNewData(collectionTransformer.data);
				progressBar.setVisibility(View.GONE);
			}
			if (typeRecylerviewAdater.getData().size() == 0){
			}else {
			}
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.exitBTN:
                dismiss();
                break;
        }
    }

	public interface Callback {
		void onSuccess(String categoryName, int categoryID);
	}

	@Override
	public void onCategoryClicked(ReportTypeModel reportTypeModel) {
		if(callback != null){
			callback.onSuccess(reportTypeModel.title, reportTypeModel.id);
			dismiss();
		}
	}
}
