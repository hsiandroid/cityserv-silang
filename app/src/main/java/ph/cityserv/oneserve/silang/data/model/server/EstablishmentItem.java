package ph.cityserv.oneserve.silang.data.model.server;


import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 6/25/2016.
 */
public class EstablishmentItem  extends AndroidModel {
    @SerializedName("name")
    public String name;

    @SerializedName("address")
    public String address;

    @SerializedName("contact")
    public String contact;

    @SerializedName("geolocation")
    public Geolocation geolocation;


    public class Geolocation{

        @SerializedName("lat")
        public Double latitude;

        @SerializedName("long")
        public Double longitude;

        public LatLng getLatLong(){
            return new LatLng(latitude, longitude);
        }
    }
}
