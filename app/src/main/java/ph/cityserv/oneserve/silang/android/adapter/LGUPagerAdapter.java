package ph.cityserv.oneserve.silang.android.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ph.cityserv.oneserve.silang.android.fragment.lgu.CityPhotosFragment;
import ph.cityserv.oneserve.silang.android.fragment.lgu.OfficialsFragment;


/**
 * Created by Ken Drew on 11/11/2017.
 */

public class LGUPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 2;
    private Context context;

    public LGUPagerAdapter(FragmentManager fragmentManager, Context c) {
        super(fragmentManager);
        context = c;
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return OfficialsFragment.newInstance();
            case 1:
                return CityPhotosFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_UNCHANGED;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return "Officials";
            case 1:
                return "Photos";
            default:
                return "Page" + position;

        }

    }

}