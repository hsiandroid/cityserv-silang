package ph.cityserv.oneserve.silang.android.dialog;

import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.MomentItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseDialog;
import ph.cityserv.oneserve.silang.vendor.android.widget.ResizableImageView;

public class MomentEditDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = MomentEditDialog.class.getName().toString();

	private MomentItem momentItem;

	@BindView(R.id.titleBarTXT)			TextView titleBarTXT;
	@BindView(R.id.imageRIV) 			ResizableImageView imageRIV;
	@BindView(R.id.dateTXT)				TextView dateTXT;
	@BindView(R.id.contentET)			EditText contentET;
	@BindView(R.id.saveBTN)				TextView saveBTN;
	@BindView(R.id.backBTN)				ImageView backBTN;

	public static MomentEditDialog newInstance(MomentItem momentItem) {
		MomentEditDialog fragment = new MomentEditDialog();
		fragment.momentItem = momentItem;
		return fragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_moment_edit;
	}

	@Override
	public void onViewReady() {
		titleBarTXT.setText("EDIT MOMENT");

		backBTN.setOnClickListener(this);
		saveBTN.setOnClickListener(this);
		momentData(momentItem);
	}

	private  void momentData(MomentItem momentItem){
		Picasso.with(getContext())
				.load(momentItem.info.data.full_path)
				.placeholder(R.drawable.placeholder_logo)
				.error(R.drawable.placeholder_logo)
				.into(imageRIV);

		contentET.setText(Html.fromHtml(momentItem.info.data.content));
		dateTXT.setText(momentItem.date.data.time_passed);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.saveBTN:

				break;

			case R.id.backBTN:
				dismiss();
				break;
		}
	}
}
