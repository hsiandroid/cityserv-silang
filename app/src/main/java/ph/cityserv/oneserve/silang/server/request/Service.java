package ph.cityserv.oneserve.silang.server.request;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import ph.cityserv.oneserve.silang.config.Keys;
import ph.cityserv.oneserve.silang.config.Url;
import ph.cityserv.oneserve.silang.data.model.server.ServiceItem;
import ph.cityserv.oneserve.silang.data.model.server.UserItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.request.APIResponse;
import ph.cityserv.oneserve.silang.vendor.server.request.BaseRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by BCTI 3 on 8/16/2017.
 */

public class Service extends BaseRequest {
    public Service(Context context) {
        super(context);
    }

    private String include = "info,date,subservices.info";

    public static Service getDefault(Context context) {
        Service fragment = new Service(context);
        return fragment;
    }

    public void all(SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<ServiceItem>>(getContext()) {
            @Override
            public Call<CollectionTransformer<ServiceItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestAPI(Url.getAllService(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ServiceResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .execute();
    }

    public void detail(int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ServiceItem>>(getContext()) {
            @Override
            public Call<SingleTransformer<ServiceItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleAPI(Url.getServiceDetail(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new DetailResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addParameter(Keys.SERVICE_ID, id)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<ServiceItem>> requestAPI(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<ServiceItem>> requestSingleAPI(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

    }

    public class ServiceResponse extends APIResponse<CollectionTransformer<ServiceItem>> {
        public ServiceResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class DetailResponse extends APIResponse<SingleTransformer<ServiceItem>> {
        public DetailResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
