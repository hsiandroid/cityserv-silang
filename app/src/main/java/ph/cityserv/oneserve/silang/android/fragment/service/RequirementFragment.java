package ph.cityserv.oneserve.silang.android.fragment.service;

import android.widget.TextView;


import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.AssessorActivity;
import ph.cityserv.oneserve.silang.data.model.server.ServiceItem;
import ph.cityserv.oneserve.silang.data.model.server.SubServiceItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.WrapContentWebView;

public class RequirementFragment extends BaseFragment {

    public static final String TAG = RequirementFragment.class.getName().toString();

    private AssessorActivity assessorActivity;

    private ServiceItem serviceItem;
    private SubServiceItem subServiceItem;

    @BindView(R.id.requirementTXT)          TextView requirementTXT;
    @BindView(R.id.requirementWV)           WrapContentWebView requirementWV;

    public static RequirementFragment newInstance(ServiceItem serviceItem) {
        RequirementFragment fragment = new RequirementFragment();
        fragment.serviceItem = serviceItem;
        return fragment;
    }

    public static RequirementFragment newInstance(SubServiceItem subServiceItem) {
        RequirementFragment fragment = new RequirementFragment();
        fragment.subServiceItem = subServiceItem;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_requirement;
    }

    @Override
    public void onViewReady() {
        assessorActivity = (AssessorActivity) getActivity();
        assessorActivity.setTitle(serviceItem.title);

        if(serviceItem != null){
            requirementTXT.setText(serviceItem.title);
            requirementWV.loadDataWithBaseURL(null, serviceItem.info.data.content, "text/html", "utf-8", null);
        }else{
            requirementTXT.setText(subServiceItem.title);
            requirementWV.loadDataWithBaseURL(null, subServiceItem.info.data.content, "text/html", "utf-8", null);
        }
    }
}
