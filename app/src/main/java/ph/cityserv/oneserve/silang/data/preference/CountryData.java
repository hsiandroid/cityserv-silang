package ph.cityserv.oneserve.silang.data.preference;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BCTI 3 on 5/16/2017.
 */

public class CountryData {

    public static List<Country> countries;

    public static String getCountryByCode1(String code){
        for(CountryData.Country data : getCountryData()){
            if(data.code1.equalsIgnoreCase(code)){
                return data.country;
            }
        }
        return "";
    }

    public static String getCountryByCode2(String code){
        for(CountryData.Country data : getCountryData()){
            if(data.code2.equalsIgnoreCase(code)){
                return data.country;
            }
        }
        return "";
    }

    public static String getCountryByCode3(String code){
        for(CountryData.Country data : getCountryData()){
            if(data.code3.equalsIgnoreCase(code)){
                return data.country;
            }
        }
        return "";
    }

    public static CountryData.Country getCountryDataByCode1(String code){
        for(CountryData.Country data : getCountryData()){
            if(data.code1.equalsIgnoreCase(code)){
                return data;
            }
        }
        return new CountryData.Country();
    }

    public static CountryData.Country getCountryDataByCode2(String code){
        for(CountryData.Country data : getCountryData()){
            if(data.code2.equalsIgnoreCase(code)){
                return data;
            }
        }
        return new CountryData.Country();
    }

    public static CountryData.Country getCountryDataByCode3(String code){
        for(CountryData.Country data : getCountryData()){
            if(data.code3.equalsIgnoreCase(code)){
                return data;
            }
        }
        return new CountryData.Country();
    }

    public static List<Country> getCountryData(){

        if(countries != null){
            return countries;
        }

        countries = new ArrayList<>();
        countries.add(new Country("Afghanistan", "AF", "AFG", "93"));
        countries.add(new Country("Albania", "AL", "ALB", "355"));
        countries.add(new Country("Algeria", "DZ", "DZA", "213"));
        countries.add(new Country("American Samoa", "AS", "ASM", "1 684"));
        countries.add(new Country("Andorra", "AD", "AND", "376"));
        countries.add(new Country("Angola", "AO", "AGO", "244"));
        countries.add(new Country("Anguilla", "AI", "AIA", "1 264"));
        countries.add(new Country("Antarctica", "AQ", "ATA", "672, 64"));
        countries.add(new Country("Antigua and Barbuda", "AG", "ATG", "1 268"));
        countries.add(new Country("Argentina", "AR", "ARG", "54"));
        countries.add(new Country("Armenia", "AM", "ARM", "374"));
        countries.add(new Country("Aruba", "AW", "ABW", "297"));
        countries.add(new Country("Ascension Island", "AC", "ASC ", "247"));
        countries.add(new Country("Australia", "AU", "AUS", "61"));
        countries.add(new Country("Austria", "AT", "AUT", "43"));
        countries.add(new Country("Azerbaijan", "AZ", "AZE", "994"));
        countries.add(new Country("Bahamas", "BS", "BHS", "1 242"));
        countries.add(new Country("Bahrain", "BH", "BHR", "973"));
        countries.add(new Country("Bangladesh", "BD", "BGD", "880"));
        countries.add(new Country("Barbados", "BB", "BRB", "1 246"));
        countries.add(new Country("Belarus", "BY", "BLR", "375"));
        countries.add(new Country("Belgium", "BE", "BEL", "32"));
        countries.add(new Country("Belize", "BZ", "BLZ", "501"));
        countries.add(new Country("Benin", "BJ", "BEN", "229"));
        countries.add(new Country("Bermuda", "BM", "BMU", "1 441"));
        countries.add(new Country("Bhutan", "BT", "BTN", "975"));
        countries.add(new Country("Bolivia", "BO", "BOL", "591"));
        countries.add(new Country("Bosnia and Herzegovina", "BA", "BIH", "387"));
        countries.add(new Country("Botswana", "BW", "BWA", "267"));
        countries.add(new Country("Brazil", "BR", "BRA", "55"));
        countries.add(new Country("British Virgin Islands", "VG", "VGB", "1 284"));
        countries.add(new Country("Brunei", "BN", "BRN", "673"));
        countries.add(new Country("Bulgaria", "BG", "BGR", "359"));
        countries.add(new Country("Burkina Faso", "BF", "BFA", "226"));
        countries.add(new Country("Burma (Myanmar)", "MM", "MMR", "95"));
        countries.add(new Country("Burundi", "BI", "BDI", "257"));
        countries.add(new Country("Cambodia", "KH", "KHM", "855"));
        countries.add(new Country("Cameroon", "CM", "CMR", "237"));

        countries.add(new Country("Canada", "CA", "CAN", "1"));
        countries.add(new Country("Cape Verde", "CV", "CPV", "238"));
        countries.add(new Country("Cayman Islands", "KY", "CYM", "1 345"));
        countries.add(new Country("Central African Republic", "CF", "CAF", "236"));
        countries.add(new Country("Chad", "TD", "TCD", "235"));
        countries.add(new Country("Chile", "CL", "CHL", "56"));
        countries.add(new Country("China", "CN", "CHN", "86"));
        countries.add(new Country("Christmas Island", "CX", "CXR", "61"));
        countries.add(new Country("Cocos (Keeling) Islands", "CC", "CCK", "61"));
        countries.add(new Country("Colombia", "CO", "COL", "57"));
        countries.add(new Country("Comoros", "KM", "COM", "269"));
        countries.add(new Country("Congo", "CG", "COG", "242"));
        countries.add(new Country("Cook Islands", "CK", "COK", "682"));
        countries.add(new Country("Costa Rica", "CR", "CRC", "506"));
        countries.add(new Country("Croatia", "HR", "HRV", "385"));
        countries.add(new Country("Cuba", "CU", "CUB", "53"));
        countries.add(new Country("Cyprus", "CY", "CYP", "357"));
        countries.add(new Country("Czech Republic", "CZ", "CZE", "420"));
        countries.add(new Country("Democratic Republic of the Congo", "CD", "COD", "243"));
        countries.add(new Country("Denmark", "DK", "DNK", "45"));
        countries.add(new Country("Diego Garci", " D", "GA ", "46"));
        countries.add(new Country("Djibouti", "DJ", "DJI", "253"));
        countries.add(new Country("Dominica", "DM", "DMA", "1 767"));
        countries.add(new Country("Dominican Republic", "DO", "DOM", "1 809, 1 829, 1 849"));
        countries.add(new Country("Ecuador", "EC", "ECU", "593"));
        countries.add(new Country("Egypt", "EG", "EGY", "20"));
        countries.add(new Country("El Salvador", "SV", "SLV", "503"));
        countries.add(new Country("Equatorial Guinea", "GQ", "GNQ", "240"));
        countries.add(new Country("Eritrea", "ER", "ERI", "291"));
        countries.add(new Country("Estonia", "EE", "EST", "372"));
        countries.add(new Country("Ethiopia", "ET", "ETH", "251"));
        countries.add(new Country("Falkland Islands", "FK", "FLK", "500"));
        countries.add(new Country("Faroe Islands", "FO", "FRO", "298"));
        countries.add(new Country("Fiji", "FJ", "FJI", "679"));
        countries.add(new Country("Finland", "FI", "FIN", "358"));
        countries.add(new Country("France", "FR", "FRA", "33"));
        countries.add(new Country("French Guiana", "GF", "GUF", "594"));
        countries.add(new Country("French Polynesia", "PF", "PYF", "689"));
        countries.add(new Country("Gabon", "GA", "GAB", "241"));
        countries.add(new Country("Gambia", "GM", "GMB", "220"));
        countries.add(new Country("Georgia", "GE", "GEO", "995"));

        countries.add(new Country("Germany", "DE", "DEU", "49"));
        countries.add(new Country("Ghana", "GH", "GHA", "233"));
        countries.add(new Country("Gibraltar", "GI", "GIB", "350"));
        countries.add(new Country("Greece", "GR", "GRC", "30"));
        countries.add(new Country("Greenland", "GL", "GRL", "299"));
        countries.add(new Country("Grenada", "GD", "GRD", "1 473"));
        countries.add(new Country("Guadeloupe", "GP", "GLP", "590"));
        countries.add(new Country("Guam", "GU", "GUM", "1 671"));
        countries.add(new Country("Guatemala", "GT", "GTM", "502"));
        countries.add(new Country("Guinea", "GN", "GIN", "224"));
        countries.add(new Country("Guinea-Bissau", "GW", "GNB", "245"));
        countries.add(new Country("Guyana", "GY", "GUY", "592"));
        countries.add(new Country("Haiti", "HT", "HTI", "509"));
        countries.add(new Country("Holy See (Vatican City)", "VA", "VAT", "39"));
        countries.add(new Country("Honduras", "HN", "HND", "504"));
        countries.add(new Country("Hong Kong", "HK", "HKG", "852"));
        countries.add(new Country("Hungary", "HU", "HUN", "36"));
        countries.add(new Country("Iceland", "IS", "IS ", "54"));
        countries.add(new Country("India", "IN", "IND", "91"));
        countries.add(new Country("Indonesia", "ID", "IDN", "62"));
        countries.add(new Country("Iran", "IR", "IRN", "98"));
        countries.add(new Country("Iraq", "IQ", "IRQ", "964"));
        countries.add(new Country("Ireland", "IE", "IRL", "353"));
        countries.add(new Country("Isle of Man", "IM", "IMN", "44"));
        countries.add(new Country("Israel", "IL", "ISR", "972"));
        countries.add(new Country("Italy", "IT", "ITA", "39"));
        countries.add(new Country("Ivory Coast (Côte d'Ivoire)", "CI", "CIV", "225"));
        countries.add(new Country("Jamaica", "JM", "JAM", "1 876"));
        countries.add(new Country("Japan", "JP", "JPN", "81"));
        countries.add(new Country("Jersey", "JE", "JEY", "44"));
        countries.add(new Country("Jordan", "JO", "JOR", "962"));
        countries.add(new Country("Kazakhstan", "KZ", "KAZ", "7"));
        countries.add(new Country("Kenya", "KE", "KEN", "254"));
        countries.add(new Country("Kiribati", "KI", "KIR", "686"));
        countries.add(new Country("Kuwait", "KW", "KWT", "965"));
        countries.add(new Country("Kyrgyzstan", "KG", "KGZ", "996"));
        countries.add(new Country("Laos", "LA", "LAO", "856"));
        countries.add(new Country("Latvia", "LV", "LVA", "371"));
        countries.add(new Country("Lebanon", "LB", "LBN", "961"));
        countries.add(new Country("Lesotho", "LS", "LSO", "266"));
        countries.add(new Country("Liberia", "LR", "LBR", "231"));

        countries.add(new Country( "Libya", "LY", "LBY", "218"));
        countries.add(new Country( "Liechtenstein", "LI", "LIE", "423"));
        countries.add(new Country( "Lithuania", "LT", "LTU", "370"));
        countries.add(new Country( "Luxembourg", "LU", "LUX", "352"));
        countries.add(new Country( "Macau", "MO", "MAC", "853"));
        countries.add(new Country( "Macedonia", "MK", "MKD", "389"));
        countries.add(new Country( "Madagascar", "MG", "MDG", "261"));
        countries.add(new Country( "Malawi", "MW", "MWI", "265"));
        countries.add(new Country( "Malaysia", "MY", "MYS", "60"));
        countries.add(new Country( "Maldives", "MV", "MDV", "960"));
        countries.add(new Country( "Mali", "ML", "MLI", "223"));
        countries.add(new Country( "Malta", "MT", "MLT", "356"));
        countries.add(new Country( "Marshall Islands", "MH", "MHL", "692"));
        countries.add(new Country( "Martinique", "MQ", "MTQ", "596"));
        countries.add(new Country( "Mauritania", "MR", "MRT", "222"));
        countries.add(new Country( "Mauritius", "MU", "MUS", "230"));
        countries.add(new Country( "Mayotte", "YT", "MYT", "262"));
        countries.add(new Country( "Mexico", "MX", "MEX", "52"));
        countries.add(new Country( "Micronesia", "FM", "FSM", "691"));
        countries.add(new Country( "Moldova", "MD", "MDA", "373"));
        countries.add(new Country( "Monaco", "MC", "MCO", "377"));
        countries.add(new Country( "Mongolia", "MN", "MNG", "976"));
        countries.add(new Country( "Montenegro", "ME", "MNE", "382"));
        countries.add(new Country( "Montserrat", "MS", "MSR", "1 664"));
        countries.add(new Country( "Morocco", "MA", "MAR", "212"));
        countries.add(new Country( "Mozambique", "MZ", "MOZ", "258"));
        countries.add(new Country( "Namibia", "NA", "NAM", "264"));
        countries.add(new Country( "Nauru", "NR", "NRU", "674"));
        countries.add(new Country( "Nepal", "NP", "NPL", "977"));
        countries.add(new Country( "Netherlands", "NL", "NLD", "31"));
        countries.add(new Country( "Netherlands Antilles", "AN", "ANT", "599"));
        countries.add(new Country( "New Caledonia", "NC", "NCL", "687"));
        countries.add(new Country( "New Zealand", "NZ", "NZL", "64"));
        countries.add(new Country( "Nicaragua", "NI", "NIC", "505"));
        countries.add(new Country( "Niger", "NE", "NER", "227"));
        countries.add(new Country( "Nigeria", "NG", "NGA", "234"));
        countries.add(new Country( "Niue", "NU", "NIU", "683"));
        countries.add(new Country( "Norfolk Island", "NF", "NFK", "672"));
        countries.add(new Country( "North Korea", "KP", "PRK", "850"));
        countries.add(new Country( "Northern Mariana Islands", "MP", "MNP", "1 670"));
        countries.add(new Country( "Norway", "NO", "NOR", "47"));

        countries.add(new Country("Oman", "OM", "OMN", "968"));
        countries.add(new Country("Pakistan", "PK", "PAK", "92"));
        countries.add(new Country("Palau", "PW", "PLW", "680"));
        countries.add(new Country("Palestine", "PS", "PSE", "970"));
        countries.add(new Country("Panama", "PA", "PAN", "507"));
        countries.add(new Country("Papua New Guinea", "PG", "PNG", "675"));
        countries.add(new Country("Paraguay", "PY", "PRY", "595"));
        countries.add(new Country("Peru", "PE", "PER", "51"));
        countries.add(new Country("Philippines", "PH", "PHL", "63"));
        countries.add(new Country("Pitcairn Islands", "PN", "PCN", "870"));
        countries.add(new Country("Poland", "PL", "POL", "48"));
        countries.add(new Country("Portugal", "PT", "PRT", "351"));
        countries.add(new Country("Puerto Rico", "PR", "PRI", "1 787, 1 939"));
        countries.add(new Country("Qatar", "QA", "QAT", "974"));
        countries.add(new Country("Republic of the Congo", "CG", "COG", "242"));
        countries.add(new Country("Reunion Island", "RE", "REU", "262"));
        countries.add(new Country("Romania", "RO", "ROU", "40"));
        countries.add(new Country("Russia", "RU", "RUS", "7"));
        countries.add(new Country("Rwanda", "RW", "RWA", "250"));
        countries.add(new Country("Saint Barthelemy", "BL", "BLM", "590"));
        countries.add(new Country("Saint Helena", "SH", "SHN", "290"));
        countries.add(new Country("Saint Kitts and Nevis", "KN", "KNA", "1 869"));
        countries.add(new Country("Saint Lucia", "LC", "LCA", "1 758"));
        countries.add(new Country("Saint Martin", "MF", "MAF", "590"));
        countries.add(new Country("Saint Pierre and Miquelon", "PM", "SPM", "508"));
        countries.add(new Country("Saint Vincent and the Grenadines", "VC", "VCT", "1 784"));
        countries.add(new Country("Samoa", "WS", "WSM", "685"));
        countries.add(new Country("San Marino", "SM", "SMR", "378"));
        countries.add(new Country("Sao Tome and Principe", "ST", "STP", "239"));
        countries.add(new Country("Saudi Arabia", "SA", "SAU", "966"));
        countries.add(new Country("Senegal", "SN", "SEN", "221"));
        countries.add(new Country("Serbia", "RS", "SRB", "381"));
        countries.add(new Country("Seychelles", "SC", "SYC", "248"));
        countries.add(new Country("Sierra Leone", "SL", "SLE", "232"));
        countries.add(new Country("Singapore", "SG", "SGP", "65"));
        countries.add(new Country("Sint Maarten", "SX", "SXM", "1 721"));
        countries.add(new Country("Slovakia", "SK", "SVK", "421"));
        countries.add(new Country("Slovenia", "SI", "SVN", "386"));
        countries.add(new Country("Solomon Islands", "SB", "SLB", "677"));
        countries.add(new Country("Somalia", "SO", "SOM", "252"));
        countries.add(new Country("South Africa", "ZA", "ZAF", "27"));

        countries.add(new Country("South Korea", "KR", "KOR", "82"));
        countries.add(new Country("South Sudan", "SS", "SSD", "211"));
        countries.add(new Country("Spain", "ES", "ESP", "34"));
        countries.add(new Country("Sri Lanka", "LK", "LKA", "94"));
        countries.add(new Country("Sudan", "SD", "SDN", "249"));
        countries.add(new Country("Suriname", "SR", "SUR", "597"));
        countries.add(new Country("Svalbard", "SJ", "SJM", "47"));
        countries.add(new Country("Swaziland", "SZ", "SWZ", "268"));
        countries.add(new Country("Sweden", "SE", "SWE", "46"));
        countries.add(new Country("Switzerland", "CH", "CHE", "41"));
        countries.add(new Country("Syria", "SY", "SYR", "963"));
        countries.add(new Country("Taiwan", "TW", "TWN", "886"));
        countries.add(new Country("Tajikistan", "TJ", "TJK", "992"));
        countries.add(new Country("Tanzania", "TZ", "TZA", "255"));
        countries.add(new Country("Thailand", "TH", "THA", "66"));
        countries.add(new Country("Timor-Leste (East Timor)", "TL", "TLS", "670"));
        countries.add(new Country("Togo", "TG", "TGO", "228"));
        countries.add(new Country("Tokelau", "TK", "TKL", "690"));
        countries.add(new Country("Tonga Islands", "TO", "TON", "676"));
        countries.add(new Country("Trinidad and Tobago", "TT", "TTO", "1 868"));
        countries.add(new Country("Tunisia", "TN", "TUN", "216"));
        countries.add(new Country("Turkey", "TR", "TUR", "90"));
        countries.add(new Country("Turkmenistan", "TM", "TKM", "993"));
        countries.add(new Country("Turks and Caicos Islands", "TC", "TCA", "1 649"));
        countries.add(new Country("Tuvalu", "TV", "TUV", "688"));
        countries.add(new Country("Uganda", "UG", "UGA", "256"));
        countries.add(new Country("Ukraine", "UA", "UKR", "380"));
        countries.add(new Country("United Arab Emirates", "AE", "ARE", "971"));
        countries.add(new Country("United Kingdom", "GB", "GBR", "44"));
        countries.add(new Country("United States", "US", "USA", "1"));
        countries.add(new Country("Uruguay", "UY", "URY", "598"));
        countries.add(new Country("US Virgin Islands", "VI", "VIR", "1 340"));
        countries.add(new Country("Uzbekistan", "UZ", "UZB", "998"));
        countries.add(new Country("Vanuatu", "VU", "VUT", "678"));
        countries.add(new Country("Venezuela", "VE", "VEN", "58"));
        countries.add(new Country("Vietnam", "VN", "VNM", "84"));
        countries.add(new Country("Wallis and Futuna", "WF", "WLF", "681"));
        countries.add(new Country("Western Sahara", "EH", "ESH", "212"));
        countries.add(new Country("Yemen", "YE", "YEM", "967"));
        countries.add(new Country("Zambia", "ZM", "ZMB", "260"));
        countries.add(new Country("Zimbabwe", "ZW", "ZWE", "263"));

        return countries;
    }

    public static class Country{

        public Country(String country, String code1, String code2, String code3){
            this.country = country;
            this.code1 = code1;
            this.code2 = code2;
            this.code3 = code3;
        }

        public Country(){
            this.country = "";
            this.code1 = "";
            this.code2 = "";
            this.code3 = "";
        }

        public String country;
        public String code1;
        public String code2;
        public String code3;
    }

}
