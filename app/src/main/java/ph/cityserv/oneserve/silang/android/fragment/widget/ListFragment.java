package ph.cityserv.oneserve.silang.android.fragment.widget;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.WidgetActivity;
import ph.cityserv.oneserve.silang.android.adapter.WidgetRecyclerViewAdapter;
import ph.cityserv.oneserve.silang.data.model.server.WidgetItem;
import ph.cityserv.oneserve.silang.server.request.Widget;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.DividerItemDecoration;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;

public class ListFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener,
        WidgetRecyclerViewAdapter.ClickListener{
    public static final String TAG = ListFragment.class.getName().toString();

    private WidgetActivity widgetActivity;
    private WidgetRecyclerViewAdapter widgetRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private WidgetItem widgetItem;

    @BindView(R.id.widgetSRL)       SwipeRefreshLayout widgetSRL;
    @BindView(R.id.widgetRV)        RecyclerView widgetRV;

    public static ListFragment newInstance(WidgetItem widgetItem) {
        ListFragment fragment = new ListFragment();
        fragment.widgetItem = widgetItem;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_widget_list;
    }

    @Override
    public void onViewReady() {
        widgetActivity = (WidgetActivity) getContext();
        setUpListView();
    }

    private void refreshList(){
        Widget.getDefault(getContext()).show(widgetSRL, widgetItem.id);
    }

    private void setUpListView(){
        widgetSRL.setOnRefreshListener(this);
        widgetRecyclerViewAdapter = new WidgetRecyclerViewAdapter(getContext());
        widgetRecyclerViewAdapter.setClickListener(this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        widgetRV.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL)
                .setDividerHeight(getContext().getResources().getDimensionPixelSize(R.dimen.default_dimen)));
        widgetRV.setLayoutManager(linearLayoutManager);
        widgetRV.setAdapter(widgetRecyclerViewAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Widget.WidgetShowResponse responseData){
        SingleTransformer<WidgetItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            widgetActivity.setTitle(singleTransformer.data.title);
            widgetRecyclerViewAdapter.setNewData(singleTransformer.data.children.data);
        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onItemClick(WidgetItem widgetItem) {
        switch (widgetItem.type){
            case "single_page":
                widgetActivity.startWidgetActivity(widgetItem, "single_page");
                break;
            case "list":
                widgetActivity.startWidgetActivity(widgetItem, "list");
                break;
        }
    }
}
