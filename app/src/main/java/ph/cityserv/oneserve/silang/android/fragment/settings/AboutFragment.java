package ph.cityserv.oneserve.silang.android.fragment.settings;


import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.SettingsActivity;
import ph.cityserv.oneserve.silang.android.dialog.WebViewDialog;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;

public class AboutFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = AboutFragment.class.getName().toString();

    private SettingsActivity settingsActivity;

    @BindView(R.id.versionTXT)          TextView versionTXT;
    @BindView(R.id.iloiloLinkTXT)       TextView iloiloLinkTXT;
    @BindView(R.id.microsoftLinkTXT)    TextView microsoftLinkTXT;
    @BindView(R.id.cityServLinkTXT)     TextView cityServLinkTXT;
    @BindView(R.id.hsLinkTXT)           TextView hsLinkTXT;


    public static AboutFragment newInstance() {
        AboutFragment fragment = new AboutFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_about;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getActivity();
        settingsActivity.setTitle(getString(R.string.about_header));

        versionTXT.setOnClickListener(this);
        iloiloLinkTXT.setOnClickListener(this);
        microsoftLinkTXT.setOnClickListener(this);
        cityServLinkTXT.setOnClickListener(this);
        hsLinkTXT.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.versionTXT:
                WebViewDialog.newInstance(cityServLinkTXT.getText().toString()).show(getChildFragmentManager(), WebViewDialog.TAG);
                break;
            case R.id.iloiloLinkTXT:
                WebViewDialog.newInstance(iloiloLinkTXT.getText().toString()).show(getChildFragmentManager(), WebViewDialog.TAG);
                break;
            case R.id.microsoftLinkTXT:
                WebViewDialog.newInstance(microsoftLinkTXT.getText().toString()).show(getChildFragmentManager(), WebViewDialog.TAG);
                break;
            case R.id.cityServLinkTXT:
                WebViewDialog.newInstance(cityServLinkTXT.getText().toString()).show(getChildFragmentManager(), WebViewDialog.TAG);
                break;
            case R.id.hsLinkTXT:
                WebViewDialog.newInstance(hsLinkTXT.getText().toString()).show(getChildFragmentManager(), WebViewDialog.TAG);
                break;
        }
    }
}
