package ph.cityserv.oneserve.silang.android.dialog;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.route.RouteActivity;
import ph.cityserv.oneserve.silang.data.model.server.UserItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.server.request.Auth;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseDialog;
import ph.cityserv.oneserve.silang.vendor.android.java.ToastMessage;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;

public class LogoutDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = LogoutDialog.class.getName().toString();

	@BindView(R.id.continueTV) TextView continueTV;
	@BindView(R.id.cancelTV) TextView cancelTV;

	public static LogoutDialog newInstance() {
		LogoutDialog fragment = new LogoutDialog();
		return fragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_logout;
	}

	@Override
	public void onViewReady() {
		continueTV.setOnClickListener(this);
		cancelTV.setOnClickListener(this);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
		EventBus.getDefault().register(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.continueTV:
				attemptLogout();
				break;
			case R.id.cancelTV:
				dismiss();
				break;
		}
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	private void attemptLogout(){
		Auth.getDefault(getContext()).logout();
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	@Subscribe
	public void onResponse(Auth.LoginResponse responseData) {
		SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
		if(responseData.isSuccess()){
			UserData.clearData();
			((RouteActivity) getContext()).startLandingActivity();
			((RouteActivity) getContext()).finish();
			ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
		}else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
			((RouteActivity) getContext()).startLandingActivity();
			UserData.clearData();

        }
	}
}
