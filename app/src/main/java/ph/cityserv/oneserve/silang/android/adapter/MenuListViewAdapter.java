package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.app.MenuItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseListViewAdapter;

public class MenuListViewAdapter extends BaseListViewAdapter<MenuListViewAdapter.ViewHolder, MenuItem> {

    private ClickListener clickListener;

    public MenuListViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_widget));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setOnClickListener(this);
        holder.widgetNameTXT.setText(holder.getItem().name);
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onWidgetItemClickListener(((ViewHolder) v.getTag()).getItem());
        }
    }

    public class ViewHolder extends BaseListViewAdapter.ViewHolder{

        @BindView(R.id.widgetNameTXT)         TextView widgetNameTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public MenuItem getItem() {
            return (MenuItem) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends Listener{
        void onWidgetItemClickListener(MenuItem menuItem);
    }
} 
