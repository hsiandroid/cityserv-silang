package ph.cityserv.oneserve.silang.vendor.android.java;

/**
 * Created by BCTI 3 on 10/25/2016.
 */

public class StringManager {

    public static String textCapWord(String string){
        if(string == null){
            return "";
        }

        StringBuilder finalString = new StringBuilder();

        String[] words = string.split(" ");

        for ( String word : words) {
            StringBuilder temp = new StringBuilder();
            if(word.length() > 0){
                temp.append(word.substring(0, 1).toUpperCase());
            }
            if(word.length() > 1){
                temp.append(word.substring(1).toLowerCase());
            }

            finalString.append(temp.toString());
            finalString.append(" ");
        }

        return finalString.toString();
    }
}
