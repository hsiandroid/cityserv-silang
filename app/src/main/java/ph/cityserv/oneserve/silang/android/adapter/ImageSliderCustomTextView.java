package ph.cityserv.oneserve.silang.android.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.slider.library.SliderTypes.TextSliderView;

import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.NewsItem;

/**
 * Created by BCTI 3 on 10/18/2016.
 */

public class ImageSliderCustomTextView extends TextSliderView {

    private NewsItem newsItem;

    public ImageSliderCustomTextView(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.item_image_slider_custom, null);

            ImageView target = view.findViewById(R.id.slider_img);
            TextView description = view.findViewById(R.id.home_description_txt);
            TextView date = view.findViewById(R.id.home_date_txt);

            description.setText(Html.fromHtml(getNewsItem().info.data.content));
            date.setText(getNewsItem().date.data.time_passed);
            image(getNewsItem().info.data.full_path);
            bindEventAndShow(view, target);
        return view;
    }

    public ImageSliderCustomTextView setImageSliderItem(NewsItem newsItem){
        this.newsItem = newsItem;
        return this;
    }

    public NewsItem getNewsItem(){
        return newsItem;
    }

}
