package ph.cityserv.oneserve.silang.android.fragment.registration;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.RegistrationActivity;
import ph.cityserv.oneserve.silang.data.model.server.UserItem;
import ph.cityserv.oneserve.silang.server.request.Auth;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.ToastMessage;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;
import ph.cityserv.oneserve.silang.vendor.server.util.ErrorResponseManger;

public class ResetPasswordFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = ResetPasswordFragment.class.getName().toString();

    private RegistrationActivity registrationActivity;

    @BindView(R.id.confirmNewPasswordTV)        TextView confirmNewPasswordTV;
    @BindView(R.id.resendVerificationCodeTV)    TextView resendVerificationCodeTV;
    @BindView(R.id.emailET)                     EditText emailET;
    @BindView(R.id.verificationCodeET)          EditText verificationCodeET;
    @BindView(R.id.passwordET)                  EditText passwordET;
    @BindView(R.id.confirmPasswordET)           EditText confirmPasswordET;
    @BindView(R.id.backBTN)                     View backBTN;

    @State String email;

    public static ResetPasswordFragment newInstance(String email) {
        ResetPasswordFragment fragment = new ResetPasswordFragment();
        fragment.email = email;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_reset_password;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getContext();

        emailET.setText(email);
        confirmNewPasswordTV.setOnClickListener(this);
        resendVerificationCodeTV.setOnClickListener(this);
        backBTN.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.confirmNewPasswordTV:
                attemptReset();
                break;
            case R.id.resendVerificationCodeTV:
                attemptResendRequestVerification();
                break;
            case R.id.backBTN:
                registrationActivity.onBackPressed();
                break;
        }
    }

    private void attemptReset(){
        Auth.getDefault(getContext()).resetPassword(emailET.getText().toString(), passwordET.getText().toString(),
                confirmPasswordET.getText().toString(), verificationCodeET.getText().toString());
    }

    private void attemptResendRequestVerification(){
        Auth.getDefault(getContext()).forgotPassword(emailET.getText().toString());
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if (singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
        }else {
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Auth.ResetPasswordResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if (singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            registrationActivity.startLandingActivity();
        }else {
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
            ErrorResponseManger.first(verificationCodeET, singleTransformer.errors.validation_token);
            ErrorResponseManger.first(passwordET, singleTransformer.errors.password);
            ErrorResponseManger.first(confirmPasswordET, singleTransformer.errors.password);
        }
    }
}
