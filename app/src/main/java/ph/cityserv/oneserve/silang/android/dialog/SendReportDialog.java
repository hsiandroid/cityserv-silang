package ph.cityserv.oneserve.silang.android.dialog;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.system.ErrnoException;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import icepick.State;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.CitizenReportItem;
import ph.cityserv.oneserve.silang.data.model.server.MomentItem;
import ph.cityserv.oneserve.silang.server.request.CitizenReport;
import ph.cityserv.oneserve.silang.server.request.Moment;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseDialog;
import ph.cityserv.oneserve.silang.vendor.android.java.ImageManager;
import ph.cityserv.oneserve.silang.vendor.android.java.PermissionChecker;
import ph.cityserv.oneserve.silang.vendor.android.java.ToastMessage;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;

public class SendReportDialog extends BaseDialog implements View.OnClickListener, TypeDialog.Callback{
    public static final String TAG = SendReportDialog.class.getName().toString();
    private File photoFile;

    private static final int REQUEST_CAMERA = 10001;
    private static final int REQUEST_GALLERY = 10002;
    private static final int PERMISSION_CAMERA = 101;
    private static final int PERMISSION_GALLERY = 102;
    private  Callback callback;

    @BindView(R.id.momentIMG)           ImageView momentIMG;
    @BindView(R.id.cameraBTN)           View cameraBTN;
    @BindView(R.id.galleryBTN)          View galleryBTN;
    @BindView(R.id.rotateLeftBTN)       View rotateLeftBTN;
    @BindView(R.id.rotateRightBTN)      View rotateRightBTN;
    @BindView(R.id.momentCON)           View momentCON;
    @BindView(R.id.uploadBTN)           View uploadBTN;
    @BindView(R.id.backBTN)             View backBTN;
    @BindView(R.id.typeBTN)             View typeBTN;
    @BindView(R.id.messageET)           EditText messageET;
    @BindView(R.id.titleBarTXT)         TextView titleBarTXT;
    @BindView(R.id.typeTXT)             TextView typeTXT;

    @State float rotate;
    @State boolean hasImage = false;

    public static SendReportDialog newInstance(Callback callback) {
        SendReportDialog dialog = new SendReportDialog();
        dialog.callback = callback;
        return dialog;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.dialog_send_report;
    }

    @Override
    public void onViewReady() {
		titleBarTXT.setText("CREATE REPORT");
        cameraBTN.setOnClickListener(this);
        galleryBTN.setOnClickListener(this);
        rotateLeftBTN.setOnClickListener(this);
        rotateRightBTN.setOnClickListener(this);
        uploadBTN.setOnClickListener(this);
        backBTN.setOnClickListener(this);
        typeBTN.setOnClickListener(this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    private void openGallery() {
        if (PermissionChecker.checkPermissions(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSION_GALLERY)) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(intent, REQUEST_GALLERY);
        }
    }

    private void openCamera(){
        if(PermissionChecker.checkPermissions(getActivity(), new String[] {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CAMERA)){
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                photoFile = createImageFile();
                if (photoFile != null) {
                    Uri uri = Uri.fromFile(photoFile);
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                            | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    if (uri != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    }
                    startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                }
            }
        }
    }

    private File createImageFile() {
        String imageFileName = "TEMP";
        File storageDir = new File(Environment.getExternalStorageDirectory(), getString(R.string.app_name) + "/data/img/temp");
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".png",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CAMERA:
            case REQUEST_GALLERY:
                if (resultCode == Activity.RESULT_OK) {
                    showImage(getPickImageResultUri(data));
                }
                break;
        }
    }

    private void rotateLeft(){
        rotate = rotate - 90f;
        momentIMG.setRotation(rotate);
    }

    private void rotateRight(){
        rotate = rotate + 90f;
        momentIMG.setRotation(rotate);
    }

    public void showImage(Uri imageUri) {
        boolean requirePermissions = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                isUriRequiresPermissions(imageUri)) {

            requirePermissions = true;
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
        }

        if (!requirePermissions) {
            rotate = 0f;
            momentIMG.setRotation(rotate);
            momentIMG.setImageURI(imageUri);
            hasImage = true;
        }
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getActivity().getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (e.getCause() instanceof ErrnoException) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        if (photoFile != null) {
            outputFileUri = Uri.fromFile(photoFile);
        }
        return outputFileUri;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CAMERA) {
            if (getBaseActivity().isAllPermissionResultGranted(grantResults)) {
                openCamera();
            }
        }
        if (requestCode == PERMISSION_GALLERY) {
            if (getBaseActivity().isAllPermissionResultGranted(grantResults)) {
                openGallery();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cameraBTN:
                openCamera();
                break;
            case R.id.galleryBTN:
                openGallery();
                break;
            case R.id.rotateLeftBTN:
                rotateLeft();
                break;
            case R.id.rotateRightBTN:
                rotateRight();
                break;
            case R.id.uploadBTN:
                attemptSendReport();
                break;
            case R.id.backBTN:
                dismiss();
                break;
            case R.id.typeBTN:
                TypeDialog.newInstance(this).show(getFragmentManager(), TAG);
                break;
        }
    }

    private void attemptSendReport(){
        if(!hasImage) {
            ToastMessage.show(getActivity(), "Image is required.", ToastMessage.Status.FAILED);
            return;
        }

        ImageManager.getFileFromBitmap(getContext(), momentCON, new ImageManager.Callback() {
            @Override
            public void success(File file) {
                CitizenReport.getDefault(getContext()).create(file, messageET.getText().toString(), typeTXT.getText().toString());
            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        setDialogMatchParent();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(CitizenReport.CreateReportResponse responseData){
        SingleTransformer<CitizenReportItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            if(callback != null){
                callback.onSuccess();
                dismiss();
            }
        }
    }

    @Override
    public void onSuccess(String categoryName, int categoryID) {
        typeTXT.setText(categoryName);
    }

    public interface Callback {
        void onSuccess();
    }

}
