package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class OfficialItem extends AndroidModel {


    @SerializedName("name")
    public String name;

    @SerializedName("position")
    public String position;

    @SerializedName("sorting")
    public int sorting;

    @SerializedName("excerpt")
    public String excerpt;

    @SerializedName("content")
    public String content;

    @SerializedName("path")
    public String path;

    @SerializedName("directory")
    public String directory;

    @SerializedName("full_path")
    public String fullPath;

    @SerializedName("thumb_path")
    public String thumbPath;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public OfficialItem convertFromJson(String json) {
        return convertFromJson(json, OfficialItem.class);
    }
}
