package ph.cityserv.oneserve.silang.android.fragment.main;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.MainActivity;
import ph.cityserv.oneserve.silang.android.adapter.MomentRecyclerViewAdapter;
import ph.cityserv.oneserve.silang.android.dialog.SendMomentDialog;
import ph.cityserv.oneserve.silang.android.dialog.defaultdialog.ConfirmationDialog;
import ph.cityserv.oneserve.silang.data.model.server.MomentItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.server.request.Moment;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.EndlessRecyclerViewScrollListener;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;

public class MomentFragment extends BaseFragment implements
        MomentRecyclerViewAdapter.ClickListener,
        SwipeRefreshLayout.OnRefreshListener, EndlessRecyclerViewScrollListener.Callback,
        View.OnClickListener{

    public static final String TAG = MomentFragment.class.getName().toString();

    private MainActivity mainActivity;

    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private APIRequest apiRequest;
    private MomentRecyclerViewAdapter momentRecyclerViewAdapter;

    @BindView(R.id.placeholderMoment)   ImageView placeholderMoment;
    @BindView(R.id.momentSRL)           SwipeRefreshLayout momentSRL;
    @BindView(R.id.momentRV)            RecyclerView momentRV;
    @BindView(R.id.momentFAB)           FloatingActionButton momentFAB;

    public static MomentFragment newInstance() {
        MomentFragment fragment = new MomentFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_moment;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_moment));
        momentFAB.setOnClickListener(this);
        seUpMomentsListView();
        Log.d(TAG, "onViewReady: " + String.valueOf(UserData.getUserId()));
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void seUpMomentsListView(){
        momentSRL.setOnRefreshListener(this);
        linearLayoutManager = new LinearLayoutManager(getContext());

        momentRecyclerViewAdapter = new MomentRecyclerViewAdapter(getContext());
        momentRecyclerViewAdapter.setonItemClickListener(this);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        momentRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        momentRV.setAdapter(momentRecyclerViewAdapter);
        momentRV.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onItemClick(MomentItem momentItem) {

    }

    @Override
    public void onItemDeleteClick(final MomentItem momentItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to delete this moment?")
                .setIcon(R.drawable.icon_information)
                .setNegativeButtonText("Cancel")
                .setNegativeButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmationDialog.dismiss();
                    }
                })
                .setPositiveButtonText("Delete")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteMoment(momentItem.id);
                        confirmationDialog.dismiss();
                    }
                })
                .build(getChildFragmentManager());
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Subscribe
    public void onResponse(Moment.MyMomentResponse responseData){
        CollectionTransformer<MomentItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            if(responseData.isNext()){
                momentRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                momentRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }
            showHidePlaceholder();

        }
    }

    @Subscribe
    public void onResponse(Moment.DeleteMomentResponse responseData){
        SingleTransformer<MomentItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            momentRecyclerViewAdapter.removeItem(singleTransformer.data);
            showHidePlaceholder();
        }
    }

    @Subscribe
    public void onResponse(Moment.CreateMomentResponse responseData){
        SingleTransformer<MomentItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            momentRecyclerViewAdapter.addNewItem(singleTransformer.data, 0);
            linearLayoutManager.scrollToPosition(0);
        }
    }

    private void showHidePlaceholder(){
        placeholderMoment.setVisibility(momentRecyclerViewAdapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
        momentRV.setVisibility(momentRecyclerViewAdapter.getItemCount() > 0 ? View.VISIBLE : View.GONE);
    }

    public void refreshList(){
       apiRequest =  Moment.getDefault(getContext()).myMoment(momentSRL);
       apiRequest.first();
    }

    public void deleteMoment(int id){
        Moment.getDefault(getContext()).delete(id);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.momentFAB:
                SendMomentDialog.newInstance().show(getFragmentManager(), SendMomentDialog.TAG);
                break;
        }
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }
}
