package ph.cityserv.oneserve.silang.android.dialog;

import android.view.View;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import icepick.State;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.CitizenReportItem;
import ph.cityserv.oneserve.silang.server.request.CitizenReport;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseDialog;
import ph.cityserv.oneserve.silang.vendor.android.java.StringFormatter;
import ph.cityserv.oneserve.silang.vendor.android.widget.ResizableImageView;

public class CitizenReportDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = CitizenReportDialog.class.getName().toString();

	public static CitizenReportDialog newInstance(CitizenReportItem citizenReportItem) {
		CitizenReportDialog dialog = new CitizenReportDialog();
		dialog.citizenReportItem = citizenReportItem;
		return dialog;
	}

	private CitizenReportItem citizenReportItem;

	@BindView(R.id.backBTN) 			View backBTN;
	@BindView(R.id.imageIV) 			ResizableImageView imageRIV;
	@BindView(R.id.dateTXT) 			TextView dateTXT;
	@BindView(R.id.contentTXT)			TextView contentTXT;


	@Override
	public int onLayoutSet() {
		return R.layout.dialog_citizen_report;
	}

	@Override
	public void onViewReady() {
		backBTN.setOnClickListener(this);
		displayNews(citizenReportItem);
	}

	private void displayNews(CitizenReportItem citizenReportItem){
		Picasso.with(getContext())
				.load(citizenReportItem.info.data.fullPath)
				.placeholder(R.drawable.placeholder_logo)
				.error(R.drawable.placeholder_logo)
				.into(imageRIV);

		contentTXT.setText(StringFormatter.fromHtml(citizenReportItem.info.data.content));

		dateTXT.setText(citizenReportItem.date.data.timePassed);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.backBTN:
				dismiss();
				break;
		}
	}

}
