package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.app.SurveyItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseListViewAdapter;

public class SurveyListViewAdapter extends BaseListViewAdapter<SurveyListViewAdapter.ViewHolder, SurveyItem> {

    private ClickListener clickListener;

    public SurveyListViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_survey));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.numberTXT.setText(String.valueOf(holder.getItem().number) + ".");
        holder.questionTXT.setText(holder.getItem().question);
        holder.noteCON.setVisibility(holder.getItem().hasNote ? View.VISIBLE : View.GONE);
        if(holder.getItem().hasNote){
            holder.note1TXT.setText(holder.getItem().note1);
            holder.note2TXT.setText(holder.getItem().note2);
        }

        holder.radioCON.setVisibility(holder.getItem().isRadio ? View.VISIBLE : View.GONE);
        if(holder.getItem().isRadio){
            holder.stronglyAgreeRDBTN.setChecked(holder.getItem().strongly_agree);
            holder.somewhatAgreeRDBTN.setChecked(holder.getItem().somewhat_agree);
            holder.stronglyDisAgreeRDBTN.setChecked(holder.getItem().strongly_disagree);
            holder.somewhatDisAgreeRDBTN.setChecked(holder.getItem().somewhat_disagree);
            holder.stronglyAgreeRDBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SurveyItem surveyItem = holder.getItem();
                    surveyItem.strongly_agree = true;
                    surveyItem.somewhat_agree = false;
                    surveyItem.strongly_disagree = false;
                    surveyItem.somewhat_disagree = false;
                    notifyDataSetChanged();
                }
            });

            holder.somewhatAgreeRDBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SurveyItem surveyItem = holder.getItem();
                    surveyItem.strongly_agree = false;
                    surveyItem.somewhat_agree = true;
                    surveyItem.strongly_disagree = false;
                    surveyItem.somewhat_disagree = false;
                    notifyDataSetChanged();
                }
            });

            holder.stronglyDisAgreeRDBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SurveyItem surveyItem = holder.getItem();
                    surveyItem.strongly_agree = false;
                    surveyItem.somewhat_agree = false;
                    surveyItem.strongly_disagree = true;
                    surveyItem.somewhat_disagree = false;
                    notifyDataSetChanged();
                }
            });

            holder.somewhatDisAgreeRDBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SurveyItem surveyItem = holder.getItem();
                    surveyItem.strongly_agree = false;
                    surveyItem.somewhat_agree = false;
                    surveyItem.strongly_disagree = false;
                    surveyItem.somewhat_disagree = true;
                    notifyDataSetChanged();
                }
            });
        }

        holder.answerCON.setVisibility(holder.getItem().isEditText ? View.VISIBLE : View.GONE);
        if(holder.getItem().isEditText){
//            holder.answerET.setText(holder.surveyItem.answer);
            holder.answerET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    SurveyItem surveyItem = holder.getItem();
                    surveyItem.answer = holder.answerET.getText().toString();
                }
            });
        }
    }

    public class ViewHolder extends BaseListViewAdapter.ViewHolder{

        @BindView(R.id.stronglyAgreeRDBTN)      RadioButton stronglyAgreeRDBTN;
        @BindView(R.id.stronglyDisAgreeRDBTN)   RadioButton stronglyDisAgreeRDBTN;
        @BindView(R.id.somewhatDisAgreeRDBTN)   RadioButton somewhatDisAgreeRDBTN;
        @BindView(R.id.somewhatAgreeRDBTN)      RadioButton somewhatAgreeRDBTN;
        @BindView(R.id.answerCON)               View answerCON;
        @BindView(R.id.answerET)                EditText answerET;
        @BindView(R.id.radioCON)                View radioCON;
        @BindView(R.id.numberTXT)               TextView numberTXT;
        @BindView(R.id.questionTXT)             TextView questionTXT;
        @BindView(R.id.noteCON)                 View noteCON;
        @BindView(R.id.note1TXT)                TextView note1TXT;
        @BindView(R.id.note2TXT)                TextView note2TXT;

        public ViewHolder(View view) {
            super(view);
        }

        public SurveyItem getItem() {
            return (SurveyItem) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick(((ViewHolder) v.getTag()).getItem());
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends Listener{
        void onItemClick(SurveyItem surveyItem);
    }
} 
