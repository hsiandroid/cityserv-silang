package ph.cityserv.oneserve.silang.android.dialog;

import android.view.View;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.TourismItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseActivity;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseDialog;
import ph.cityserv.oneserve.silang.vendor.android.java.LinkMovementMethod;
import ph.cityserv.oneserve.silang.vendor.android.java.StringFormatter;
import ph.cityserv.oneserve.silang.vendor.android.java.WrapContentWebView;
import ph.cityserv.oneserve.silang.vendor.android.widget.ResizableImageView;

public class TourismDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = TourismDialog.class.getName().toString();

	private TourismItem tourismItem;

	@BindView(R.id.backBTN)				View backBTN;
	@BindView(R.id.imageRIV)			ResizableImageView imageRIV;
	@BindView(R.id.dateTXT)				TextView dateTXT;
	@BindView(R.id.contentTXT)
	WrapContentWebView contentTXT;
	@BindView(R.id.titleBarTXT)         TextView titleBarTXT;

	public static TourismDialog newInstance(TourismItem tourismItem) {
		TourismDialog fragment = new TourismDialog();
		fragment.tourismItem = tourismItem;
		return fragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_news;
	}

	@Override
	public void onViewReady() {
		backBTN.setOnClickListener(this);
		displayNews(tourismItem);
	}

	private void displayNews(TourismItem tourismItem){
		Picasso.with(getContext())
				.load(tourismItem.info.data.full_path)
				.placeholder(R.drawable.placeholder_logo)
				.error(R.drawable.placeholder_logo)
				.into(imageRIV);

		titleBarTXT.setText(tourismItem.title);
//		contentTXT.setText(StringFormatter.fromHtml(tourismItem.info.data.content));
////		contentTXT.setMovementMethod(LinkMovementMethod.getInstance(getContext(), ((BaseActivity)getContext()).getSupportFragmentManager()));
		contentTXT.loadDataWithBaseURL(null, tourismItem.info.data.content, "text/html", "utf-8", null);
		dateTXT.setText(tourismItem.date.data.time_passed);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.backBTN:
				dismiss();
				break;
		}
	}
}
