package ph.cityserv.oneserve.silang.android.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


/**
 * Created by Ken Drew on 11/11/2017.
 */

public class SilangLguPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 2;
    private Context context;

    public SilangLguPagerAdapter(FragmentManager fragmentManager, Context c) {
        super(fragmentManager);
        context = c;
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:

            case 1:

            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_UNCHANGED;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return "TOWN OFFICIALS";
            case 1:
                return "CITY PHOTOS";
            default:
                return "Page" + position;

        }

    }

}