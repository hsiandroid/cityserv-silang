package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by BCTI 3 on 12/19/2016.
 */

public class ErrorItem  extends AndroidModel {

    @SerializedName("email")
    public List<String> email = null;

    @SerializedName("fname")
    public List<String> fname = null;

    @SerializedName("lname")
    public List<String> lname = null;

    @SerializedName("contact_number")
    public List<String> contact_number = null;

    @SerializedName("password")
    public List<String> password = null;

    @SerializedName("password_confirmation")
    public List<String> password_confirmation = null;

    @SerializedName("current_password")
    public List<String> current_password = null;

    @SerializedName("value")
    public List<String> value = null;
    public List<String> validation_token = null;
}
