package ph.cityserv.oneserve.silang.server.request;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import ph.cityserv.oneserve.silang.config.Keys;
import ph.cityserv.oneserve.silang.config.Url;
import ph.cityserv.oneserve.silang.data.model.server.MomentItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.request.APIResponse;
import ph.cityserv.oneserve.silang.vendor.server.request.BaseRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by BCTI 3 on 8/16/2017.
 */

public class Moment extends BaseRequest {

    public Moment(Context context) {
        super(context);
    }

    private String include = "info,date";

    public static Moment getDefault(Context context) {
        Moment fragment = new Moment(context);
        return fragment;
    }

    public APIRequest myMoment(SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<MomentItem>>(getContext()) {
            @Override
            public Call<CollectionTransformer<MomentItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionAPI(Url.getMyMoment(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new MyMomentResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .setPerPage(10)
                .showSwipeRefreshLayout(true);
        return apiRequest;
    }

    public void delete(int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<MomentItem>>(getContext()) {
            @Override
            public Call<SingleTransformer<MomentItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleAPI(Url.getDeleteMoment(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new DeleteMomentResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.MOMENT_ID, id)
                .showDefaultProgressDialog("Deleting...")
                .execute();
    }

    public void create(File file, String content) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<MomentItem>>(getContext()) {
            @Override
            public Call<SingleTransformer<MomentItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleAPI(Url.getCreateMoment(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new CreateMomentResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.CONTENT, content)
                .addParameter(Keys.FILE, file)
                .showDefaultProgressDialog("Uploading...")
                .execute();

    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<MomentItem>> requestCollectionAPI(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<MomentItem>> requestSingleAPI(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);
    }

    public class MyMomentResponse extends APIResponse<CollectionTransformer<MomentItem>> {
        public MyMomentResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class DeleteMomentResponse extends APIResponse<SingleTransformer<MomentItem>> {
        public DeleteMomentResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class CreateMomentResponse extends APIResponse<SingleTransformer<MomentItem>> {
        public CreateMomentResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
