package ph.cityserv.oneserve.silang.vendor.server.util;

import android.text.TextUtils;

/**
 * Created by BCTI 3 on 8/15/2017.
 */

public class UrlGenerator {

    private static final String UTF_8_CHARSET = "UTF8";

    public static UrlGenerator newInstance() {
        UrlGenerator fragment = new UrlGenerator();
        return fragment;
    }

    private static String getPassphraseSize16(String key) {
        if (TextUtils.isEmpty(key)) {
            return null;
        }
        char controlChar = '\u0014';
        String key16 = key + controlChar;
        if (key16.length() < 16) {
            while (key16.length() < 16) {
                key16 += key + controlChar;
            }
        }
        if (key16.length() > 16) {
            key16 = key16.substring(key16.length() - 16, key16.length());
        }
        return key16;
    }
}
