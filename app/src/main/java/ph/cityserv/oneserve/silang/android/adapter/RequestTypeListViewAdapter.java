package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.SubServiceItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseListViewAdapter;

public class RequestTypeListViewAdapter extends BaseListViewAdapter<RequestTypeListViewAdapter.ViewHolder, SubServiceItem> {

    private ClickListener clickListener;

    public RequestTypeListViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_request_type));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.adapterCON.setOnClickListener(this);

        holder.titleTXT.setText(holder.getItem().title);
        holder.requestTypeIndicatorView.setBackgroundColor(ActivityCompat.getColor(getContext(), ((position % 2) == 0) ? R.color.colorPrimary : R.color.colorPrimaryLight));
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((SubServiceItem) v.getTag());
        }
    }

    public class ViewHolder extends BaseListViewAdapter.ViewHolder{

        @BindView(R.id.titleTXT)                        TextView titleTXT;
        @BindView(R.id.requestTypeIndicatorView)        View requestTypeIndicatorView;
        @BindView(R.id.adapterCON)                      View adapterCON;

        public ViewHolder(View view) {
            super(view);
        }

        public SubServiceItem getItem() {
            return (SubServiceItem) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends Listener{
        void onItemClick(SubServiceItem subServiceItem);
    }
} 
