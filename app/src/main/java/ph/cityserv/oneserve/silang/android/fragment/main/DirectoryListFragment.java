package ph.cityserv.oneserve.silang.android.fragment.main;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.MainActivity;
import ph.cityserv.oneserve.silang.android.adapter.DirectoryListRecyclerViewAdapter;
import ph.cityserv.oneserve.silang.android.adapter.EstablishmentRecyclerViewAdapter;
import ph.cityserv.oneserve.silang.data.model.server.DirectoryItem;
import ph.cityserv.oneserve.silang.server.request.Directory;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.DividerItemDecoration;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;

public class DirectoryListFragment extends BaseFragment implements
        View.OnClickListener,
        DirectoryListRecyclerViewAdapter.ClickListener,
        SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = DirectoryListFragment.class.getName().toString();

    private MainActivity mainActivity;

    private DirectoryListRecyclerViewAdapter directoryListRecyclerViewAdapter;
    private LinearLayoutManager directoryLinearLayoutManager;

    private EstablishmentRecyclerViewAdapter establishmentRecyclerViewAdapter;
    private LinearLayoutManager establishmentLinearLayoutManager;

    @BindView(R.id.establishmentSRL)            SwipeRefreshLayout establishmentSRL;
    @BindView(R.id.establishmentRV)             RecyclerView establishmentRV;
    @BindView(R.id.listBTN)                     View listBTN;
    @BindView(R.id.mapBTN)                      View mapBTN;
    @BindView(R.id.directoryPB)                 ProgressBar directoryPB;
    @BindView(R.id.directoryRV)                 RecyclerView directoryRV;

    public static DirectoryListFragment newInstance() {
        DirectoryListFragment fragment = new DirectoryListFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_directory_list;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_directory));
        mapBTN.setOnClickListener(this);

        setUpDirectoryListView();
        setUpEstablishmentListView();
    }

    private void setUpDirectoryListView() {
        establishmentSRL.setOnRefreshListener(this);

        directoryListRecyclerViewAdapter = new DirectoryListRecyclerViewAdapter(getContext());
        directoryListRecyclerViewAdapter.setonItemClickListener(this);

        directoryLinearLayoutManager = new LinearLayoutManager(getContext());
        directoryRV.setLayoutManager(directoryLinearLayoutManager);
        directoryRV.setAdapter(directoryListRecyclerViewAdapter);
    }

    private void setUpEstablishmentListView() {
        establishmentLinearLayoutManager = new LinearLayoutManager(getContext());
        establishmentRV.setLayoutManager(establishmentLinearLayoutManager);
        establishmentRV.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(getResources().getDimensionPixelSize(R.dimen.default_dimen)));
        establishmentRecyclerViewAdapter = new EstablishmentRecyclerViewAdapter(getContext());
        establishmentRV.setAdapter(establishmentRecyclerViewAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mapBTN:
                mainActivity.openDirectoryMapFragment();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onItemClick(DirectoryItem directoryItem) {
        establishmentRecyclerViewAdapter.setNewData(directoryItem.establishments.data);
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Subscribe
    public void onResponse(Directory.DirectoryResponse responseData){
        CollectionTransformer<DirectoryItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            if(responseData.isNext()){
                directoryListRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else{
                directoryListRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }

            if(directoryListRecyclerViewAdapter.getItemCount() > 0){
                directoryListRecyclerViewAdapter.setSelected(0);
                establishmentRecyclerViewAdapter.setNewData(directoryListRecyclerViewAdapter.getItem(0).establishments.data);
            }
        }
    }

    public void refreshList(){
        Directory.getDefault(getContext()).all(establishmentSRL);
    }
}
