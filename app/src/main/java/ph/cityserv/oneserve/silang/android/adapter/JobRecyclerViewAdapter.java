package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.JobItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseListViewAdapter;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseRecylerViewAdapter;
import ph.cityserv.oneserve.silang.vendor.android.java.StringFormatter;
import ph.cityserv.oneserve.silang.vendor.android.widget.ResizableImageView;

public class JobRecyclerViewAdapter extends BaseRecylerViewAdapter<JobRecyclerViewAdapter.ViewHolder, JobItem> {

    private ClickListener clickListener;

    public JobRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_jobs));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.adapterCON.setOnClickListener(this);
        holder.applyBTN.setTag(holder.getItem());
        holder.applyBTN.setOnClickListener(this);

        holder.contentTXT.setText(StringFormatter.fromHtml(holder.getItem().excerpt));
        holder.dateTXT.setText(holder.getItem().date.data.timePassed);
        holder.nameTXT.setText(holder.getItem().title);

        if(holder.getItem().isApplied){
            holder.applyBTN.setEnabled(false);
            holder.applyBTN.setText("Applied");
        }else{
            holder.applyBTN.setText("Apply");
        }

        Picasso.with(getContext())
                .load(holder.getItem().info.data.fullPath)
                .placeholder(R.drawable.placeholder_silang)
                .error(R.drawable.placeholder_silang)
                .into(holder.imageIV);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.imageIV)             ResizableImageView imageIV;
        @BindView(R.id.contentTXT)          TextView contentTXT;
        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.dateTXT)             TextView dateTXT;
        @BindView(R.id.applyBTN)            TextView applyBTN;
        @BindView(R.id.adapterCON)          View adapterCON;

        public ViewHolder(View view) {
            super(view);
        }

        public JobItem getItem() {
            return (JobItem) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onClick(((JobItem) v.getTag()));
                }
                break;
            case R.id.applyBTN:
                if(clickListener != null){
                    clickListener.onApplyClick(((JobItem) v.getTag()));
                }
                break;

        }

    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends BaseListViewAdapter.Listener {
        void onClick(JobItem newsItem);
        void onApplyClick(JobItem newsItem);
    }
} 
