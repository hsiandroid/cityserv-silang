package ph.cityserv.oneserve.silang.android.dialog;

import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.route.RouteActivity;
import ph.cityserv.oneserve.silang.config.Keys;
import ph.cityserv.oneserve.silang.data.model.app.SettingsItem;
import ph.cityserv.oneserve.silang.data.model.server.UserItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.server.request.User;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseDialog;
import ph.cityserv.oneserve.silang.vendor.android.java.Keyboard;
import ph.cityserv.oneserve.silang.vendor.android.java.ToastMessage;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;
import ph.cityserv.oneserve.silang.vendor.server.util.ErrorResponseManger;

public class EditProfileDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = EditProfileDialog.class.getName().toString();

	private SettingsItem settingsItem;

	@BindView(R.id.contentET)				EditText contentET;
	@BindView(R.id.passwordET)				EditText passwordET;
	@BindView(R.id.confirmPasswordET)		EditText confirmPasswordET;
	@BindView(R.id.titleTXT)				TextView titleTXT;
	@BindView(R.id.saveBTN)					TextView saveBTN;
	@BindView(R.id.backBTN)					ImageView backBTN;

	public static EditProfileDialog newInstance(SettingsItem settingsItem) {
		EditProfileDialog fragment = new EditProfileDialog();
		fragment.settingsItem = settingsItem;
		return fragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_edit_profile;
	}

	@Override
	public void onViewReady() {
		saveBTN.setOnClickListener(this);
		backBTN.setOnClickListener(this);
		displayData();
	}

	private void displayData() {

        titleTXT.setText(settingsItem.title);
        contentET.setHint("Enter " + settingsItem.title);
        passwordET.setVisibility(View.GONE);
        confirmPasswordET.setVisibility(View.GONE);

		switch (settingsItem.id){
			case 1:
				contentET.setText(settingsItem.content);
                contentET.setSelection(contentET.getText().length());
				contentET.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
				break;
			case 2:
				contentET.setText(settingsItem.content);
                contentET.setSelection(contentET.getText().length());
				contentET.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
				break;
			case 3:
				contentET.setText(settingsItem.content);
                contentET.setSelection(contentET.getText().length());
				contentET.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
				break;
			case 4:
				contentET.setText(settingsItem.content);
                contentET.setSelection(contentET.getText().length());
				contentET.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
				break;
			case 5:
				contentET.setHint("Enter Old " + settingsItem.title);
				contentET.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
				contentET.setTransformationMethod(PasswordTransformationMethod.getInstance());
                contentET.setSelection(contentET.getText().length());
				passwordET.setVisibility(View.VISIBLE);
				confirmPasswordET.setVisibility(View.VISIBLE);
				break;
		}
    }

    private void attemptUpdate(){
        Keyboard.hideKeyboard((RouteActivity) getContext());
        if(settingsItem.id != 5){
            User.getDefault(getContext()).field()
					.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                    .addParameter(Keys.FIELD, settingsItem.key)
                    .addParameter(Keys.NICE_FIELD, settingsItem.title)
                    .addParameter(Keys.VALUE, contentET.getText().toString())
                    .showDefaultProgressDialog( "Updating "+ settingsItem.title + "...")
                    .execute();
        }else{
            User.getDefault(getContext()).password()
					.addParameter(Keys.CURRENT_PASSWORD, contentET.getText().toString())
                    .addParameter(Keys.PASSWORD, passwordET.getText().toString())
                    .addParameter(Keys.PASSWORD_CONFIRMATION, confirmPasswordET.getText().toString())
                    .showDefaultProgressDialog( "Updating "+ settingsItem.title + "...")
                    .execute();

        }
    }

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.saveBTN:
			    attemptUpdate();
				break;
			case R.id.backBTN:
				dismiss();
				break;
		}
	}

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        setDialogMatchParent();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(User.EditProfileResponse responseData){
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            UserData.insert(singleTransformer.data);
            dismiss();
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
            if(singleTransformer.hasRequirements()){
                ErrorResponseManger.first(contentET, singleTransformer.errors.value);
            }
        }
    }
}
