package ph.cityserv.oneserve.silang.android.route;

import android.content.Intent;
import android.os.Bundle;

import com.google.gson.Gson;

import ph.cityserv.oneserve.silang.android.activity.AssessorActivity;
import ph.cityserv.oneserve.silang.android.activity.HistoryActivity;
import ph.cityserv.oneserve.silang.android.activity.JobActivity;
import ph.cityserv.oneserve.silang.android.activity.LandingActivity;
import ph.cityserv.oneserve.silang.android.activity.MainActivity;
import ph.cityserv.oneserve.silang.android.activity.RegistrationActivity;
import ph.cityserv.oneserve.silang.android.activity.SettingsActivity;
import ph.cityserv.oneserve.silang.android.activity.WidgetActivity;
import ph.cityserv.oneserve.silang.data.model.server.RequestItem;
import ph.cityserv.oneserve.silang.data.model.server.SubServiceItem;
import ph.cityserv.oneserve.silang.data.model.server.WidgetItem;
import ph.cityserv.oneserve.silang.data.model.server.ServiceItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseActivity;
import ph.cityserv.oneserve.silang.vendor.android.base.RouteManager;

/**
 * Created by BCTI 3 on 2/6/2017.
 */

public class RouteActivity extends BaseActivity {

    public void startMainActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(MainActivity.class)
                .addActivityTag("main")
                .addFragmentTag(fragmentTAG)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
    }

    public void startLandingActivity() {
        RouteManager.Route.with(this)
                .addActivityClass(LandingActivity.class)
                .addActivityTag("registration")
                .addFragmentTag("splash")
                .startActivity();

        finish();
    }

    public void startRegistrationActivity(String email, String fragmentTAG) {
        Bundle bundle = new Bundle();
        bundle.putString("email", email);

        RouteManager.Route.with(this)
                .addActivityClass(RegistrationActivity.class)
                .addActivityTag("registration")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startSettingsActivity() {
        RouteManager.Route.with(this)
                .addActivityClass(SettingsActivity.class)
                .addActivityTag("settings")
                .addFragmentTag("splash")
                .startActivity();
    }

    public void startSettingsActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(SettingsActivity.class)
                .addActivityTag("settings")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startWidgetActivity(WidgetItem widgetItem, String fragmentTAG){
        Bundle bundle = new Bundle();
        bundle.putString("widget_item", new Gson().toJson(widgetItem));

        RouteManager.Route.with(this)
                .addActivityClass(WidgetActivity.class)
                .addActivityTag("widget")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startAssessorActivity(SubServiceItem subServiceItem, String fragmentTAG){
        Bundle bundle = new Bundle();
        bundle.putString("subservice_item", new Gson().toJson(subServiceItem));

        RouteManager.Route.with(this)
                .addActivityClass(AssessorActivity.class)
                .addActivityTag("assessor")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startCCActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(AssessorActivity.class)
                .addActivityTag("assessor")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startAssessorActivity(ServiceItem serviceItem, String fragmentTAG){
        Bundle bundle = new Bundle();
        bundle.putString("service_item", new Gson().toJson(serviceItem));

        RouteManager.Route.with(this)
                .addActivityClass(AssessorActivity.class)
                .addActivityTag("assessor")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startAssessorActivity(RequestItem requestItem, String fragmentTAG){
        Bundle bundle = new Bundle();
        bundle.putString("request_item", new Gson().toJson(requestItem));

        RouteManager.Route.with(this)
                .addActivityClass(AssessorActivity.class)
                .addActivityTag("assessor")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startJobActivty(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(JobActivity.class)
                .addActivityTag("job")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startHistoryActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(HistoryActivity.class)
                .addActivityTag("history")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }
}
