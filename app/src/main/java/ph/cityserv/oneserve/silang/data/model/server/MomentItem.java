package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 6/25/2016.
 */
public class MomentItem extends AndroidModel {

    @SerializedName("excerpt")
    public String excerpt;


    @SerializedName("date")
    public Date date;

    @SerializedName("info")
    public Info info;

    public class Date {

        @SerializedName("data")
        public DateItem data;
    }

    public class Info {

        @SerializedName("data")
        public Data data;

        public class Data {

            @SerializedName("sender")
            public String sender;

            @SerializedName("status")
            public String status;

            @SerializedName("content")
            public String content;

            @SerializedName("path")
            public String path;

            @SerializedName("directory")
            public String directory;

            @SerializedName("full_path")
            public String full_path;

            @SerializedName("thumb_path")
            public String thumb_path;
        }
    }
}
