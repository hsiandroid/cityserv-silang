package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;


/**
 * Created by Labyalo on 8/12/2017.
 */

public class BarangayModel extends AndroidModel {


    @SerializedName("specialty")
    public String specialty;

    @SerializedName("is_selected")
    public boolean isSelected;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public BarangayModel convertFromJson(String json) {
        return convertFromJson(json, BarangayModel.class);
    }
}
