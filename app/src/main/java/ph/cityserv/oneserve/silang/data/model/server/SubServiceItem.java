package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 6/25/2016.
 */
public class SubServiceItem extends AndroidModel{

    @SerializedName("title")
    public String title;

    @SerializedName("code")
    public String code;

    @SerializedName("date")
    public Date date;

    @SerializedName("info")
    public Info info;

    public class Date {

        @SerializedName("data")
        public DateItem data;
    }

    public class Info {

        @SerializedName("data")
        public Data data;

        public class Data {

            @SerializedName("content")
            public String content;
        }
    }
}
