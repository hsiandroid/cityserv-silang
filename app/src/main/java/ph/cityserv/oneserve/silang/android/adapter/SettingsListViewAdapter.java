package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.app.SettingsItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseListViewAdapter;

public class SettingsListViewAdapter extends BaseListViewAdapter<SettingsListViewAdapter.ViewHolder, SettingsItem> {

    private ClickListener clickListener;

    public SettingsListViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_settings));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));

        holder.adapterCON.setTag(holder.getItem());
        holder.adapterCON.setOnClickListener(this);

        holder.titleTXT.setText(holder.getItem().title);
        holder.contentTXT.setText(holder.getItem().content);
        holder.editBTN.setTag(holder.getItem());
        holder.editBTN.setOnClickListener(this);
        holder.editBTN.setVisibility(View.VISIBLE);

        switch (holder.getItem().id){
            case 4:
                holder.titleTXT.setInputType(InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS);
                holder.editBTN.setVisibility(View.GONE);
                break;
            case 5:
                holder.titleTXT.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                break;
        }
    }

    public class ViewHolder extends BaseListViewAdapter.ViewHolder{

        @BindView(R.id.titleTXT)        TextView titleTXT;
        @BindView(R.id.contentTXT)      TextView contentTXT;
        @BindView(R.id.editBTN)         ImageView editBTN;
        @BindView(R.id.adapterCON)      View adapterCON;

        public ViewHolder(View view) {
            super(view);
        }

        public SettingsItem getItem() {
            return (SettingsItem) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((SettingsItem) v.getTag());
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends Listener{
        void onItemClick(SettingsItem settingsItem);
    }
} 
