package ph.cityserv.oneserve.silang.data.model.app;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by BCTI 3 on 1/31/2017.
 */

public class SettingsItem extends AndroidModel {
    public String title;
    public String content;
    public String key;

    public SettingsItem(int id, String title, String content, String key){
        this.id = id;
        this.title = title;
        this.content = content;
        this.key = key;
    }
}
