package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 8/12/2017.
 */

public class SampleModel extends AndroidModel {

    @SerializedName("name")
    public String name;
}
