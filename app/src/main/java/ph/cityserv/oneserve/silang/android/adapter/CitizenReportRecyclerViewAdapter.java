package ph.cityserv.oneserve.silang.android.adapter;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.StrictMode;
import android.support.v7.widget.PopupMenu;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.CitizenReportItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseListViewAdapter;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseRecylerViewAdapter;

public class CitizenReportRecyclerViewAdapter extends BaseRecylerViewAdapter<CitizenReportRecyclerViewAdapter.ViewHolder, CitizenReportItem> {

    private ClickListener clickListener;

    public CitizenReportRecyclerViewAdapter(Context context) {
        super(context);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_citizen));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setTag(holder.getItem());
        holder.getBaseView().setOnClickListener(this);

        Picasso.with(getContext())
                .load(holder.getItem().info.data.fullPath)
                .placeholder(R.drawable.placeholder_logo)
                .error(R.drawable.placeholder_logo)
                .fit()
                .into(holder.imageIV);

        holder.contentTXT.setText(Html.fromHtml(holder.getItem().info.data.content));
        holder.dateTXT.setText(holder.getItem().date.data.timePassed);
        holder.statusTXT.setText(holder.getItem().info.data.status);
        holder.typeTXT.setText(holder.getItem().type);

        holder.adapterCON.setTag(holder.getItem());
        holder.adapterCON.setOnClickListener(this);
    }


    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.imageIV)             ImageView imageIV;
        @BindView(R.id.contentTXT)          TextView contentTXT;
        @BindView(R.id.statusTXT)           TextView statusTXT;
        @BindView(R.id.typeTXT)             TextView typeTXT;
        @BindView(R.id.dateTXT)             TextView dateTXT;
        @BindView(R.id.adapterCON)          View adapterCON;

        public ViewHolder(View view) {
            super(view);
        }

        public CitizenReportItem getItem() {
            return (CitizenReportItem) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((CitizenReportItem) v.getTag());
                }
                break;
        }
    }

    public void setonItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends BaseListViewAdapter.Listener {
        void onItemClick(CitizenReportItem citizenReportItem);
    }
} 
