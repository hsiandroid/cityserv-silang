package ph.cityserv.oneserve.silang.android.fragment.main;

import android.support.v4.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.MainActivity;
import ph.cityserv.oneserve.silang.android.adapter.WidgetListViewAdapter;
import ph.cityserv.oneserve.silang.data.model.server.WidgetItem;
import ph.cityserv.oneserve.silang.server.request.Article;
import ph.cityserv.oneserve.silang.server.request.Widget;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.widget.ExpandableHeightListView;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;

public class MyCityFragment extends BaseFragment implements
        WidgetListViewAdapter.ClickListener,
        SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = MyCityFragment.class.getName().toString();

    private MainActivity mainActivity;
    private WidgetListViewAdapter widgetListViewAdapter;

    @BindView(R.id.citySRL)                 SwipeRefreshLayout citySRL;
    @BindView(R.id.myCityEHLV)              ExpandableHeightListView myCityEHLV;

    public static MyCityFragment newInstance() {
        MyCityFragment fragment = new MyCityFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_my_city;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_city_history));
        setUpListView();
    }

    private void setUpListView(){
        citySRL.setOnRefreshListener(this);
        widgetListViewAdapter =  new WidgetListViewAdapter(getContext());
        myCityEHLV.setAdapter(widgetListViewAdapter);
        widgetListViewAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void refreshList(){
        Widget.getDefault(getContext()).all(citySRL, "my_city");
        Article.getDefault(getContext()).recent(citySRL);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onItemClick(WidgetItem widgetItem) {
//        switch (widgetItem.type){
//            case "single_page":
//                mainActivity.startWidgetActivity(widgetItem, "single_page");
//                break;
//            case "list":
//                mainActivity.startWidgetActivity(widgetItem, "list");
//                break;
//        }
        if(widgetItem.title.equalsIgnoreCase("Local Economic and Investment Promotion Office")){
            mainActivity.startJobActivty("leipo");
        }else if(widgetItem.code.equalsIgnoreCase("tourism")) {
            mainActivity.startJobActivty("tourism");
        }else if(widgetItem.type.equalsIgnoreCase("single_page")){
            mainActivity.startWidgetActivity(widgetItem, "single_page");
        }else if (widgetItem.type.equalsIgnoreCase("list")){
            mainActivity.startWidgetActivity(widgetItem, "list");
        }
    }

    @Subscribe
    public void onResponse(Widget.WidgetAllResponse responseData) {
        CollectionTransformer<WidgetItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            if(responseData.isNext()){
                widgetListViewAdapter.addNewData(collectionTransformer.data);
            }else{
                widgetListViewAdapter.setNewData(collectionTransformer.data);
            }
        }
    }
}
