package ph.cityserv.oneserve.silang.android.fragment.main;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.MainActivity;
import ph.cityserv.oneserve.silang.android.adapter.SettingsListViewAdapter;
import ph.cityserv.oneserve.silang.android.dialog.EditProfileDialog;
import ph.cityserv.oneserve.silang.android.dialog.LogoutDialog;
import ph.cityserv.oneserve.silang.config.Keys;
import ph.cityserv.oneserve.silang.data.model.app.SettingsItem;
import ph.cityserv.oneserve.silang.data.model.server.UserItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.server.request.User;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.widget.ExpandableHeightListView;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;

public class SettingsFragment extends BaseFragment implements SettingsListViewAdapter.ClickListener,View.OnClickListener {

    public static final String TAG = SettingsFragment.class.getName().toString();
    private MainActivity mainActivity;
    private SettingsListViewAdapter settingsListViewAdapter;

    @BindView(R.id.settingsEHLV)         ExpandableHeightListView settingsEHLV;
    @BindView(R.id.signOutBTN)           TextView signOutBTN;
    @BindView(R.id.aboutBTN)             TextView aboutBTN;
    @BindView(R.id.raffle_id)            TextView raffle_id;
    @BindView(R.id.raffleLL)             View raffleLL;

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_settings;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getContext();
        mainActivity.setTitle(getString(R.string.title_settings));
        signOutBTN.setOnClickListener(this);
        aboutBTN.setOnClickListener(this);
        setupProfileListView();
        Log.e("Items",">>>" + new Gson().toJson(UserData.getUserItem()));

        if (UserData.getUserItem().raffle_id != null){
            raffleLL.setVisibility(View.VISIBLE);
            raffle_id.setText(UserData.getUserItem().raffle_id);
        }else{
            raffleLL.setVisibility(View.GONE);
        }

    }

    private void setupProfileListView() {
        settingsListViewAdapter = new SettingsListViewAdapter(getContext());
        settingsListViewAdapter.setClickListener(this);
        settingsListViewAdapter.setNewData(getData(UserData.getUserItem()));

        settingsEHLV.setAdapter(settingsListViewAdapter);
    }

    private List<SettingsItem> getData(UserItem userItem) {
        List<SettingsItem> settingsItem = new ArrayList<>();
        String fname;
        String lname;
        if (userItem.info != null){
            fname =  userItem.info.data.fname;
            lname =  userItem.info.data.lname;
        }else {
            fname = "";
            lname = "";
        }
        settingsItem.add(new SettingsItem(1, getString(R.string.settings_firstname), fname, Keys.FNAME));
        settingsItem.add(new SettingsItem(2, getString(R.string.settings_lastname), lname, Keys.LNAME));
        settingsItem.add(new SettingsItem(3, getString(R.string.settings_user_name),userItem.username, Keys.USERNAME));
        settingsItem.add(new SettingsItem(4, getString(R.string.settings_email), userItem.email, Keys.EMAIL));
        settingsItem.add(new SettingsItem(5, getString(R.string.settings_password), "*******", Keys.PASSWORD));
        return settingsItem;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.signOutBTN:
                LogoutDialog.newInstance().show(getChildFragmentManager(),LogoutDialog.TAG);
                break;
            case R.id.aboutBTN:
                mainActivity.startSettingsActivity("about");
                break;
        }
    }

    @Override
    public void onItemClick(SettingsItem settingsItem) {
        EditProfileDialog.newInstance(settingsItem).show(getChildFragmentManager(),EditProfileDialog.TAG);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(User.EditProfileResponse responseData){
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            settingsListViewAdapter.setNewData(getData(singleTransformer.data));
        }else{

        }
    }
}