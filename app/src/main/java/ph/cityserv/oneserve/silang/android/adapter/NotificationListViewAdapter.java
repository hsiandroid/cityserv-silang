package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.NotificationItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseListViewAdapter;

public class NotificationListViewAdapter extends BaseListViewAdapter<NotificationListViewAdapter.ViewHolder, NotificationItem> {

    private ClickListener clickListener;

    public NotificationListViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_notification));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.titleTXT.setText(holder.getItem().title);
        holder.contentTXT.setText(holder.getItem().msg);
        holder.dateTXT.setText(holder.getItem().date.data.time_passed);
        holder.notificationIndicatorView.setBackgroundColor(ActivityCompat.getColor(getContext(), ((position % 2) == 0) ? R.color.colorPrimary : R.color.colorPrimaryLight));

    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick(((ViewHolder) v.getTag()).getItem());
        }
    }

    public class ViewHolder extends BaseListViewAdapter.ViewHolder{

        @BindView(R.id.notificationIndicatorView)       View notificationIndicatorView;
        @BindView(R.id.titleTXT)                        TextView titleTXT;
        @BindView(R.id.contentTXT)                      TextView contentTXT;
        @BindView(R.id.dateTXT)                         TextView dateTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public NotificationItem getItem() {
            return (NotificationItem) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends Listener{
        void onItemClick(NotificationItem notificationItem);
    }
} 
