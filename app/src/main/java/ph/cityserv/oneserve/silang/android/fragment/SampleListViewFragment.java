package ph.cityserv.oneserve.silang.android.fragment;

import android.widget.ListView;

import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.adapter.DefaultListViewAdapter;
import ph.cityserv.oneserve.silang.data.model.server.SampleModel;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class SampleListViewFragment extends BaseFragment {
    public static final String TAG = SampleListViewFragment.class.getName().toString();

    private DefaultListViewAdapter defaultListViewAdapter;

    @BindView(R.id.defaultLV)       ListView defaultLV;

    public static SampleListViewFragment newInstance() {
        SampleListViewFragment fragment = new SampleListViewFragment();
        return fragment;
    }

    @Override
    public void onViewReady() {
        defaultListViewAdapter = new DefaultListViewAdapter(getContext());
        defaultListViewAdapter.setNewData(getDefaultData());
        defaultLV.setAdapter(defaultListViewAdapter);
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_listview;
    }

    private List<SampleModel> getDefaultData(){
        List<SampleModel> androidModels = new ArrayList<>();
        SampleModel defaultItem;
        for(int i = 0; i < 20; i++){
            defaultItem = new SampleModel();
            defaultItem.id = i;
            defaultItem.name = "name " + i;
            androidModels.add(defaultItem);
        }
        return androidModels;
    }
}
