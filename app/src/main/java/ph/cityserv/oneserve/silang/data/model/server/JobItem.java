package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 8/12/2017.
 */

public class JobItem extends AndroidModel {


    @SerializedName("title")
    public String title;
    @SerializedName("allow_application")
    public String allowApplication;
    @SerializedName("status")
    public String status;
    @SerializedName("num_applied")
    public String numApplied;
    @SerializedName("is_applied")
    public boolean isApplied;
    @SerializedName("excerpt")
    public String excerpt;
    @SerializedName("info")
    public Info info;
    @SerializedName("date")
    public Date date;
    @SerializedName("phone_number")
    public String phoneNumber;
    @SerializedName("contact_number")
    public String contactNumber;
    @SerializedName("email")
    public String email;
    @SerializedName("address")
    public String address;

    public static class Info {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("content")
            public String content;
            @SerializedName("path")
            public Object path;
            @SerializedName("directory")
            public String directory;
            @SerializedName("full_path")
            public String fullPath;
            @SerializedName("thumb_path")
            public String thumbPath;
        }
    }

    public static class Date {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("date_db")
            public String dateDb;
            @SerializedName("month_year")
            public String monthYear;
            @SerializedName("time_passed")
            public String timePassed;
            @SerializedName("timestamp")
            public Timestamp timestamp;

            public static class Timestamp {
                @SerializedName("date")
                public String date;
                @SerializedName("timezone_type")
                public int timezoneType;
                @SerializedName("timezone")
                public String timezone;
            }
        }
    }
}
