package ph.cityserv.oneserve.silang.android.activity;

import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.fragment.widget.ListFragment;
import ph.cityserv.oneserve.silang.android.fragment.widget.WebFragment;
import ph.cityserv.oneserve.silang.android.route.RouteActivity;
import ph.cityserv.oneserve.silang.data.model.server.WidgetItem;


/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class WidgetActivity extends RouteActivity implements View.OnClickListener {
    public static final String TAG = WidgetActivity.class.getName().toString();

    @BindView(R.id.mainIconIV)       View mainIconIV;
    @BindView(R.id.mainTitleTXT)     TextView mainTitleTXT;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_widget;
    }

    @Override
    public void onViewReady() {
        mainIconIV.setOnClickListener(this);
        setTitle("");
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "list":
                openListFragment(new Gson().fromJson(getFragmentBundle().getString("widget_item"),WidgetItem.class));
                break;
            case "single_page":
                openWebFragment(new Gson().fromJson(getFragmentBundle().getString("widget_item"),WidgetItem.class));
                break;
        }
    }

    public void openWebFragment(WidgetItem widgetItem){
        switchFragment(WebFragment.newInstance(widgetItem));
    }

    public void openListFragment(WidgetItem widgetItem){
        switchFragment(ListFragment.newInstance(widgetItem));
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainIconIV:
                onBackPressed();
        }
    }

}
