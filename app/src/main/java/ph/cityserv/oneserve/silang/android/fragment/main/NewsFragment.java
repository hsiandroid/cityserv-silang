package ph.cityserv.oneserve.silang.android.fragment.main;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.MainActivity;
import ph.cityserv.oneserve.silang.android.adapter.NewsRecyclerViewAdapter;
import ph.cityserv.oneserve.silang.android.dialog.NewsDialog;
import ph.cityserv.oneserve.silang.data.model.server.NewsItem;
import ph.cityserv.oneserve.silang.server.request.Article;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.DividerItemDecoration;
import ph.cityserv.oneserve.silang.vendor.android.java.EndlessRecyclerViewScrollListener;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;

public class NewsFragment extends BaseFragment implements
        NewsRecyclerViewAdapter.ClickListener,
        SwipeRefreshLayout.OnRefreshListener, EndlessRecyclerViewScrollListener.Callback{

    public static final String TAG = NewsFragment.class.getName().toString();

    private MainActivity mainActivity;

    private NewsRecyclerViewAdapter newsRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private APIRequest apiRequest;

    @BindView(R.id.newsSRL)                 SwipeRefreshLayout newsSRL;
    @BindView(R.id.newsRV)                  RecyclerView newsRV;
    @BindView(R.id.newsPlaceholderCON)      View newsPlaceholderCON;

    public static NewsFragment newInstance() {
        NewsFragment fragment = new NewsFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_news;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_news));

        setUpNewsListView();
    }

    private void setUpNewsListView(){
        newsSRL.setOnRefreshListener(this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        newsRecyclerViewAdapter = new NewsRecyclerViewAdapter(getContext());
        newsRecyclerViewAdapter.setClickListener(this);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        newsRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        newsRV.setLayoutManager(linearLayoutManager);
        newsRV.setAdapter(newsRecyclerViewAdapter);
    }

    @Override
    public void onReadMoreClick(NewsItem newsItem) {
        NewsDialog.newInstance(newsItem).show(getChildFragmentManager(),NewsDialog.TAG);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Subscribe
    public void onResponse(Article.AllNewsResponse responseData){
        CollectionTransformer<NewsItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            if(responseData.isNext()){
                newsRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                newsRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }
        }
        if (newsRecyclerViewAdapter.getItemCount() == 0){
            newsRV.setVisibility(View.GONE);
            newsPlaceholderCON.setVisibility(View.VISIBLE);
        }else {
            newsRV.setVisibility(View.VISIBLE);
            newsPlaceholderCON.setVisibility(View.GONE);
        }
    }

    public void refreshList(){
        apiRequest = Article.getDefault(getContext()).all(newsSRL);
        apiRequest.first();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }
}
