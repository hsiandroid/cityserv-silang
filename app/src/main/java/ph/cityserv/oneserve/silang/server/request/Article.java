package ph.cityserv.oneserve.silang.server.request;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import ph.cityserv.oneserve.silang.config.Keys;
import ph.cityserv.oneserve.silang.config.Url;
import ph.cityserv.oneserve.silang.data.model.server.JobItem;
import ph.cityserv.oneserve.silang.data.model.server.LeipoItem;
import ph.cityserv.oneserve.silang.data.model.server.NewsItem;
import ph.cityserv.oneserve.silang.data.model.server.NewsItem;
import ph.cityserv.oneserve.silang.data.model.server.TourismItem;
import ph.cityserv.oneserve.silang.data.model.server.YouthAffairsItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.request.APIResponse;
import ph.cityserv.oneserve.silang.vendor.server.request.BaseRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by BCTI 3 on 8/16/2017.
 */

public class Article extends BaseRequest {
    public Article(Context context) {
        super(context);
    }

    private String include = "info,date";

    public static Article getDefault(Context context) {
        Article fragment = new Article(context);
        return fragment;
    }

    public APIRequest all(SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<NewsItem>>(getContext()) {
            @Override
            public Call<CollectionTransformer<NewsItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestAPI( Url.getAllArticle(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllNewsResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(10)
                .showSwipeRefreshLayout(true);
        return apiRequest;

    }

    public APIRequest allLeipo(SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<LeipoItem>>(getContext()) {
            @Override
            public Call<CollectionTransformer<LeipoItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestLeipo( Url.getAllLeipo(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllLeipoResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(10)
                .showSwipeRefreshLayout(true);
        return apiRequest;

    }

    public APIRequest allTourism(SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<TourismItem>>(getContext()) {
            @Override
            public Call<CollectionTransformer<TourismItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestTourism( Url.getAllTourism(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllTourismResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(10)
                .showSwipeRefreshLayout(true);
        return apiRequest;
    }

    public APIRequest allYouthAffairs(SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<YouthAffairsItem>>(getContext()) {
            @Override
            public Call<CollectionTransformer<YouthAffairsItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestYouthAffairs( Url.getAllYouth(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllYouthAffairsResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(10)
                .showSwipeRefreshLayout(true);
        return apiRequest;
    }

    public APIRequest jobList(SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<JobItem>>(getContext()) {
            @Override
            public Call<CollectionTransformer<JobItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestJobCollection(Url.getJobList(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllJobResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(10)
                .showSwipeRefreshLayout(true);
        return apiRequest;

    }

    public void recent(SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<NewsItem>>(getContext()) {
            @Override
            public Call<CollectionTransformer<NewsItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestAPI(Url.getRecentArticle(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RecentNewsResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .execute();

    }

    public void apply(String email, String name, String contact, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<JobItem>>(getContext()) {
            @Override
            public Call<SingleTransformer<JobItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestJobSingle(Url.getApplyJob(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ApplyResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.JOB_ID, id)
                .addParameter(Keys.EMAIL, email)
                .addParameter(Keys.NAME, name)
                .addParameter(Keys.CONTACT_NUMBER, contact)
                .showDefaultProgressDialog("Applying...")
                .execute();
    }

    public void peso() {
        APIRequest apiRequest = new APIRequest<SingleTransformer<PESOModel>>(getContext()) {
            @Override
            public Call<SingleTransformer<PESOModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestPESO(Url.getPeso(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new PesoResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<NewsItem>> requestAPI(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<LeipoItem>> requestLeipo(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<YouthAffairsItem>> requestYouthAffairs(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<TourismItem>> requestTourism(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);


        @Multipart
        @POST("{p}")
        Call<SingleTransformer<NewsItem>> requestSingleAPI(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<JobItem>> requestJobCollection(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<JobItem>> requestJobSingle(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<PESOModel>> requestPESO(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

    }

    public class RecentNewsResponse extends APIResponse<CollectionTransformer<NewsItem>> {
        public RecentNewsResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class AllNewsResponse extends APIResponse<CollectionTransformer<NewsItem>> {
        public AllNewsResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class AllYouthAffairsResponse extends APIResponse<CollectionTransformer<YouthAffairsItem>> {
        public AllYouthAffairsResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class AllTourismResponse extends APIResponse<CollectionTransformer<TourismItem>> {
        public AllTourismResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class AllLeipoResponse extends APIResponse<CollectionTransformer<LeipoItem>> {
        public AllLeipoResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class AllJobResponse extends APIResponse<CollectionTransformer<JobItem>> {
        public AllJobResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class ApplyResponse extends APIResponse<SingleTransformer<JobItem>> {
        public ApplyResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class PesoResponse extends APIResponse<SingleTransformer<PESOModel>> {
        public PesoResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

}
