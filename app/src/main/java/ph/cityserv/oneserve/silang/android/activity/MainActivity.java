package ph.cityserv.oneserve.silang.android.activity;

import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.adapter.MenuListViewAdapter;
import ph.cityserv.oneserve.silang.android.fragment.main.CitizenRegistrationID;
import ph.cityserv.oneserve.silang.android.fragment.main.CitizenReportingFragment;
import ph.cityserv.oneserve.silang.android.fragment.main.DirectoryListFragment;
import ph.cityserv.oneserve.silang.android.fragment.main.DirectoryMapFragment;
import ph.cityserv.oneserve.silang.android.fragment.main.HomeFragment;
import ph.cityserv.oneserve.silang.android.fragment.main.MayorsActionFragment;
import ph.cityserv.oneserve.silang.android.fragment.main.MomentFragment;
import ph.cityserv.oneserve.silang.android.fragment.main.MyCityFragment;
import ph.cityserv.oneserve.silang.android.fragment.main.NewsFragment;
import ph.cityserv.oneserve.silang.android.fragment.main.ServicesFragment;
import ph.cityserv.oneserve.silang.android.fragment.main.SettingsFragment;
import ph.cityserv.oneserve.silang.android.fragment.main.SilangLguFragment;
import ph.cityserv.oneserve.silang.android.route.RouteActivity;
import ph.cityserv.oneserve.silang.data.model.app.MenuItem;

public class MainActivity extends RouteActivity
        implements View.OnClickListener,MenuListViewAdapter.ClickListener{

    private MenuListViewAdapter widgetAdapter;

    @BindView(R.id.drawerLayout)            DrawerLayout drawerLayout;
    @BindView(R.id.navigationView)          NavigationView navigationView;
    @BindView(R.id.menuIV)                  View menuIV;
    @BindView(R.id.mainTitleTXT)            TextView mainTitleTXT;
    @BindView(R.id.widgetLV)                ListView widgetLV;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_main;
    }

    @Override
    public void onViewReady() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                        View.SYSTEM_UI_FLAG_IMMERSIVE);
        disableNavigationViewScrollbars();
        navigationView.getBackground().setAlpha(180);
        hideNavigationDrawer();

        menuIV.setOnClickListener(this);

        setUpWidgetListView();
        menuIV.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        openHomeFragment();
    }


    private void setUpWidgetListView(){
        widgetAdapter =  new MenuListViewAdapter(getContext());
        widgetLV.setAdapter(widgetAdapter);
        widgetAdapter.setClickListener(this);
        widgetAdapter.setNewData(getWidgetItems());
    }

    private List<MenuItem> getWidgetItems(){
        List<MenuItem> menuItems = new ArrayList<>();

        MenuItem menuItem = new MenuItem();
        menuItem.id = 1;
        menuItem.name = getString(R.string.nav_home);
        menuItems.add(menuItem);

        menuItem = new MenuItem();
        menuItem.id = 2;
        menuItem.name = getString(R.string.nav_moments);
        menuItems.add(menuItem);

        menuItem = new MenuItem();
        menuItem.id = 3;
        menuItem.name = getString(R.string.nav_action);
        menuItems.add(menuItem);

        menuItem = new MenuItem();
        menuItem.id = 4;
        menuItem.name = getString(R.string.nav_jobs);
        menuItems.add(menuItem);

        menuItem = new MenuItem();
        menuItem.id = 5;
        menuItem.name = getString(R.string.nav_lgu);
        menuItems.add(menuItem);

        menuItem = new MenuItem();
        menuItem.id = 6;
        menuItem.name = getString(R.string.title_news);
        menuItems.add(menuItem);

        menuItem = new MenuItem();
        menuItem.id = 7;
        menuItem.name = getString(R.string.nav_settings);
        menuItems.add(menuItem);
        return menuItems;
    }

    public void openHomeFragment(){
        switchFragment(HomeFragment.newInstance());
    }
//    public void openMyCityFragment(){
//        switchFragment(MyCityFragment.newInstance());
//    }
    public void openDirectoryListFragment(){
        switchFragment(DirectoryListFragment.newInstance());
    }
    public void openDirectoryMapFragment(){
        switchFragment(DirectoryMapFragment.newInstance());
    }
    public void openNewsFragment(){ switchFragment(NewsFragment.newInstance()); }
    public void openCitizenIDFragment(){ switchFragment(CitizenRegistrationID.newInstance()); }
    public void openMomentFragment(){
        switchFragment(MomentFragment.newInstance());
    }
    public void openActionCentertFragment(){ switchFragment(MayorsActionFragment.newInstance()); }
    public void openLGUFragment(){ switchFragment(SilangLguFragment.newInstance()); }
//    public void openServicesFragment(){
//        switchFragment(ServicesFragment.newInstance());
//    }
    public void openSettingsFragment(){
        switchFragment(SettingsFragment.newInstance());
    }
//    public void openCitizenReportFragment(){ switchFragment(CitizenReportingFragment.newInstance()); }

    @Override
    public void onStart(){
        drawerLayout.closeDrawer(GravityCompat.START);
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showNavigationDrawer(){
        drawerLayout.openDrawer(Gravity.LEFT);
    }
    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    public void hideNavigationDrawer(){
        drawerLayout.closeDrawer(Gravity.LEFT);
    }

    private void disableNavigationViewScrollbars() {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }

    @Override
    public void onClick(View v) {
            switch (v.getId()){
                case R.id.menuIV:
                    showNavigationDrawer();
                    break;
            }
    }

    @Override
    public void onWidgetItemClickListener(MenuItem menuItem) {
        switch (menuItem.id){
            case 1:
                hideNavigationDrawer();
                openHomeFragment();
                break;
            case 2:
                hideNavigationDrawer();
                openMomentFragment();
                break;
            case 3:
                hideNavigationDrawer();
                openActionCentertFragment();
                break;
            case 4:
                hideNavigationDrawer();
                startJobActivty("job");
                break;
            case 5:
                hideNavigationDrawer();
                openLGUFragment();
                break;
            case 6:
                hideNavigationDrawer();
                openNewsFragment();
                break;
            case 7:
                hideNavigationDrawer();
                openSettingsFragment();
                break;

        }
    }
}

