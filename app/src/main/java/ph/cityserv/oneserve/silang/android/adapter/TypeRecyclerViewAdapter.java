package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.ReportTypeModel;


import butterknife.BindView;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseRecylerViewAdapter;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class TypeRecyclerViewAdapter extends BaseRecylerViewAdapter<TypeRecyclerViewAdapter.ViewHolder, ReportTypeModel> {

    private ClickListener clickListener;

    public TypeRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent,
                R.layout.adapter_report_type));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.categoryTXT.setText(holder.getItem().title);

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(
                R.id.adapterCON)          View adapterCON;
        @BindView(
                R.id.categoryTXT)         TextView categoryTXT;

        public ViewHolder(View view) {
            super(view);
            adapterCON.setOnClickListener(TypeRecyclerViewAdapter.this);
        }
        public ReportTypeModel getItem() {
            return (ReportTypeModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
                case
                        R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onCategoryClicked((ReportTypeModel) v.getTag());
                }
                break;
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onCategoryClicked(ReportTypeModel reportTypeModel);
    }
} 
