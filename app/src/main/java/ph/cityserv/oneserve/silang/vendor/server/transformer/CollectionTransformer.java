package ph.cityserv.oneserve.silang.vendor.server.transformer;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 8/3/2017.
 */

public class CollectionTransformer<T> extends BaseTransformer{

    @SerializedName("data")
    public List<T> data;
}
