package ph.cityserv.oneserve.silang.android.fragment.registration;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.RegistrationActivity;
import ph.cityserv.oneserve.silang.data.model.server.UserItem;
import ph.cityserv.oneserve.silang.server.request.Auth;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.ToastMessage;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;

public class ForgotPasswordFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = ForgotPasswordFragment.class.getName().toString();

    private RegistrationActivity registrationActivity;

    @BindView(R.id.requestEmailTV)      TextView requestEmailTV;
    @BindView(R.id.emailET)             EditText emailET;

    public static ForgotPasswordFragment newInstance() {
        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_forgot_password;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getContext();
        requestEmailTV.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.requestEmailTV:
                attemptRequestVerification();
                break;
        }
    }

    private void attemptRequestVerification(){
        Auth.getDefault(getContext()).forgotPassword(emailET.getText().toString());
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        registrationActivity.setTitle("Forgot Password");
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if (singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            registrationActivity.startRegistrationActivity(emailET.getText().toString(), "reset");
        }else {
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }
}
