package ph.cityserv.oneserve.silang.server.request;

import android.content.Context;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import ph.cityserv.oneserve.silang.config.Keys;
import ph.cityserv.oneserve.silang.config.Url;
import ph.cityserv.oneserve.silang.data.model.server.NotificationItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.request.APIResponse;
import ph.cityserv.oneserve.silang.vendor.server.request.BaseRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by BCTI 3 on 8/16/2017.
 */

public class Notification extends BaseRequest {
    public Notification(Context context) {
        super(context);
    }

    private String include = "date";
    public static Notification getDefault(Context context) {
        Notification fragment = new Notification(context);
        return fragment;
    }

    public void all() {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<NotificationItem>>(getContext()) {
            @Override
            public Call<CollectionTransformer<NotificationItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestAPI(Url.getAllNotification(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new NotificationResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<NotificationItem>> requestAPI(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);
    }

    public class NotificationResponse extends APIResponse<CollectionTransformer<NotificationItem>> {
        public NotificationResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
