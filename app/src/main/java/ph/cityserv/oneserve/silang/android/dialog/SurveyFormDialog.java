package ph.cityserv.oneserve.silang.android.dialog;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.adapter.SurveyListViewAdapter;
import ph.cityserv.oneserve.silang.data.model.app.SurveyItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseDialog;
import ph.cityserv.oneserve.silang.vendor.android.widget.ExpandableHeightListView;

public class SurveyFormDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = SurveyFormDialog.class.getName().toString();

	private String trackingID;

	private List<SurveyItem> speedServiceList;
	private List<SurveyItem> clarityList;
	private List<SurveyItem> overallList;
	private List<SurveyItem> qualitativeList;

	private String answer = "";

	@BindView(R.id.submitBTN)					TextView submitBTN;
	@BindView(R.id.titleBarTXT)					TextView titleBarTXT;
	@BindView(R.id.backBTN)						ImageView backBTN;
	@BindView(R.id.speedServiceEHLV)			ExpandableHeightListView speedServiceEHLV;
	@BindView(R.id.clarityEHLV)					ExpandableHeightListView clarityEHLV;
	@BindView(R.id.overallEHLV)					ExpandableHeightListView overallEHLV;
	@BindView(R.id.qualitativeEHLV)				ExpandableHeightListView qualitativeEHLV;

	public static SurveyFormDialog newInstance(String trackingID) {
		SurveyFormDialog fragment = new SurveyFormDialog();
		fragment.trackingID = trackingID;
		return fragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_survey_form;
	}

	@Override
	public void onViewReady() {
		titleBarTXT.setText("SURVEY FORM");
			submitBTN.setOnClickListener(this);
			backBTN.setOnClickListener(this);

		initClarity();
		initOverall();
		initQualitative();
		initSpeedService();
	}

	public String getAnswer(SurveyItem surveyItem){
		if(surveyItem.isEditText){
			answer += surveyItem.answer + "|";
		}else{
			if(surveyItem.strongly_agree){
				answer += "strongly_agree" + "|";
			}else if(surveyItem.somewhat_agree){
				answer += "somewhat_agree" + "|";
			}else if(surveyItem.strongly_disagree){
				answer += "strongly_disagree" + "|";
			}else if(surveyItem.somewhat_disagree){
				answer += "somewhat_disagree" + "|";
			}
		}
		return answer;
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	private void initSpeedService(){
		speedServiceList = new ArrayList<>();
		SurveyItem surveyItem = new SurveyItem();
		surveyItem.number = 1;
		surveyItem.question = "Transactions are completed within the timeframe provided for by the citizen's charter.";
		surveyItem.hasNote = true;
		surveyItem.isRadio = true;
		surveyItem.isEditText = false;
		surveyItem.note1 = "For simple transactions - 3 days ";
		surveyItem.note2 = "For complex transactions - 2 weeks";
		speedServiceList.add(surveyItem);

		surveyItem = new SurveyItem();
		surveyItem.number = 2;
		surveyItem.question = "The length of process can be improved";
		surveyItem.hasNote = false;
		surveyItem.isRadio = true;
		surveyItem.isEditText = false;
		speedServiceList.add(surveyItem);

		surveyItem = new SurveyItem();
		surveyItem.number = 3;
		surveyItem.question = "Follow-ups were necessary to produce the documents requested.";
		surveyItem.hasNote = false;
		surveyItem.isRadio = true;
		surveyItem.isEditText = false;
		speedServiceList.add(surveyItem);

		surveyItem = new SurveyItem();
		surveyItem.number = 4;
		surveyItem.question = "There is immediate and timely notification for the availability of the documents requested.";
		surveyItem.hasNote = false;
		surveyItem.isRadio = true;
		surveyItem.isEditText = false;
		speedServiceList.add(surveyItem);

		SurveyListViewAdapter surveyListViewAdapter = new SurveyListViewAdapter(getContext());
		surveyListViewAdapter.setNewData(speedServiceList);
		speedServiceEHLV.setAdapter(surveyListViewAdapter);
	}

	private void initClarity(){
		clarityList = new ArrayList<>();
		SurveyItem surveyItem = new SurveyItem();
		surveyItem.number = 5;
		surveyItem.question = "The flow/process of the transaction is comprehensive and easy to understand.";
		surveyItem.hasNote = false;
		surveyItem.isRadio = true;
		surveyItem.isEditText = false;
		clarityList.add(surveyItem);

		surveyItem = new SurveyItem();
		surveyItem.number = 6;
		surveyItem.question = "The process is client-friendly";
		surveyItem.hasNote = false;
		surveyItem.isRadio = true;
		surveyItem.isEditText = false;
		clarityList.add(surveyItem);

		surveyItem = new SurveyItem();
		surveyItem.number = 7;
		surveyItem.question = "Process guides are easily accessible for the clients.";
		surveyItem.hasNote = false;
		surveyItem.isRadio = true;
		surveyItem.isEditText = false;
		clarityList.add(surveyItem);

		SurveyListViewAdapter surveyListViewAdapter = new SurveyListViewAdapter(getContext());
		surveyListViewAdapter.setNewData(clarityList);
		clarityEHLV.setAdapter(surveyListViewAdapter);
	}

	private void initOverall(){
		overallList = new ArrayList<>();
		SurveyItem surveyItem = new SurveyItem();
		surveyItem.number = 8;
		surveyItem.question = "The documents are completely accurate.";
		surveyItem.hasNote = false;
		surveyItem.isRadio = true;
		surveyItem.isEditText = false;
		overallList.add(surveyItem);

		surveyItem = new SurveyItem();
		surveyItem.number = 9;
		surveyItem.question = "The City Assessor’s Office handles transactions efficiently.";
		surveyItem.hasNote = false;
		surveyItem.isRadio = true;
		surveyItem.isEditText = false;
		overallList.add(surveyItem);

		surveyItem = new SurveyItem();
		surveyItem.number = 10;
		surveyItem.question = "I am satisfied with how the City Assessor’s Office handled my transaction.";
		surveyItem.hasNote = false;
		surveyItem.isRadio = true;
		surveyItem.isEditText = false;
		overallList.add(surveyItem);
		SurveyListViewAdapter surveyListViewAdapter = new SurveyListViewAdapter(getContext());
		surveyListViewAdapter.setNewData(overallList);
		overallEHLV.setAdapter(surveyListViewAdapter);
	}

	private void initQualitative(){
		qualitativeList = new ArrayList<>();
		SurveyItem surveyItem = new SurveyItem();
		surveyItem.number = 11;
		surveyItem.question = "What were the specific difficulties you encountered during the transaction? How was it addressed by the agency concerned?";
		surveyItem.hasNote = false;
		surveyItem.isRadio = false;
		surveyItem.isEditText = true;
		qualitativeList.add(surveyItem);

		surveyItem = new SurveyItem();
		surveyItem.number = 12;
		surveyItem.question = "What are the specific innovations that you want the agency to adapt to better the service provision?";
		surveyItem.hasNote = false;
		surveyItem.isRadio = false;
		surveyItem.isEditText = true;
		qualitativeList.add(surveyItem);

		surveyItem = new SurveyItem();
		surveyItem.number = 13;
		surveyItem.question = "What are other important things that you want to be addressed to better improve the service delivery?";
		surveyItem.hasNote = false;
		surveyItem.isRadio = false;
		surveyItem.isEditText = true;
		qualitativeList.add(surveyItem);

		surveyItem = new SurveyItem();
		surveyItem.number = 14;
		surveyItem.question = "How do you feel about the entire process?";
		surveyItem.hasNote = false;
		surveyItem.isRadio = false;
		surveyItem.isEditText = true;
		qualitativeList.add(surveyItem);

		SurveyListViewAdapter surveyListViewAdapter = new SurveyListViewAdapter(getContext());
		surveyListViewAdapter.setNewData(qualitativeList);
		qualitativeEHLV.setAdapter(surveyListViewAdapter);
	}

	private void getAnswer(){
		for (SurveyItem surveyItem :  speedServiceList){
			answer += getAnswer(surveyItem);
		}
		for (SurveyItem surveyItem :  clarityList){
			answer += getAnswer(surveyItem);
		}
		for (SurveyItem surveyItem :  overallList){
			answer += getAnswer(surveyItem);
		}
		for (SurveyItem surveyItem :  qualitativeList){
			answer += getAnswer(surveyItem);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.submitBTN:
				getAnswer();
				dismiss();
				break;
			case R.id.backBTN:
				dismiss();
				break;
		}
	}
}
