package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.NewsItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseListViewAdapter;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseRecylerViewAdapter;
import ph.cityserv.oneserve.silang.vendor.android.java.StringFormatter;
import ph.cityserv.oneserve.silang.vendor.android.widget.ResizableImageView;

public class NewsRecyclerViewAdapter extends BaseRecylerViewAdapter<NewsRecyclerViewAdapter.ViewHolder, NewsItem> {

    private ClickListener clickListener;

    public NewsRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_news));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.adapterCON.setOnClickListener(this);

        holder.contentTXT.setText(StringFormatter.fromHtml(holder.getItem().excerpt));
        holder.dateTXT.setText(holder.getItem().date.data.time_passed);
        holder.nameTXT.setText(holder.getItem().title);
        Picasso.with(getContext())
                .load(holder.getItem().info.data.full_path)
                .placeholder(R.drawable.placeholder_silang)
                .error(R.drawable.placeholder_silang)
                .into(holder.imageIV);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.imageIV)             ResizableImageView imageIV;
        @BindView(R.id.contentTXT)          TextView contentTXT;
        @BindView(R.id.dateTXT)             TextView dateTXT;
        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.adapterCON)          View adapterCON;

        public ViewHolder(View view) {
            super(view);
        }

        public NewsItem getItem() {
            return (NewsItem) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
                case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onReadMoreClick(((NewsItem) v.getTag()));
                }
                break;
        }
    }
    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends BaseListViewAdapter.Listener {
        void onReadMoreClick(NewsItem newsItem);
    }
} 
