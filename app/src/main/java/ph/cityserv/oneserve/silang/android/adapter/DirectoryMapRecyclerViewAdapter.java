package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.DirectoryItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseListViewAdapter;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseRecylerViewAdapter;

public class DirectoryMapRecyclerViewAdapter extends BaseRecylerViewAdapter<DirectoryMapRecyclerViewAdapter.ViewHolder, DirectoryItem> {

    private ClickListener clickListener;

    public DirectoryMapRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_directory_map));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setOnClickListener(this);
        holder.getBaseView().setTag(holder.getItem());

        Picasso.with(getContext())
                .load(holder.getItem().icon.data.full_path)
                .fit()
                .into(holder.imageIV);

        holder.getBaseView().setSelected(holder.getItem().isSelected);
    }

    public void setSelected(int pos){
        for(DirectoryItem directory: getData()){
            directory.isSelected = false;
        }
        getData().get(pos).isSelected = true;
        notifyDataSetChanged();
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{


        @BindView(R.id.imageIV)         ImageView imageIV;

        public ViewHolder(View view) {
            super(view);
        }

        public DirectoryItem getItem() {
            return (DirectoryItem) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        for(DirectoryItem directory: getData()){
            directory.isSelected = false;
        }

        DirectoryItem directoryItem = (DirectoryItem) v.getTag();

        directoryItem.isSelected = true;

        notifyDataSetChanged();

        if(clickListener != null){
            clickListener.onItemClick(directoryItem);
        }
    }

    public void setonClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends BaseListViewAdapter.Listener {
        void onItemClick(DirectoryItem directoryItem);
    }
} 
