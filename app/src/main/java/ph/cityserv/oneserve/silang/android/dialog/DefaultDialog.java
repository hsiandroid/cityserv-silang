package ph.cityserv.oneserve.silang.android.dialog;

import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseDialog;

public class DefaultDialog extends BaseDialog {
	public static final String TAG = DefaultDialog.class.getName().toString();

	public static DefaultDialog newInstance() {
		DefaultDialog dialog = new DefaultDialog();
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_default;
	}

	@Override
	public void onViewReady() {

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}
}
