package ph.cityserv.oneserve.silang.vendor.android.java;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by BCTI 3 on 5/19/2017.
 */

public class TimestampManager {

    public static String formatToString(String timeStamp, String format) {
        try {
            SimpleDateFormat newFormat = new SimpleDateFormat(format);
            SimpleDateFormat currentFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date date = currentFormat.parse(timeStamp);
            return newFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "0000-00-00 00:00:00.000";
    }

    public static String format(String timeStamp, String format) {
        try {
            SimpleDateFormat newFormat = new SimpleDateFormat(format);
            SimpleDateFormat currentFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date date = currentFormat.parse(timeStamp);
            return newFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "0000-00-00 00:00:00.000";
    }

    public static Date formatStringToDate(String timeStamp) {
        try {
            SimpleDateFormat currentFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            return currentFormat.parse(timeStamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static String formatDateToString(Date date) {
        return formatDateToString(date, "yyyy-MM-dd HH:mm:ss.SSS");
    }

    public static String formatDateToString(Date date, String format) {
        SimpleDateFormat newFormat = new SimpleDateFormat(format);
        return newFormat.format(date);
    }

}
