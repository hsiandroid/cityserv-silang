package ph.cityserv.oneserve.silang.android.fragment.registration;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.RegistrationActivity;
import ph.cityserv.oneserve.silang.config.Keys;
import ph.cityserv.oneserve.silang.data.model.server.UserItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.server.request.Auth;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.ToastMessage;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;
import ph.cityserv.oneserve.silang.vendor.server.util.ErrorResponseManger;

public class SignUpFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = SignUpFragment.class.getName().toString();
    public RegistrationActivity registrationActivity;

    @BindView(R.id.firstNameET)         EditText firstNameET;
    @BindView(R.id.lastNameET)          EditText lastNameET;
    @BindView(R.id.contactET)           EditText contactET;
    @BindView(R.id.emailET)             EditText emailET;
    @BindView(R.id.passwordET)          EditText passwordET;
    @BindView(R.id.confirmPasswordET)   EditText confirmPasswordET;
    @BindView(R.id.signUpTV)            TextView signUpTV;

    public static SignUpFragment newInstance(){
        SignUpFragment fragment = new SignUpFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_signup;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getContext();
        registrationActivity.setTitle("Sign up");
        signUpTV.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.signUpTV:
                attemptSignUp();
                break;
        }
    }

    private void attemptSignUp(){
        Auth.getDefault(getContext()).signUp(
                emailET.getText().toString(),
                firstNameET.getText().toString(),
                lastNameET.getText().toString(),
                contactET.getText().toString(),
                passwordET.getText().toString(),
                confirmPasswordET.getText().toString()
                );
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.SignUpResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            UserData.insert(singleTransformer.data);
            UserData.insert(UserData.AUTHORIZATION, singleTransformer.token);
//            UserData.insert(UserData.FIRST_LOGIN, singleTransformer.first_login);
            registrationActivity.startMainActivity("main");
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
            if(singleTransformer.hasRequirements()){
                ErrorResponseManger.first(emailET, singleTransformer.errors.email);
                ErrorResponseManger.first(firstNameET, singleTransformer.errors.fname);
                ErrorResponseManger.first(lastNameET, singleTransformer.errors.lname);
                ErrorResponseManger.first(contactET, singleTransformer.errors.contact_number);
                ErrorResponseManger.first(passwordET, singleTransformer.errors.password);
            }
        }
    }
}
