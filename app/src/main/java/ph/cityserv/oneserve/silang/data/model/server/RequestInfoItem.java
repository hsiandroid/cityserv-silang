package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 6/25/2016.
 */
public class RequestInfoItem extends AndroidModel{

    @SerializedName("title")
    public String title;

    @SerializedName("code")
    public String code;

    @SerializedName("status")
    public String status;

    @SerializedName("duration")
    public String duration;

    @SerializedName("remarks")
    public String remarks;
}
