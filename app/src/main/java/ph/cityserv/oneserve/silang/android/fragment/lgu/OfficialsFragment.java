package ph.cityserv.oneserve.silang.android.fragment.lgu;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.MainActivity;
import ph.cityserv.oneserve.silang.android.adapter.SilangLguOfficialsAdapter;
import ph.cityserv.oneserve.silang.data.model.server.OfficialItem;
import ph.cityserv.oneserve.silang.server.request.Auth;
import ph.cityserv.oneserve.silang.server.request.LGU;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.EndlessRecyclerViewScrollListener;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.BaseTransformer;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;

public class OfficialsFragment extends BaseFragment implements SilangLguOfficialsAdapter.ClickListener,
        SwipeRefreshLayout.OnRefreshListener, EndlessRecyclerViewScrollListener.Callback{
    public static final String TAG = OfficialsFragment.class.getName().toString();


    public static OfficialsFragment newInstance() {
        OfficialsFragment fragment = new OfficialsFragment();
        return fragment;
    }

    @BindView(R.id.photosRV)                RecyclerView photosRV;
    @BindView(R.id.lguSRL)                  SwipeRefreshLayout lguSRL;

    private MainActivity mainActivity;
    private SilangLguOfficialsAdapter singleLGURVAdapter;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private APIRequest apiRequest;

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getContext();
        setUpNewsListView();
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_official;
    }

    private void setUpNewsListView(){
        lguSRL.setOnRefreshListener(this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        singleLGURVAdapter = new SilangLguOfficialsAdapter(getContext());
        singleLGURVAdapter.setClickListener(this);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        photosRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        photosRV.setLayoutManager(linearLayoutManager);
        photosRV.setAdapter(singleLGURVAdapter);
    }

    public void refreshList(){
        apiRequest = LGU.getDefault(getContext()).officials(lguSRL);
        apiRequest.first();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(LGU.OfficialResponse responseData){
        CollectionTransformer<OfficialItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            if(responseData.isNext()){
                singleLGURVAdapter.addNewData(collectionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                singleLGURVAdapter.setNewData(collectionTransformer.data);
            }
        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    @Override
    public void onItemClick(OfficialItem appointmentModel) {

    }
}
