package ph.cityserv.oneserve.silang.server.request;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import ph.cityserv.oneserve.silang.config.Keys;
import ph.cityserv.oneserve.silang.config.Url;
import ph.cityserv.oneserve.silang.data.model.server.MomentItem;
import ph.cityserv.oneserve.silang.data.model.server.OfficialItem;
import ph.cityserv.oneserve.silang.data.model.server.PhotosModel;
import ph.cityserv.oneserve.silang.data.model.server.RequestIDItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.request.APIResponse;
import ph.cityserv.oneserve.silang.vendor.server.request.BaseRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by BCTI 3 on 8/16/2017.
 */

public class LGU extends BaseRequest {
    public LGU(Context context) {
        super(context);
    }

    public static LGU getDefault(Context context) {
        LGU fragment = new LGU(context);
        return fragment;
    }

    public APIRequest officials(SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<OfficialItem>>(getContext()) {
            @Override
            public Call<CollectionTransformer<OfficialItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionAPI(Url.getOfficials(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new OfficialResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);
        return apiRequest;
    }

    public APIRequest photos(SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<PhotosModel>>(getContext()) {
            @Override
            public Call<CollectionTransformer<PhotosModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionPhoto(Url.getPhoto(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new PhotosResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);
        return apiRequest;
    }

    public APIRequest requestID(String fname, String lname, String mname, String gender, String nationality, String street_address, String brgy, String occupation, String weight, String height,
                                String birthdate, String contact_number, String email) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<RequestIDItem>>(getContext()) {
            @Override
            public Call<SingleTransformer<RequestIDItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingle(Url.getRequestID(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new IDResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.FNAME, fname)
                .addParameter(Keys.LNAME, lname)
                .addParameter(Keys.MNAME, mname)
                .addParameter(Keys.GENDER, gender)
                .addParameter(Keys.NATIONALITY, nationality)
                .addParameter(Keys.STREET_ADDRESS, street_address)
                .addParameter(Keys.BRGY, brgy)
                .addParameter(Keys.OCCUPATION, occupation)
                .addParameter(Keys.WEIGHT, weight)
                .addParameter(Keys.HEIGHT, height)
                .addParameter(Keys.BIRTHDATE, birthdate)
                .addParameter(Keys.CONTACT_NUMBER, contact_number)
                .addParameter(Keys.EMAIL, email)
                .execute();

        return apiRequest;
    }

    public APIRequest validateID() {
        APIRequest apiRequest = new APIRequest<SingleTransformer<RequestIDItem>>(getContext()) {
            @Override
            public Call<SingleTransformer<RequestIDItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingle(Url.getIDStatus(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new IDResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Validating...")
                .execute();

        return apiRequest;
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<OfficialItem>> requestCollectionAPI(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<PhotosModel>> requestCollectionPhoto(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<RequestIDItem>> requestSingle(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

    }

    public class OfficialResponse extends APIResponse<CollectionTransformer<OfficialItem>> {
        public OfficialResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class PhotosResponse extends APIResponse<CollectionTransformer<PhotosModel>> {
        public PhotosResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class IDResponse extends APIResponse<SingleTransformer<RequestIDItem>> {
        public IDResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
