package ph.cityserv.oneserve.silang.vendor.android.java;

import android.app.Activity;
import android.support.v4.app.ActivityCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import ph.cityserv.oneserve.silang.R;


/**
 * Created by BCTI 3 on 10/26/2016.
 */

public class ToastMessage {

    private static Toast customToast;

    public enum Status{
        SUCCESS,
        FAILED
    }

    public static void show(Activity activity, String message, Status status){
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_message, null);

        TextView messageTXT = layout.findViewById(R.id.messageTXT);
        messageTXT.setText(message);

        View background = layout.findViewById(R.id.backgroundCON);
        ImageView iconIV = layout.findViewById(R.id.iconIV);

        switch (status){
            case SUCCESS:
                background.setBackground(ActivityCompat.getDrawable(activity.getBaseContext(),R.drawable.bg_toast_success));
                iconIV.setImageDrawable(ActivityCompat.getDrawable(activity.getBaseContext(),R.drawable.icon_success));
                break;
            case FAILED:
                background.setBackground(ActivityCompat.getDrawable(activity.getBaseContext(),R.drawable.bg_toast_failed));
                iconIV.setImageDrawable(ActivityCompat.getDrawable(activity.getBaseContext(),R.drawable.icon_failed));
                break;
        }

        iconIV.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        if (customToast != null) {
            customToast.cancel();
            customToast = null;
        }

        customToast = new Toast(activity.getBaseContext());
        customToast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
        customToast.setDuration(Toast.LENGTH_LONG);
        customToast.setView(layout);
        customToast.show();
    }

    public static void cancel(){
        if (customToast != null) {
            customToast.cancel();
            customToast.getView().setVisibility(View.GONE);
            customToast = null;
        }
    }
}
