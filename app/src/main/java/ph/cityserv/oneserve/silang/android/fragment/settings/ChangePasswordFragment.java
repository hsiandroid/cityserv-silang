package ph.cityserv.oneserve.silang.android.fragment.settings;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.SettingsActivity;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.PasswordEditTextManager;

public class ChangePasswordFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = ChangePasswordFragment.class.getName().toString();

    private SettingsActivity settingsActivity;

    @BindView(R.id.oldPasswordET)               EditText oldPasswordET;
    @BindView(R.id.showOldPasswordBTN)          ImageView showOldPasswordBTN;
    @BindView(R.id.newPasswordET)               EditText newPasswordET;
    @BindView(R.id.showNewPasswordBTN)          ImageView showNewPasswordBTN;
    @BindView(R.id.confirmNewPasswordET)        EditText confirmNewPasswordET;
    @BindView(R.id.showConfirmNewPasswordBTN)   ImageView showConfirmNewPasswordBTN;
    @BindView(R.id.confirmBTN)                  View confirmBTN;
    @BindView(R.id.avatarCIV)                   ImageView avatarCIV;
    @BindView(R.id.userNameTXT)                 TextView userNameTXT;

    public static ChangePasswordFragment newInstance() {
        ChangePasswordFragment fragment = new ChangePasswordFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_change_password;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getActivity();
        settingsActivity.setTitle("Change Password");
        confirmBTN.setOnClickListener(this);

        userNameTXT.setText(UserData.getUserItem().info.data.name);

        Picasso.with(getContext())
                .load(UserData.getUserItem().avatar.data.fullPath)
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .into(avatarCIV);

        PasswordEditTextManager.addShowPassword(getContext(), oldPasswordET, showOldPasswordBTN);
        PasswordEditTextManager.addShowPassword(getContext(), newPasswordET, showNewPasswordBTN);
        PasswordEditTextManager.addShowPassword(getContext(), confirmNewPasswordET, showConfirmNewPasswordBTN);
    }

    private void attemptSignUp(){
//        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest(getContext());
//        changePasswordRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Updating Password...", false, false))
//                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Variable.server.key.INCLUDE, "info,social,statistics")
//                .addParameters(Variable.server.key.CURRENT_PASSWORD, oldPasswordET.getText().toString())
//                .addParameters(Variable.server.key.NEW_PASSWORD, newPasswordET.getText().toString())
//                .addParameters(Variable.server.key.NEW_PASSWORD_CONFIRMATION, confirmNewPasswordET.getText().toString())
//                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirmBTN:
                attemptSignUp();
                break;
        }
    }



//    @Subscribe
//    public void onResponse(ChangePasswordRequest.ServerResponse responseData) {
//        UserSingleTransformer userSingleTransformer = responseData.getData(UserSingleTransformer.class);
//        if(userSingleTransformer.status){
//            oldPasswordET.setText("");
//            newPasswordET.setText("");
//            confirmNewPasswordET.setText("");
//            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.SUCCESS);
//        }else{
//            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.FAILED);
//            if(userSingleTransformer.hasRequirements()){
//                ErrorResponseManger.first(oldPasswordET, userSingleTransformer.requires.current_password);
//                ErrorResponseManger.first(newPasswordET, userSingleTransformer.requires.new_password);
//                ErrorResponseManger.first(confirmNewPasswordET, userSingleTransformer.requires.new_password_confirmation);
//            }
//        }
//    }
}
