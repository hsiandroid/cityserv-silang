package ph.cityserv.oneserve.silang.android.fragment.main;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.azoft.carousellayoutmanager.CarouselLayoutManager;
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener;
import com.azoft.carousellayoutmanager.CenterScrollListener;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.MainActivity;
import ph.cityserv.oneserve.silang.android.adapter.HomeNewsRecyclerViewAdapter;
import ph.cityserv.oneserve.silang.android.adapter.WidgetListViewAdapter;
import ph.cityserv.oneserve.silang.android.adapter.ImageSliderCustomTextView;
import ph.cityserv.oneserve.silang.android.dialog.NewsDialog;
import ph.cityserv.oneserve.silang.data.model.server.NewsItem;
import ph.cityserv.oneserve.silang.data.model.server.WidgetItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.server.request.Article;
import ph.cityserv.oneserve.silang.server.request.Widget;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.widget.ExpandableHeightListView;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;

public class HomeFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener, HomeNewsRecyclerViewAdapter.ClickListener, View.OnClickListener{

    public static final String TAG = HomeFragment.class.getName().toString();

    private MainActivity mainActivity;
    private HomeNewsRecyclerViewAdapter homeNewsRecyclerViewAdapter;

    @BindView(R.id.homeSRL)                         SwipeRefreshLayout homeSRL;
    @BindView(R.id.newsEventsRV)                    RecyclerView newsRV;
    @BindView(R.id.momentsBTN)                      LinearLayout momentsBTN;
    @BindView(R.id.jobsBTN)                         LinearLayout jobsBTN;
    @BindView(R.id.newsEventBTN)                    LinearLayout newsEventBTN;
    @BindView(R.id.lguBTN)                          LinearLayout lguBTN;
    @BindView(R.id.mayorsBTN)                       LinearLayout mayorabTN;



    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_dashboard;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle("WELCOME, " + UserData.getUserItem().username);
        setUpListView();
        homeSRL.setOnRefreshListener(this);
        homeSRL.setBackgroundResource(R.color.colorPrimary);
        momentsBTN.setOnClickListener(this);
        jobsBTN.setOnClickListener(this);
        newsEventBTN.setOnClickListener(this);
        lguBTN.setOnClickListener(this);
        mayorabTN.setOnClickListener(this);
    }

    private void setUpListView(){
        final CarouselLayoutManager layoutManager = new CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL, true);
        layoutManager.setPostLayoutListener(new CarouselZoomPostLayoutListener());
        homeNewsRecyclerViewAdapter = new HomeNewsRecyclerViewAdapter(getContext());
        newsRV.setLayoutManager(layoutManager);
        newsRV.setHasFixedSize(true);
        newsRV.addOnScrollListener(new CenterScrollListener());
        newsRV.setAdapter(homeNewsRecyclerViewAdapter);
        homeNewsRecyclerViewAdapter.setClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void refreshList(){
        Article.getDefault(getContext()).recent(homeSRL);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Article.RecentNewsResponse responseData) {
        CollectionTransformer<NewsItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            if (responseData.isNext()){
                homeNewsRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else {
                homeNewsRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }

        }
    }

    @Override
    public void onItemClick(NewsItem newsItem) {
        NewsDialog.newInstance(newsItem).show(getFragmentManager(), TAG);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.momentsBTN:
                mainActivity.openMomentFragment();
                break;
            case R.id.jobsBTN:
                mainActivity.startJobActivty("job");
                break;
            case R.id.newsEventBTN:
                mainActivity.openNewsFragment();
                break;
            case R.id.lguBTN:
                mainActivity.openLGUFragment();
                break;
            case R.id.mayorsBTN:
                mainActivity.openActionCentertFragment();
                break;
        }
    }
}
