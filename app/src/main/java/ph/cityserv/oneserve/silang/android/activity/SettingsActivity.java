package ph.cityserv.oneserve.silang.android.activity;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.fragment.settings.AboutFragment;
import ph.cityserv.oneserve.silang.android.fragment.settings.ChangePasswordFragment;
import ph.cityserv.oneserve.silang.android.fragment.settings.EditProfileFragment;
import ph.cityserv.oneserve.silang.android.route.RouteActivity;


/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class SettingsActivity extends RouteActivity implements View.OnClickListener {
    public static final String TAG = SettingsActivity.class.getName().toString();

    @BindView(R.id.mainIconIV)       View mainIconIV;
    @BindView(R.id.mainTitleTXT)     TextView mainTitleTXT;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_settings;
    }

    @Override
    public void onViewReady() {
        mainIconIV.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "edit":
                openEditProfileFragment();
                break;
            case "password":
                openChangePasswordFragment();
                break;
            case "about":
                openAboutFragment();
                break;
        }
    }

    public void openChangePasswordFragment(){
        switchFragment(ChangePasswordFragment.newInstance());
    }

    public void openEditProfileFragment(){
        switchFragment(EditProfileFragment.newInstance());
    }

    public void openAboutFragment(){
        switchFragment(AboutFragment.newInstance());
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainIconIV:
                onBackPressed();
        }
    }

}
