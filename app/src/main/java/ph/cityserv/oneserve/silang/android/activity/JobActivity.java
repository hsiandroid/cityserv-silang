package ph.cityserv.oneserve.silang.android.activity;

import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.fragment.main.LeipoFragment;
import ph.cityserv.oneserve.silang.android.fragment.main.TourismFragment;
import ph.cityserv.oneserve.silang.android.fragment.main.YouthAffairsFragment;
import ph.cityserv.oneserve.silang.android.fragment.service.job.JobFragment;
import ph.cityserv.oneserve.silang.android.fragment.widget.ListFragment;
import ph.cityserv.oneserve.silang.android.fragment.widget.WebFragment;
import ph.cityserv.oneserve.silang.android.route.RouteActivity;
import ph.cityserv.oneserve.silang.data.model.server.WidgetItem;


/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class JobActivity extends RouteActivity implements View.OnClickListener {
    public static final String TAG = JobActivity.class.getName().toString();

    @BindView(R.id.mainIconIV)       View mainIconIV;
    @BindView(R.id.mainTitleTXT)     TextView mainTitleTXT;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_widget;
    }

    @Override
    public void onViewReady() {
        mainIconIV.setOnClickListener(this);
        setTitle("Job Openings");
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "job":
                openJobFragment();
                break;
            case "leipo":
                openLeipoFragment();
                break;
            case "tourism":
                openTourismFragment();
                break;
            case "youth":
                openYouthAffairsFragment();
                break;
        }
    }

    public void openJobFragment(){ switchFragment(JobFragment.newInstance()); }

    public void openLeipoFragment(){ switchFragment(LeipoFragment.newInstance()); }

    public void openTourismFragment(){ switchFragment(TourismFragment.newInstance()); }

    public void openYouthAffairsFragment(){ switchFragment(YouthAffairsFragment.newInstance()); }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainIconIV:
                onBackPressed();
        }
    }

}
