package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.data.model.server.MomentItem;
import ph.cityserv.oneserve.silang.data.model.server.OfficialItem;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseRecylerViewAdapter;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SilangLguOfficialsAdapter extends BaseRecylerViewAdapter<SilangLguOfficialsAdapter.ViewHolder, OfficialItem> {
    private ClickListener clickListener;

    public SilangLguOfficialsAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_silang_officials));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.adapterCON.setOnClickListener(this);
        holder.officialPositionTXT.setText(holder.getItem().position);
        holder.officialTXT.setText(holder.getItem().name);
        Picasso.with(getContext())
                .load(holder.getItem().fullPath)
                .centerCrop()
                .fit()
                .into(holder.photosIV);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.officialsTXT)                    TextView officialTXT;
        @BindView(R.id.officialPositionTXT)             TextView officialPositionTXT;
        @BindView(R.id.photosIV)                        ImageView photosIV;
        @BindView(R.id.adapterCON)                      View adapterCON;

        public ViewHolder(View view) {
            super(view);
        }

        public OfficialItem getItem() {
            return (OfficialItem) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((OfficialItem) v.getTag());
                }
                break;

        }
    }

    public interface ClickListener{
        void onItemClick(OfficialItem appointmentModel);
    }
} 
