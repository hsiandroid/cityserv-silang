package ph.cityserv.oneserve.silang.server.request;

import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 8/12/2017.
 */

public class PESOModel extends AndroidModel {

    @SerializedName("phone_number")
    public String phoneNumber;

    @SerializedName("contact_number")
    public String contactNumber;

    @SerializedName("email")
    public String email;

    @SerializedName("address")
    public String address;
}
