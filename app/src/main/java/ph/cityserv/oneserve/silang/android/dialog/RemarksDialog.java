package ph.cityserv.oneserve.silang.android.dialog;

import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.RequestInfoItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseDialog;

public class RemarksDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = RemarksDialog.class.getName().toString();

	private RequestInfoItem requestInfoItem;

	@BindView(R.id.imageIV)				ImageView imageIV;
	@BindView(R.id.titleTXT)			TextView titleTXT;
	@BindView(R.id.messageTXT)			TextView messageTXT;
	@BindView(R.id.confirmBTN)			TextView confirmBTN;

	public static RemarksDialog newInstance(RequestInfoItem requestInfoItem) {
		RemarksDialog fragment = new RemarksDialog();
		fragment.requestInfoItem = requestInfoItem;
		return fragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_remarks;
	}

	@Override
	public void onViewReady() {
		confirmBTN.setOnClickListener(this);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
	}

	public void show(){
		switch (requestInfoItem.status.toLowerCase()){
			case "done":
				imageIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_status_completed));
				break;
			case "pending":
				imageIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_status_pending));
				break;
			case "for_revalidation":
				imageIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_status_revalidate));
				break;
			case "rejected":
				imageIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_status_rejected));
				break;
		}

		requestInfoItem.remarks = requestInfoItem.remarks == null || requestInfoItem.remarks.equals("null") ? "" : requestInfoItem.remarks;
		titleTXT.setText(requestInfoItem.title);
		messageTXT.setText(requestInfoItem.remarks);
		this.show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.confirmBTN:
				dismiss();
				break;
		}
	}
}
