package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.WidgetItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseListViewAdapter;

public class WidgetListViewAdapter extends BaseListViewAdapter<WidgetListViewAdapter.ViewHolder, WidgetItem> {

    private ClickListener clickListener;

    public WidgetListViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_home));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setOnClickListener(this);
        holder.homeTXT.setText(holder.getItem().title);
        Picasso.with(getContext())
                .load(holder.getItem().image.full_path)
                .into(holder.homeIV);
    }

    public class ViewHolder extends BaseListViewAdapter.ViewHolder{

        @BindView(R.id.homeTXT)   TextView homeTXT;
        @BindView(R.id.homeIV)    ImageView homeIV;

        public ViewHolder(View view) {
            super(view);
        }

        public WidgetItem getItem() {
            return (WidgetItem) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick(((ViewHolder) v.getTag()).getItem());
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends Listener{
        void onItemClick(WidgetItem widgetItem);
    }
} 
