package ph.cityserv.oneserve.silang.android.adapter;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.StrictMode;
import android.support.v7.widget.PopupMenu;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.dialog.MomentDialog;
import ph.cityserv.oneserve.silang.android.dialog.MomentEditDialog;
import ph.cityserv.oneserve.silang.data.model.server.MomentItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseActivity;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseListViewAdapter;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseRecylerViewAdapter;
import ph.cityserv.oneserve.silang.vendor.android.java.ImageManager;

public class MomentRecyclerViewAdapter extends BaseRecylerViewAdapter<MomentRecyclerViewAdapter.ViewHolder, MomentItem> {

    private ClickListener clickListener;

    public MomentRecyclerViewAdapter(Context context) {
        super(context);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_moment));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setTag(holder.getItem());
        holder.getBaseView().setOnClickListener(this);

        Picasso.with(getContext())
                .load(holder.getItem().info.data.full_path)
                .placeholder(R.drawable.placeholder_logo)
                .error(R.drawable.placeholder_logo)
                .fit()
                .into(holder.imageIV);

        if(holder.getItem().info.data.content.equals("")){
            holder.getItem().info.data.content = "My Iloilo Moment.";
        }
        holder.contentTXT.setText(Html.fromHtml(holder.getItem().info.data.content));
        holder.dateTXT.setText(holder.getItem().date.data.time_passed);
        holder.imageIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPage(getItem(position));
            }
        });

        holder.optionBTN.setTag(position);
        holder.optionBTN.setOnClickListener(this);
        holder.adapterCON.setTag(holder.getItem());
        holder.adapterCON.setOnClickListener(this);
    }

    private void otherOptionClick(View view) {
        final MomentItem momentItem = getItem((int) view.getTag());

        PopupMenu popup = new PopupMenu(getContext(), view);
        popup.getMenuInflater().inflate(R.menu.pop_menu_moment, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.message_menu_remove:
                        if(clickListener != null){
                            clickListener.onItemDeleteClick(momentItem);
                        }
                        break;
                    case R.id.message_menu_view:
                        openPage(momentItem);
                        break;
                    case R.id.message_menu_share:
                        shareImage(momentItem.info.data.full_path);
                        break;
//                    case R.id.message_menu_edit:
//                        MomentEditDialog.newInstance(momentItem).show(((BaseActivity) getContext()).getSupportFragmentManager(),MomentEditDialog.TAG);
//                        break;
                }
                return false;
            }
        });
        popup.show();
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.imageIV)             ImageView imageIV;
        @BindView(R.id.contentTXT)          TextView contentTXT;
        @BindView(R.id.readMoreBTN)         TextView readMoreBTN;
        @BindView(R.id.dateTXT)             TextView dateTXT;
        @BindView(R.id.optionBTN)           View optionBTN;
        @BindView(R.id.adapterCON)          View adapterCON;

        public ViewHolder(View view) {
            super(view);
        }

        public MomentItem getItem() {
            return (MomentItem) super.getItem();
        }
    }

    private void openPage(MomentItem momentItem){
        MomentDialog.newInstance(momentItem).show(((BaseActivity) getContext()).getSupportFragmentManager(), MomentDialog.TAG);
    }

    public void shareImage(String image){
        final ProgressDialog progressDialog = ProgressDialog.show(getContext(), "", "Preparing to share.", true, false);
        Picasso.with(getContext())
                .load(image)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        ImageManager.getFileFromBitmap(getContext(),bitmap, "temp", new ImageManager.Callback() {
                            @Override
                            public void success(File file) {
                                if(file != null){
                                    progressDialog.cancel();
                                    Uri imageUri = Uri.fromFile(file);
                                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                    sharingIntent.setType("image/*");
                                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "My IloIlo Moment");
                                    sharingIntent.putExtra(Intent.EXTRA_TEXT, "My IloIlo Moment");
                                    sharingIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
                                    getContext().startActivity(Intent.createChooser(sharingIntent, "Where would you like to share this photo?"));
                                }
                            }
                        });
                    }
                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        progressDialog.cancel();
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        progressDialog.cancel();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.optionBTN:
                otherOptionClick(v);
                break;
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((MomentItem) v.getTag());
                }
                break;
           
        }
    }

    public void setonItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends BaseListViewAdapter.Listener {
        void onItemClick(MomentItem momentItem);
        void onItemDeleteClick(MomentItem momentItem);
    }
} 
