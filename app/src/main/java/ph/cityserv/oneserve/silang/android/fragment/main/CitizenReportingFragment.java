package ph.cityserv.oneserve.silang.android.fragment.main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import icepick.State;
import io.realm.Realm;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.MainActivity;
import ph.cityserv.oneserve.silang.android.adapter.CitizenReportRecyclerViewAdapter;
import ph.cityserv.oneserve.silang.android.adapter.MomentRecyclerViewAdapter;
import ph.cityserv.oneserve.silang.android.dialog.CitizenReportDialog;
import ph.cityserv.oneserve.silang.android.dialog.SendReportDialog;
import ph.cityserv.oneserve.silang.data.model.db.SampleRealModel;
import ph.cityserv.oneserve.silang.data.model.server.CitizenReportItem;
import ph.cityserv.oneserve.silang.data.model.server.MomentItem;
import ph.cityserv.oneserve.silang.server.request.Auth;
import ph.cityserv.oneserve.silang.server.request.CitizenReport;
import ph.cityserv.oneserve.silang.server.request.Moment;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.EndlessRecyclerViewScrollListener;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.BaseTransformer;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;

import static android.app.Activity.RESULT_OK;

public class CitizenReportingFragment extends BaseFragment implements View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener, EndlessRecyclerViewScrollListener.Callback,
        CitizenReportRecyclerViewAdapter.ClickListener, SendReportDialog.Callback {
    public static final String TAG = CitizenReportingFragment.class.getName().toString();

    private MainActivity mainActivity;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private APIRequest apiRequest;
    private CitizenReportRecyclerViewAdapter citizenReportRecyclerViewAdapter;

    @BindView(R.id.placeholderReport)           ImageView placeholderReport;
    @BindView(R.id.reportSRL)                   SwipeRefreshLayout reportSRL;
    @BindView(R.id.reportRV)                    RecyclerView reportRV;
    @BindView(R.id.reportFAB)                   FloatingActionButton reportFAB;


    public static CitizenReportingFragment newInstance() {
        CitizenReportingFragment fragment = new CitizenReportingFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_reporting;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity)getContext();
        mainActivity.setTitle(getString(R.string.title_citizen_reporting));
        reportFAB.setOnClickListener(this);
        seUpReportListView();
    }

    private void seUpReportListView(){
        reportSRL.setOnRefreshListener(this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        citizenReportRecyclerViewAdapter = new CitizenReportRecyclerViewAdapter(getContext());
        citizenReportRecyclerViewAdapter.setonItemClickListener(this);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        reportRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        reportRV.setAdapter(citizenReportRecyclerViewAdapter);
        reportRV.setLayoutManager(linearLayoutManager);
    }

    private void showHidePlaceholder(){
        placeholderReport.setVisibility(citizenReportRecyclerViewAdapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
        reportRV.setVisibility(citizenReportRecyclerViewAdapter.getItemCount() > 0 ? View.VISIBLE : View.GONE);
    }

    public void refreshList(){
        apiRequest =  CitizenReport.getDefault(getContext()).myReport(reportSRL);
        apiRequest.first();
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Subscribe
    public void onResponse(CitizenReport.MyReportResponse responseData){
        CollectionTransformer<CitizenReportItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            if(responseData.isNext()){
                citizenReportRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                citizenReportRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }
            showHidePlaceholder();

        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.reportFAB:
                SendReportDialog.newInstance(this).show(getFragmentManager(), TAG);
                break;
        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    @Override
    public void onItemClick(CitizenReportItem citizenReportItem) {
        CitizenReportDialog.newInstance(citizenReportItem).show(getFragmentManager(), TAG);
    }

    @Override
    public void onSuccess() {
        refreshList();
    }
}
