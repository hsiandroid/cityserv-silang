package ph.cityserv.oneserve.silang.android.fragment.service.job;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.JobActivity;
import ph.cityserv.oneserve.silang.android.activity.JobActivity;
import ph.cityserv.oneserve.silang.android.adapter.JobRecyclerViewAdapter;
import ph.cityserv.oneserve.silang.android.adapter.NewsRecyclerViewAdapter;
import ph.cityserv.oneserve.silang.android.dialog.ApplyJobDialog;
import ph.cityserv.oneserve.silang.android.dialog.JobDialog;
import ph.cityserv.oneserve.silang.android.dialog.NewsDialog;
import ph.cityserv.oneserve.silang.android.dialog.PostJobDialog;
import ph.cityserv.oneserve.silang.data.model.server.JobItem;
import ph.cityserv.oneserve.silang.data.model.server.NewsItem;
import ph.cityserv.oneserve.silang.data.model.server.SampleModel;
import ph.cityserv.oneserve.silang.server.request.Article;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.DividerItemDecoration;
import ph.cityserv.oneserve.silang.vendor.android.java.EndlessRecyclerViewScrollListener;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;

public class JobFragment extends BaseFragment implements JobRecyclerViewAdapter.ClickListener,
        SwipeRefreshLayout.OnRefreshListener, ApplyJobDialog.Callback,
        EndlessRecyclerViewScrollListener.Callback, View.OnClickListener {
    public static final String TAG = JobFragment.class.getName().toString();

    private JobRecyclerViewAdapter jobRecyclerViewAdapter;
    private JobActivity  jobActivity;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private APIRequest apiRequest;

    @BindView(R.id.jobSRL)                 SwipeRefreshLayout jobSRL;
    @BindView(R.id.jobRV)                  RecyclerView jobRV;
    @BindView(R.id.jobPlaceholderCON)      View jobPlaceholderCON;
    @BindView(R.id.postBTN)                View postBTN;

    public static JobFragment newInstance() {
        JobFragment fragment = new JobFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_jobs;
    }

    @Override
    public void onViewReady() {
        jobActivity = (JobActivity) getActivity();
        postBTN.setOnClickListener(this);
        setUpNewsListView();
    }

    private void setUpNewsListView(){
        jobSRL.setOnRefreshListener(this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        jobRecyclerViewAdapter = new JobRecyclerViewAdapter(getContext());
        jobRecyclerViewAdapter.setClickListener(this);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        jobRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        jobRV.setLayoutManager(linearLayoutManager);
        jobRV.setAdapter(jobRecyclerViewAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    public void refreshList(){
        apiRequest = Article.getDefault(getContext()).jobList(jobSRL);
        apiRequest.first();
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    @Subscribe
    public void onResponse(Article.AllJobResponse responseData){
        CollectionTransformer<JobItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            if(responseData.isNext()){
                jobRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                jobRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }
        }
        if (jobRecyclerViewAdapter.getItemCount() == 0){
            jobRV.setVisibility(View.GONE);
            jobPlaceholderCON.setVisibility(View.VISIBLE);
        }else {
            jobRV.setVisibility(View.VISIBLE);
            jobPlaceholderCON.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.postBTN:
                PostJobDialog.newInstance().show(getFragmentManager(), TAG);
                break;
        }
    }

    @Override
    public void onSuccess() {
        refreshList();
    }

    @Override
    public void onClick(JobItem newsItem) {
        JobDialog.newInstance(newsItem).show(getChildFragmentManager(),NewsDialog.TAG);
    }

    @Override
    public void onApplyClick(JobItem newsItem) {
        ApplyJobDialog.newInstance(this, newsItem.id).show(getFragmentManager(), TAG);
    }
}
