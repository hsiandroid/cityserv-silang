package ph.cityserv.oneserve.silang.android.activity;

import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.fragment.main.CitizenCardFragment;
import ph.cityserv.oneserve.silang.android.fragment.service.RequirementFragment;
import ph.cityserv.oneserve.silang.android.fragment.service.assessor.AssessorFormFragment;
import ph.cityserv.oneserve.silang.android.fragment.service.assessor.AssessorMainFragment;
import ph.cityserv.oneserve.silang.android.fragment.service.assessor.RequestStatusFragment;
import ph.cityserv.oneserve.silang.android.route.RouteActivity;
import ph.cityserv.oneserve.silang.data.model.server.RequestItem;
import ph.cityserv.oneserve.silang.data.model.server.ServiceItem;
import ph.cityserv.oneserve.silang.data.model.server.SubServiceItem;


/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class AssessorActivity extends RouteActivity implements View.OnClickListener {
    public static final String TAG = AssessorActivity.class.getName().toString();

    @BindView(R.id.mainIconIV)       View mainIconIV;
    @BindView(R.id.mainTitleTXT)     TextView mainTitleTXT;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_assessor;
    }

    @Override
    public void onViewReady() {
        mainIconIV.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "assessor":
                openAssessorMainFragment(new Gson().fromJson(getFragmentBundle().getString("service_item"),ServiceItem.class));
                break;
            case "requirements":
                openRequirementFragment(new Gson().fromJson(getFragmentBundle().getString("service_item"),ServiceItem.class));
                break;
            case "requirements-subservice":
                openRequirementFragment(new Gson().fromJson(getFragmentBundle().getString("subservice_item"),SubServiceItem.class));
                break;
            case "form":
                openAssessorFormFragment();
                break;
            case "status":
                openRequirementStatusFragment(new Gson().fromJson(getFragmentBundle().getString("request_item"), RequestItem.class));
                break;
            case "citizen_card":
                openCitizenCardFragment();
                break;
        }
    }

    public void openRequirementFragment(ServiceItem serviceItem){
        switchFragment(RequirementFragment.newInstance(serviceItem));
    }

    public void openRequirementFragment(SubServiceItem subServiceItem){
        switchFragment(RequirementFragment.newInstance(subServiceItem));
    }

    public void openAssessorMainFragment(ServiceItem serviceItem){
        switchFragment(AssessorMainFragment.newInstance(serviceItem));
    }

    public void openAssessorFormFragment(){
        switchFragment(AssessorFormFragment.newInstance());
    }

    public void openRequirementStatusFragment(RequestItem requestItem){
        switchFragment(RequestStatusFragment.newInstance(requestItem));
    }

    public void openCitizenCardFragment(){
        switchFragment(CitizenCardFragment.newInstance());
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainIconIV:
                onBackPressed();
        }
    }

}
