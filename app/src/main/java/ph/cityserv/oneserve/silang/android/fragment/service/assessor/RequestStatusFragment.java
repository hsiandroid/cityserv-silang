package ph.cityserv.oneserve.silang.android.fragment.service.assessor;

import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.AssessorActivity;
import ph.cityserv.oneserve.silang.android.adapter.RequestInfoListViewAdapter;
import ph.cityserv.oneserve.silang.android.dialog.RemarksDialog;
import ph.cityserv.oneserve.silang.android.dialog.SurveyFormDialog;
import ph.cityserv.oneserve.silang.data.model.server.RequestInfoItem;
import ph.cityserv.oneserve.silang.data.model.server.RequestItem;
import ph.cityserv.oneserve.silang.server.request.CitizenRequest;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.StringManager;
import ph.cityserv.oneserve.silang.vendor.android.widget.ExpandableHeightListView;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;

public class RequestStatusFragment extends BaseFragment implements
        RequestInfoListViewAdapter.ClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        View.OnClickListener{

    public static final String TAG = RequestStatusFragment.class.getName().toString();

    private AssessorActivity assessorActivity;

    private RequestItem requestItem;
    private RequestInfoListViewAdapter requestStatusAdapter;

    @BindView(R.id.statusSRL)               SwipeRefreshLayout statusSRL;
    @BindView(R.id.titleTXT)                TextView titleTXT;
    @BindView(R.id.codeLBL)                 TextView codeLBL;
    @BindView(R.id.codeTXT)                 TextView codeTXT;
    @BindView(R.id.durationTXT)             TextView durationTXT;
    @BindView(R.id.taskTXT)                 TextView taskTXT;
    @BindView(R.id.tastkStatusTXT)          TextView tastkStatusTXT;
    @BindView(R.id.statusEHLV)              ExpandableHeightListView statusEHLV;
    @BindView(R.id.statusTXT)               TextView statusTXT;
    @BindView(R.id.dateTXT)                 TextView dateTXT;
    @BindView(R.id.sendBTN)                 TextView sendBTN;

    public static RequestStatusFragment newInstance(RequestItem requestItem) {
        RequestStatusFragment fragment = new RequestStatusFragment();
        fragment.requestItem = requestItem;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_request_status;
    }

    @Override
    public void onViewReady() {
        assessorActivity = (AssessorActivity) getActivity();
        assessorActivity.setTitle(getString(R.string.title_request_status));
        sendBTN.setOnClickListener(this);
        setUpListView();
        displayData(requestItem);
    }

    private void setUpListView(){
        statusSRL.setOnRefreshListener(this);
        requestStatusAdapter = new RequestInfoListViewAdapter(getContext());
        statusEHLV.setAdapter(requestStatusAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    public void showSurvey(boolean b){
        sendBTN.setVisibility(b ? View.VISIBLE : View.GONE);
    }

    public void displayData(RequestItem requestItem){
        if (requestItem != null){
            titleTXT.setText(requestItem.title);
            codeTXT.setText(requestItem.tracking_number);
            durationTXT.setText(requestItem.duration);
            statusTXT.setText(StringManager.textCapWord(requestItem.status));
            dateTXT.setText(requestItem.date.data.time_passed);
            requestStatusAdapter.setNewData(requestItem.info.data.more.data);
            requestStatusAdapter.setClickListener(this);

            Log.e("Count", ">>>" + requestItem.info.data.more.data.size());
            if(requestItem.status.equalsIgnoreCase("done")){
                showSurvey(!requestItem.survey.data.survey_exist);
            }else{
                showSurvey(false);
            }
        }
    }

    @Override
    public void onItemClick(RequestInfoItem requestInfoItem) {
        RemarksDialog.newInstance(requestInfoItem).show(getChildFragmentManager(), RemarksDialog.TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sendBTN:
                SurveyFormDialog.newInstance(requestItem.tracking_number).show(getChildFragmentManager(), SurveyFormDialog.TAG);
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Subscribe
    public void onResponse(CitizenRequest.TrackRequestResponse responseData){
        SingleTransformer<RequestItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            requestItem = singleTransformer.data;
            displayData(requestItem);
        }
    }

    public void refreshList(){
        CitizenRequest.getDefault(getContext()).show(statusSRL, requestItem.id);
    }
}
