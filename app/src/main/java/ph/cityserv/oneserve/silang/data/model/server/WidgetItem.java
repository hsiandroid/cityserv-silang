package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 6/25/2016.
 */
public class WidgetItem extends AndroidModel{

    @SerializedName("title")
    public String title;

    @SerializedName("code")
    public String code;

    @SerializedName("status")
    public String status;

    @SerializedName("parent_id")
    public int parent_id;

    @SerializedName("is_parent")
    public boolean is_parent;

    @SerializedName("type")
    public String type;

    @SerializedName("display_screen")
    public String display_screen;

    @SerializedName("short_description")
    public String short_description;

    @SerializedName("content")
    public String content;

    @SerializedName("list_type")
    public String list_type;

    @SerializedName("display_type")
    public String display_type;

    @SerializedName("image")
    public Image image;

    @SerializedName("date")
    public Date date;

    @SerializedName("children")
    public Children children;

    public class Date{

        @SerializedName("data")
        public DateItem data;
    }

    public class Image{

        @SerializedName("path")
        public String path;

        @SerializedName("filename")
        public String filename;

        @SerializedName("directory")
        public String directory;

        @SerializedName("full_path")
        public String full_path;

        @SerializedName("thumb_path")
        public String thumb_path;
    }

    public class Children{

        @SerializedName("data")
        public List<WidgetItem> data;
    }

}
