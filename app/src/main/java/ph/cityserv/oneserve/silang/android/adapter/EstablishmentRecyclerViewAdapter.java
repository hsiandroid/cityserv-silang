package ph.cityserv.oneserve.silang.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.EstablishmentItem;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseListViewAdapter;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseRecylerViewAdapter;

public class EstablishmentRecyclerViewAdapter extends BaseRecylerViewAdapter<EstablishmentRecyclerViewAdapter.ViewHolder, EstablishmentItem> {

    private ClickListener clickListener;

    public EstablishmentRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_establishment));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setOnClickListener(this);

        holder.cityTXT.setText(holder.getItem().name);
        holder.addressTXT.setText(holder.getItem().address);
        holder.contactTXT.setText(holder.getItem().contact);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.cityTXT)           TextView cityTXT;
        @BindView(R.id.addressTXT)        TextView addressTXT;
        @BindView(R.id.contactTXT)        TextView contactTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public EstablishmentItem getItem() {
            return (EstablishmentItem) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick(((ViewHolder) v.getTag()).getItem());
        }
    }

    public void setonItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends BaseListViewAdapter.Listener {
        void onItemClick(EstablishmentItem establishmentItem);
    }
} 
