package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 8/12/2017.
 */

public class CitizenReportItem extends AndroidModel {


    @SerializedName("type")
    public String type;

    @SerializedName("excerpt")
    public String excerpt;

    @SerializedName("info")
    public Info info;

    @SerializedName("date")
    public Date date;

    @SerializedName("user")
    public User user;

    public static class Info {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("sender")
            public String sender;

            @SerializedName("status")
            public String status;

            @SerializedName("code")
            public String code;

            @SerializedName("content")
            public String content;

            @SerializedName("geolocation")
            public Geolocation geolocation;

            @SerializedName("path")
            public Object path;

            @SerializedName("directory")
            public String directory;

            @SerializedName("full_path")
            public String fullPath;

            @SerializedName("thumb_path")
            public String thumbPath;

            public static class Geolocation {
                @SerializedName("lat")
                public String lat;

                @SerializedName("long")
                public String longX;
            }
        }
    }

    public static class Date {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("date_db")
            public String dateDb;

            @SerializedName("month_year")
            public String monthYear;

            @SerializedName("time_passed")
            public String timePassed;

            @SerializedName("timestamp")
            public Timestamp timestamp;

            public static class Timestamp {
                @SerializedName("date")
                public String date;

                @SerializedName("timezone_type")
                public int timezoneType;

                @SerializedName("timezone")
                public String timezone;
            }
        }
    }

    public static class User {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("id")
            public int idX;

            @SerializedName("username")
            public String username;

            @SerializedName("email")
            public String email;

            @SerializedName("type")
            public String type;

            @SerializedName("fb_id")
            public Object fbId;

            @SerializedName("is_verify")
            public boolean isVerify;
        }
    }
}
