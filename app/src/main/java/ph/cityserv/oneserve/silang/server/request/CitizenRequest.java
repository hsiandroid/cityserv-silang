package ph.cityserv.oneserve.silang.server.request;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import ph.cityserv.oneserve.silang.config.Keys;
import ph.cityserv.oneserve.silang.config.Url;
import ph.cityserv.oneserve.silang.data.model.server.RequestItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.request.APIResponse;
import ph.cityserv.oneserve.silang.vendor.server.request.BaseRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by BCTI 3 on 8/16/2017.
 */

public class CitizenRequest extends BaseRequest {
    public CitizenRequest(Context context) {
        super(context);
    }

    private String include = "info.more,date,logs,survey";

    public static CitizenRequest getDefault(Context context) {
        CitizenRequest fragment = new CitizenRequest(context);
        return fragment;
    }

    public void create(String tracking) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<RequestItem>>(getContext()) {
            @Override
            public Call<SingleTransformer<RequestItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleAPI(Url.getCreateCitizenRequest(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new TrackRequestResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addParameter(Keys.TRACKING_ID, tracking)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Tracking request...")
                .execute();
    }

    public void show(SwipeRefreshLayout swipeRefreshLayout, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<RequestItem>>(getContext()) {
            @Override
            public Call<SingleTransformer<RequestItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleAPI(Url.getShowCitizenRequest(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new TrackRequestResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addParameter(Keys.REQUEST_ID, id)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .execute();
    }

    public void myRequest() {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<RequestItem>>(getContext()) {
            @Override
            public Call<CollectionTransformer<RequestItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionAPI(Url.getMyRequestCitizenRequest(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new MyRequestResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<RequestItem>> requestSingleAPI(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<RequestItem>> requestCollectionAPI(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);
    }

    public class TrackRequestResponse extends APIResponse<SingleTransformer<RequestItem>> {
        public TrackRequestResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class MyRequestResponse extends APIResponse<CollectionTransformer<RequestItem>> {
        public MyRequestResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
