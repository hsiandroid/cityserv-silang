package ph.cityserv.oneserve.silang.android.fragment.mycity;

import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.HistoryActivity;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;

public class CityServHistoryFragment extends BaseFragment {

    public static final String TAG = CityServHistoryFragment.class.getName().toString();

    private HistoryActivity historyActivity;

    @BindView(R.id.cityHistoryWV)       WebView cityHistoryWV;

    public static CityServHistoryFragment newInstance() {
        CityServHistoryFragment fragment = new CityServHistoryFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_city_history;
    }

    @Override
    public void onViewReady() {
        historyActivity = (HistoryActivity) getActivity();
        historyActivity.setTitle(getString(R.string.title_city_history));

        init();
        sampleData();
    }

    private void init(){
        cityHistoryWV.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }

    private void sampleData(){
        cityHistoryWV.loadUrl("http://www.facebook.com");
    }
}
