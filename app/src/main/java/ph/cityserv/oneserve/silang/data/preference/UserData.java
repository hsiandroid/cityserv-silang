package ph.cityserv.oneserve.silang.data.preference;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import ph.cityserv.oneserve.silang.data.model.server.UserItem;

/**
 * Created by BCTI 3 on 8/14/2017.
 */

public class UserData extends Data{

    public static final String USER_ITEM = "user_item";
    public static final String AUTHORIZATION = "authorization";
    public static final String FIRST_LOGIN = "first_login";
    public static final String UNIT_ITEMS = "unit_items";

    public static boolean isLogin(){
        return getUserItem().id != 0;
    }

    public static boolean isMe(int id){
        return getUserItem().id == id;
    }

    public static int getUserId(){
        return getUserItem().id;
    }

    public static void insert(UserItem userItem){
        SharedPreferences.Editor editor = Data.getSharedPreferences().edit();
        editor.putString(USER_ITEM, new Gson().toJson(userItem));
        editor.commit();
    }

    public static UserItem getUserItem(){
        UserItem userItem = new Gson().fromJson(Data.getSharedPreferences().getString(USER_ITEM, ""), UserItem.class);
        if(userItem == null){
            userItem = new UserItem();
        }
        return userItem;
    }
}
