package ph.cityserv.oneserve.silang.android.fragment.landing;

import android.os.Handler;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.LandingActivity;
import ph.cityserv.oneserve.silang.data.model.server.UserItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.server.request.Auth;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;

public class SplashFragment extends BaseFragment {

    public static final String TAG = SplashFragment.class.getName().toString();

    private LandingActivity landingActivity;
    private Handler handler;
    private Runnable runnable;

    public static SplashFragment newInstance() {
        SplashFragment fragment = new SplashFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_splash;
    }

    @Override
    public void onViewReady() {
        landingActivity = (LandingActivity) getContext();
//        attempt();
    }

    private void attemptGetSettings(){
//        AppSettingsRequest refreshTokenRequest = new AppSettingsRequest(getContext());
//        refreshTokenRequest.execute();
    }

    private void attempt(){

        runnable = new Runnable(){
            @Override
            public void run() {
                landingActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(UserData.isLogin()){
                            landingActivity.startMainActivity("main");
                        }else{
                            landingActivity.openLoginFragment();
                        }
                    }
                });
            }
        };

        handler = new Handler();
        handler.postDelayed(runnable, 3000);

    }

    @Override
    public void onResume() {
        super.onResume();
        attemptRefreshToken();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
    private void attemptRefreshToken(){
        Auth.getDefault(getContext()).refreshToken();
    }

    @Subscribe
    public void onResponse(Auth.RefreshTokenResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            UserData.insert(singleTransformer.data);
            UserData.insert(UserData.AUTHORIZATION, singleTransformer.new_token);
            landingActivity.startMainActivity("main");
        }else{
            landingActivity.openLoginFragment();
        }
    }

}
