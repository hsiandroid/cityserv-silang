package ph.cityserv.oneserve.silang.server.request;

import android.content.Context;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import ph.cityserv.oneserve.silang.config.Keys;
import ph.cityserv.oneserve.silang.config.Url;
import ph.cityserv.oneserve.silang.data.model.server.SampleModel;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.request.APIResponse;
import ph.cityserv.oneserve.silang.vendor.server.request.BaseRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by BCTI 3 on 8/16/2017.
 */

public class Sample extends BaseRequest {
    public Sample(Context context) {
        super(context);
    }

    public static Sample getDefault(Context context) {
        Sample fragment = new Sample(context);
        return fragment;
    }

    public void sample(String username, String password) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<SampleModel>>(getContext()) {
            @Override
            public Call<SingleTransformer<SampleModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestAPI(Url.getLogin(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new SampleResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.USERNAME, username)
                .addParameter(Keys.PASSWORD, password)
                .showDefaultProgressDialog("Logging in...")
                .execute();
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<SampleModel>> requestAPI(@Path("p") String p, @Part List<MultipartBody.Part> parts);
    }

    public class SampleResponse extends APIResponse<SingleTransformer<SampleModel>> {
        public SampleResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
