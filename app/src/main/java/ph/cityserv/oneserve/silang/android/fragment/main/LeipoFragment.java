package ph.cityserv.oneserve.silang.android.fragment.main;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.activity.JobActivity;
import ph.cityserv.oneserve.silang.android.adapter.LeipoRecyclerViewAdapter;
import ph.cityserv.oneserve.silang.android.dialog.LeipoDialog;
import ph.cityserv.oneserve.silang.android.dialog.NewsDialog;
import ph.cityserv.oneserve.silang.data.model.server.LeipoItem;
import ph.cityserv.oneserve.silang.data.model.server.NewsItem;
import ph.cityserv.oneserve.silang.server.request.Article;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseFragment;
import ph.cityserv.oneserve.silang.vendor.android.java.DividerItemDecoration;
import ph.cityserv.oneserve.silang.vendor.android.java.EndlessRecyclerViewScrollListener;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.CollectionTransformer;

public class LeipoFragment extends BaseFragment implements
        LeipoRecyclerViewAdapter.ClickListener,
        SwipeRefreshLayout.OnRefreshListener, EndlessRecyclerViewScrollListener.Callback{

    public static final String TAG = LeipoFragment.class.getName().toString();

    private JobActivity jobActivity;

    private LeipoRecyclerViewAdapter leipoRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private APIRequest apiRequest;

    @BindView(R.id.newsSRL)                 SwipeRefreshLayout newsSRL;
    @BindView(R.id.newsRV)                  RecyclerView newsRV;
    @BindView(R.id.newsPlaceholderCON)      View newsPlaceholderCON;

    public static LeipoFragment newInstance() {
        LeipoFragment fragment = new LeipoFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_news;
    }

    @Override
    public void onViewReady() {
        jobActivity = (JobActivity) getActivity();
        jobActivity.setTitle("DOING BUSINESS IN ILOILO");

        setUpNewsListView();
    }

    private void setUpNewsListView(){
        newsSRL.setOnRefreshListener(this);
        linearLayoutManager = new LinearLayoutManager(getContext());

        leipoRecyclerViewAdapter = new LeipoRecyclerViewAdapter(getContext());
        leipoRecyclerViewAdapter.setClickListener(this);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        newsRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        newsRV.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(getResources().getDimensionPixelSize(R.dimen.default_dimen)));
        newsRV.setLayoutManager(linearLayoutManager);
        newsRV.setAdapter(leipoRecyclerViewAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Subscribe
    public void onResponse(Article.AllLeipoResponse responseData){
        CollectionTransformer<LeipoItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            if(responseData.isNext()){
                leipoRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                leipoRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }
        }
        if (leipoRecyclerViewAdapter.getItemCount() == 0){
            newsRV.setVisibility(View.GONE);
            newsPlaceholderCON.setVisibility(View.VISIBLE);
        }else {
            newsRV.setVisibility(View.VISIBLE);
            newsPlaceholderCON.setVisibility(View.GONE);
        }
    }

    public void refreshList(){
        apiRequest = Article.getDefault(getContext()).allLeipo(newsSRL);
        apiRequest.first();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    @Override
    public void onReadMoreClick(LeipoItem leipoItem) {
        LeipoDialog.newInstance(leipoItem).show(getChildFragmentManager(),NewsDialog.TAG);
    }
}
