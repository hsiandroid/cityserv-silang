package ph.cityserv.oneserve.silang.android.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import butterknife.BindView;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.data.model.server.BarangayModel;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseRecylerViewAdapter;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class BarangayRecyclerViewAdapter extends BaseRecylerViewAdapter<BarangayRecyclerViewAdapter.ViewHolder, BarangayModel> {

    private ClickListener clickListener;

    public BarangayRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_barangay));
    }

    public String getSelectedItems(){
        String total = "";
        for (BarangayModel specialtyModel : getData()){
            if (specialtyModel.isSelected){
                total = total + specialtyModel.id;

            }
        }

        return total;
    }

    public void setSelected(int id){
        for(BarangayModel item : getData()){
            item.isSelected = item.id == id;
        }
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.categoryTXT.setText(holder.getItem().specialty);
        holder.categoryTXT.setSelected(holder.getItem().isSelected);

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.adapterCON)
        View adapterCON;
        @BindView(R.id.categoryTXT)
        TextView categoryTXT;

        public ViewHolder(View view) {
            super(view);
            adapterCON.setOnClickListener(BarangayRecyclerViewAdapter.this);
        }
        public BarangayModel getItem() {
            return (BarangayModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
                case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onCategoryClicked((BarangayModel) v.getTag());
                }
                break;
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onCategoryClicked(BarangayModel menteesModel);
    }
} 
