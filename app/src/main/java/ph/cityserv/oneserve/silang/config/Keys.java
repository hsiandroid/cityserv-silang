package ph.cityserv.oneserve.silang.config;

/**
 * Created by BCTI 3 on 8/7/2017.
 */

public class Keys {
    public static String DEVICE_ID = "device_id";
    public static String DEVICE_NAME = "device_name";
    public static String DEVICE_REG_ID = "device_reg_id";
    public static String API_TOKEN = "api_token";
    public static String PAGE = "page";
    public static String PER_PAGE = "per_page";
    public static String USER_ID = "user_id";
    public static String USERNAME = "username";
    public static String PASSWORD = "password";
    public static String INCLUDE = "include";
    public static String AUTH_ID = "auth_id";
    public static String EMAIL = "email";
    public static String FNAME = "fname";
    public static String LNAME = "lname";
    public static String CONTACT_NUMBER = "contact_number";
    public static String AGE = "age";
    public static String GENDER = "gender";
    public static String CURRENT_PASSWORD = "current_password";
    public static String PASSWORD_CONFIRMATION = "password_confirmation";
    public static String FIELD = "field";
    public static String NICE_FIELD = "nice_field";
    public static String VALUE = "value";
    public static String TRACKING_NUMBER = "tracking_number";
    public static String TRACKING_ID = "tracking_id";
    public static String NAME = "name";
    public static String PHONE = "phone";
    public static String MOBILE = "mobile";
    public static String TILLE = "title";
    public static String TYPE = "type";
    public static String SUB_TYPE = "sub_type";
    public static String PROPERTY_OWNER = "property_owner";
    public static String PROPERTY_LOCATION = "property_location";
    public static String PIN = "pin";
    public static String ZONE = "zone";
    public static String VALIDATION_TOKEN = "validation_token";
    public static String CONTENT = "content";
    public static String FILE = "file";
    public static String DISPLAY_SCREEN = "display_screen";
    public static String WIDGET_ID = "widget_id";
    public static String REQUEST_ID = "request_id";
    public static String MOMENT_ID = "moment_id";
    public static String REPORT_ID = "report_id";
    public static String JOB_ID = "job_id";
    public static String SERVICE_ID = "service_id";
    public static String STREET = "street";
    public static String BARANGAY = "barangay";
    public static String BRGY = "brgy";
    public static String ANSWER = "answers";
    public static String MNAME = "mname";
    public static String WEIGHT = "weight";
    public static String HEIGHT = "height";
    public static String BIRTHDATE = "birthdate";
    public static String STREET_ADDRESS = "street_address";
    public static String OCCUPATION = "occupation";
    public static String NATIONALITY = "nationality";

}
