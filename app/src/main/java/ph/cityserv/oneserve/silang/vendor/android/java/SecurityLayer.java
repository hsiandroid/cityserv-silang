package ph.cityserv.oneserve.silang.vendor.android.java;

import android.util.Base64;

import java.io.UnsupportedEncodingException;

/**
 * Created by BCTI 3 on 8/15/2017.
 */

public class SecurityLayer {

//    public static String decrypt(String s) {
//        return s;
//    }

    public static String decrypt(String encoded) {
        byte[] dataDec = Base64.decode(encoded, Base64.DEFAULT);
        String decodedString = "";
        try {

            decodedString = new String(dataDec, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        } finally {

            return decodedString;
        }
    }
}
