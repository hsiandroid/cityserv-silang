package ph.cityserv.oneserve.silang.android.dialog;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import icepick.State;
import ph.cityserv.oneserve.silang.R;
import ph.cityserv.oneserve.silang.android.adapter.BarangayRecyclerViewAdapter;
import ph.cityserv.oneserve.silang.data.model.server.BarangayModel;
import ph.cityserv.oneserve.silang.data.model.server.BarangayModel;
import ph.cityserv.oneserve.silang.vendor.android.base.BaseDialog;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class BarangayDialog extends BaseDialog implements View.OnClickListener, BarangayRecyclerViewAdapter.ClickListener{
	public static final String TAG = BarangayDialog.class.getName().toString();

	public static BarangayDialog newInstance(Callback callback) {
		BarangayDialog mentorshipSuccessDialog = new BarangayDialog();
		mentorshipSuccessDialog.callback = callback;
		return mentorshipSuccessDialog;
	}

	private BarangayRecyclerViewAdapter categoryRVAdapeter;
	private LinearLayoutManager menteesLLM;
	private Callback callback;

	@State int specialty_id;

	@BindView(R.id.exitBTN) 		View exitBTN;
	@BindView(R.id.categoryRV) 		RecyclerView categoryRV;


	@Override
	public int onLayoutSet() {return R.layout.dialog_barangays;}

	@Override
	public void onViewReady() {
        exitBTN.setOnClickListener(this);
        setupRV();
	}

	private void setupRV(){
		menteesLLM = new LinearLayoutManager(getContext());
		categoryRVAdapeter = new BarangayRecyclerViewAdapter(getContext());
		categoryRV.setLayoutManager(menteesLLM);
		categoryRVAdapeter.setNewData(getData());
		categoryRV.setAdapter(categoryRVAdapeter);
		categoryRVAdapeter.setClickListener(this);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.exitBTN:
                dismiss();
                break;

        }
    }


	private List<BarangayModel> getData(){
		List<BarangayModel> androidModels = new ArrayList<>();
		BarangayModel barangayItem;

			barangayItem = new BarangayModel();
			barangayItem.id = 1;
			barangayItem.specialty = "Adlas";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 2;
			barangayItem.specialty = "Barangay Celia";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 3;
			barangayItem.specialty = "Anahaw 1";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 4;
			barangayItem.specialty = "Anahaw 2";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 5;
			barangayItem.specialty = "Balite 1";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 6;
			barangayItem.specialty = "Balite 2";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 7;
			barangayItem.specialty = "Balubad";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 8;
			barangayItem.specialty = "Banaba";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 9;
			barangayItem.specialty = "Batas";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 10;
			barangayItem.specialty = "Biga 1";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 11;
			barangayItem.specialty = "Biga 2";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 12;
			barangayItem.specialty = "Biluso";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 13;
			barangayItem.specialty = "Bucal";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 14;
			barangayItem.specialty = "Buho";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 15;
			barangayItem.specialty = "Bulihan";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 16;
			barangayItem.specialty = "Cabangaan";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 17;
			barangayItem.specialty = "Carmen";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 18;
			barangayItem.specialty = "Hoyo";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 19;
			barangayItem.specialty = "Hukay";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 20;
			barangayItem.specialty = "Iba";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 21;
			barangayItem.specialty = "Inchican";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 22;
			androidModels.add(barangayItem);
			barangayItem.specialty = "Ipil I";

			barangayItem = new BarangayModel();
			barangayItem.id = 23;
			barangayItem.specialty = "Ipil II";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 24;
			barangayItem.specialty = "Kalubkob";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 25;
			barangayItem.specialty = "Kaong";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 26;
			barangayItem.specialty = "Lalaan I";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 27;
			barangayItem.specialty = "Lalaan II";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 28;
			barangayItem.specialty = "Litlit";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 29;
			barangayItem.specialty = "Lucsuhin";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 30;
			barangayItem.specialty = "Lumil";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 31;
			barangayItem.specialty = "Maguyam";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 32;
			barangayItem.specialty = "Malabag";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 33;
			barangayItem.specialty = "Malaking Tatiao";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 34;
			barangayItem.specialty = "Mataas na Burol";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 35;
			barangayItem.specialty = "Muntitng Ilog";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 36;
			barangayItem.specialty = "Narra I";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 37;
			barangayItem.specialty = "Narra II";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 38;
			barangayItem.specialty = "Narra III";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 39;
			barangayItem.specialty = "Paligawan";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 40;
			barangayItem.specialty = "Pasong Langka";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 41;
			barangayItem.specialty = "Poblacion I";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 42;
			barangayItem.specialty = "Poblacion II";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 43;
			barangayItem.specialty = "Poblacion III";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 44;
			barangayItem.specialty = "Poblacion IV";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 45;
			barangayItem.specialty = "Poblacion V";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 46;
			barangayItem.specialty = "Pooc I";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 47;
			barangayItem.specialty = "Pooc II";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 48;
			barangayItem.specialty = "Pulong Bunga";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 49;
			barangayItem.specialty = "Pulong Saging";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 50;
			barangayItem.specialty = "Puting Kahoy";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 51;
			barangayItem.specialty = "Sabutan";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 52;
			barangayItem.specialty = "San Miguel I";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 53;
			barangayItem.specialty = "San Miguel II";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 54;
			barangayItem.specialty = "San Vicente I";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 55;
			barangayItem.specialty = "San Vicente II";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 56;
			barangayItem.specialty = "Santol";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 57;
			barangayItem.specialty = "Tartaria";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 58;
			barangayItem.specialty = "Tibig";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 59;
			barangayItem.specialty = "Toledo";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 60;
			barangayItem.specialty = "Tubuan I";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 61;
			barangayItem.specialty = "Tubuan II";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 62;
			barangayItem.specialty = "Tubuan III";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 63;
			barangayItem.specialty = "Ulat";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 64;
			barangayItem.specialty = "Ulat";
			androidModels.add(barangayItem);

			barangayItem = new BarangayModel();
			barangayItem.id = 65;
			barangayItem.specialty = "Yakal";
			androidModels.add(barangayItem);

		return androidModels;
	}


	@Override
	public void onCategoryClicked(BarangayModel menteesModel) {
		if(callback != null){
			callback.onSuccess(menteesModel.specialty, menteesModel.id);
			dismiss();
		}
	}

	public interface Callback {
		void onSuccess(String categoryName, int categoryID);
	}

}
