package ph.cityserv.oneserve.silang.server.request;

import android.content.Context;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import ph.cityserv.oneserve.silang.config.Keys;
import ph.cityserv.oneserve.silang.config.Url;
import ph.cityserv.oneserve.silang.data.model.server.UserItem;
import ph.cityserv.oneserve.silang.data.preference.UserData;
import ph.cityserv.oneserve.silang.vendor.server.request.APIRequest;
import ph.cityserv.oneserve.silang.vendor.server.request.APIResponse;
import ph.cityserv.oneserve.silang.vendor.server.request.BaseRequest;
import ph.cityserv.oneserve.silang.vendor.server.transformer.SingleTransformer;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by BCTI 3 on 8/16/2017.
 */

public class User extends BaseRequest {
    public User(Context context) {
        super(context);
    }

    private String include = "date,info,avatar";

    public static User getDefault(Context context) {
        User fragment = new User(context);
        return fragment;
    }

    public APIRequest field() {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(getContext()) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestAPI(Url.getFieldUser(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new EditProfileResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION));

        return apiRequest;
    }

    public APIRequest password() {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(getContext()) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestAPI(Url.getPasswordUser(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new EditProfileResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION));

        return apiRequest;
    }

    public interface RequestService {
//        @FormUrlEncoded
//        @POST("{p}")
//        Call<SingleTransformer<UserItem>> requestAPI(@Path("p") String p, @FieldMap Map<String, String> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserItem>> requestAPI(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);
    }

    public class UserResponse extends APIResponse<SingleTransformer<UserItem>> {
        public UserResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class EditProfileResponse extends APIResponse<SingleTransformer<UserItem>> {
        public EditProfileResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
