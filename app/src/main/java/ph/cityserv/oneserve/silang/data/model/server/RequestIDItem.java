package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class RequestIDItem extends AndroidModel {

    @SerializedName("status")
    public String status;

    @SerializedName("status_display")
    public String statusDisplay;

    @SerializedName("lgu_id")
    public String lguId;

    @SerializedName("fname")
    public String fname;

    @SerializedName("lname")
    public String lname;

    @SerializedName("mname")
    public String mname;

    @SerializedName("display_name")
    public String displayName;

    @SerializedName("gender")
    public String gender;

    @SerializedName("birthdate")
    public String birthdate;

    @SerializedName("date_issue")
    public String dateIssue;

    @SerializedName("nationality")
    public String nationality;

    @SerializedName("occupation")
    public String occupation;

    @SerializedName("street_address")
    public String streetAddress;

    @SerializedName("brgy")
    public String brgy;

    @SerializedName("weight")
    public String weight;

    @SerializedName("height")
    public String height;

    @SerializedName("contact_number")
    public String contactNumber;

    @SerializedName("contact_email")
    public String contactEmail;

    @SerializedName("request_date")
    public RequestDate requestDate;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public RequestIDItem convertFromJson(String json) {
        return convertFromJson(json, RequestIDItem.class);
    }

    public static class RequestDate {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public Timestamp timestamp;

        public static class Timestamp {
            @SerializedName("date")
            public String date;
            @SerializedName("timezone_type")
            public int timezoneType;
            @SerializedName("timezone")
            public String timezone;
        }
    }
}
