package ph.cityserv.oneserve.silang.data.model.server;

import com.google.gson.annotations.SerializedName;

import ph.cityserv.oneserve.silang.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 6/25/2016.
 */
public class DateItem  extends AndroidModel{

    @SerializedName("date_db")
    public String date_db;

    @SerializedName("month_year")
    public String month_year;

    @SerializedName("time_passed")
    public String time_passed;

    @SerializedName("date")
    public String date;

    @SerializedName("timezone_type")
    public String timezone_type;

    @SerializedName("timezone")
    public String timezone;
}
